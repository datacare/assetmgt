﻿namespace AssetMGT.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class helo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AssetClasses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetClassName = c.String(),
                        AssetClassCode = c.String(),
                        assetClassStatus = c.Int(nullable: false),
                        DepreciationMethod = c.Int(nullable: false),
                        DepPercentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AssetIndicators",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetIndicatorName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Assets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetCode = c.String(),
                        AssetName = c.String(),
                        AssetQuantity = c.String(),
                        AssetStatus = c.String(),
                        AssetSerialNumber = c.String(),
                        AssetEngravementNumber = c.String(),
                        AssetPurchaseDate = c.DateTime(nullable: false),
                        AssetPurchasePrice = c.Int(nullable: false),
                        SalvageValue = c.Int(),
                        AssetComment = c.String(),
                        StartDepreciationDate = c.DateTime(nullable: false),
                        EndDepreciationDate = c.DateTime(),
                        AssetClassId = c.Int(nullable: false),
                        MaintenanceFrequencyId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        SupplierId = c.Int(nullable: false),
                        AssetIndicatorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssetClasses", t => t.AssetClassId, cascadeDelete: true)
                .ForeignKey("dbo.AssetIndicators", t => t.AssetIndicatorId, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.MaintenanceFrequencies", t => t.MaintenanceFrequencyId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.AssetClassId)
                .Index(t => t.MaintenanceFrequencyId)
                .Index(t => t.EmployeeId)
                .Index(t => t.SupplierId)
                .Index(t => t.AssetIndicatorId);
            
            CreateTable(
                "dbo.Depreciations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepreciationMethod = c.String(),
                        DepreciationPercentage = c.Int(nullable: false),
                        DepreciationValue = c.Int(nullable: false),
                        NetbookValue = c.Int(nullable: false),
                        salvageValue = c.Int(nullable: false),
                        DepreciationPeriod = c.Int(nullable: false),
                        CurrencyId = c.Int(nullable: false),
                        AssetId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assets", t => t.AssetId, cascadeDelete: true)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .Index(t => t.CurrencyId)
                .Index(t => t.AssetId);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CurrencyName = c.String(),
                        CurrencyCode = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmpName = c.String(),
                        LocationName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MaintenanceFrequencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TheFrequency = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierName = c.String(),
                        SupplierTelephone = c.String(),
                        SupplierEmail = c.String(),
                        CurrencyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: false)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.ControllerRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RolemodelId = c.Int(nullable: false),
                        ControllersTableId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ControllersTables", t => t.ControllersTableId, cascadeDelete: true)
                .ForeignKey("dbo.Rolemodels", t => t.RolemodelId, cascadeDelete: true)
                .Index(t => t.RolemodelId)
                .Index(t => t.ControllersTableId);
            
            CreateTable(
                "dbo.ControllersTables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ControllerName = c.Int(nullable: false),
                        ActionName = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rolemodels",
                c => new
                    {
                        RolemodelID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RolemodelID);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RolemodelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rolemodels", t => t.RolemodelId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RolemodelId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(),
                        CustomerTelephone = c.String(),
                        CustomerEmail = c.String(),
                        CustomerAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Disposals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisposalDate = c.DateTime(nullable: false),
                        AssetId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assets", t => t.AssetId, cascadeDelete: true)
                .Index(t => t.AssetId);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Date = c.DateTime(nullable: false),
                        Action = c.String(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserActivations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ActivationCode = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Disposals", "AssetId", "dbo.Assets");
            DropForeignKey("dbo.ControllerRoles", "RolemodelId", "dbo.Rolemodels");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "RolemodelId", "dbo.Rolemodels");
            DropForeignKey("dbo.ControllerRoles", "ControllersTableId", "dbo.ControllersTables");
            DropForeignKey("dbo.Assets", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.Suppliers", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.Assets", "MaintenanceFrequencyId", "dbo.MaintenanceFrequencies");
            DropForeignKey("dbo.Assets", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Depreciations", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.Depreciations", "AssetId", "dbo.Assets");
            DropForeignKey("dbo.Assets", "AssetIndicatorId", "dbo.AssetIndicators");
            DropForeignKey("dbo.Assets", "AssetClassId", "dbo.AssetClasses");
            DropIndex("dbo.Disposals", new[] { "AssetId" });
            DropIndex("dbo.UserRoles", new[] { "RolemodelId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.ControllerRoles", new[] { "ControllersTableId" });
            DropIndex("dbo.ControllerRoles", new[] { "RolemodelId" });
            DropIndex("dbo.Suppliers", new[] { "CurrencyId" });
            DropIndex("dbo.Depreciations", new[] { "AssetId" });
            DropIndex("dbo.Depreciations", new[] { "CurrencyId" });
            DropIndex("dbo.Assets", new[] { "AssetIndicatorId" });
            DropIndex("dbo.Assets", new[] { "SupplierId" });
            DropIndex("dbo.Assets", new[] { "EmployeeId" });
            DropIndex("dbo.Assets", new[] { "MaintenanceFrequencyId" });
            DropIndex("dbo.Assets", new[] { "AssetClassId" });
            DropTable("dbo.UserActivations");
            DropTable("dbo.Transactions");
            DropTable("dbo.Disposals");
            DropTable("dbo.Customers");
            DropTable("dbo.Users");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Rolemodels");
            DropTable("dbo.ControllersTables");
            DropTable("dbo.ControllerRoles");
            DropTable("dbo.Suppliers");
            DropTable("dbo.MaintenanceFrequencies");
            DropTable("dbo.Employees");
            DropTable("dbo.Currencies");
            DropTable("dbo.Depreciations");
            DropTable("dbo.Assets");
            DropTable("dbo.AssetIndicators");
            DropTable("dbo.AssetClasses");
        }
    }
}
