﻿namespace AssetMGT.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Assets", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Assets", new[] { "EmployeeId" });
            AlterColumn("dbo.Assets", "EmployeeId", c => c.Int(nullable: true));
            CreateIndex("dbo.Assets", "EmployeeId");
            AddForeignKey("dbo.Assets", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Assets", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Assets", new[] { "EmployeeId" });
            AlterColumn("dbo.Assets", "EmployeeId", c => c.Int());
            CreateIndex("dbo.Assets", "EmployeeId");
            AddForeignKey("dbo.Assets", "EmployeeId", "dbo.Employees", "Id");
        }
    }
}
