﻿namespace AssetMGT.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hello : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assets", "AssetBarCode", c => c.String());
            DropColumn("dbo.Assets", "AssetQuantity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Assets", "AssetQuantity", c => c.String());
            DropColumn("dbo.Assets", "AssetBarCode");
        }
    }
}
