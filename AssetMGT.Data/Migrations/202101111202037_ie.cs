﻿namespace AssetMGT.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ie : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assets", "Subclass", c => c.String());
            DropColumn("dbo.Assets", "AssetSubclass");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Assets", "AssetSubclass", c => c.String());
            DropColumn("dbo.Assets", "Subclass");
        }
    }
}
