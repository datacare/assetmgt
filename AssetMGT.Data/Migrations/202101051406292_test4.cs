﻿namespace AssetMGT.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmployeeAssets", "AssetId", "dbo.Assets");
            DropForeignKey("dbo.EmployeeAssets", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.EmployeeAssets", new[] { "AssetId" });
            DropIndex("dbo.EmployeeAssets", new[] { "EmployeeId" });
            DropTable("dbo.EmployeeAssets");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmployeeAssets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.EmployeeAssets", "EmployeeId");
            CreateIndex("dbo.EmployeeAssets", "AssetId");
            AddForeignKey("dbo.EmployeeAssets", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmployeeAssets", "AssetId", "dbo.Assets", "Id", cascadeDelete: true);
        }
    }
}
