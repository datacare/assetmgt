﻿namespace AssetMGT.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployeeAssets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssetId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assets", t => t.AssetId, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.AssetId)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeAssets", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.EmployeeAssets", "AssetId", "dbo.Assets");
            DropIndex("dbo.EmployeeAssets", new[] { "EmployeeId" });
            DropIndex("dbo.EmployeeAssets", new[] { "AssetId" });
            DropTable("dbo.EmployeeAssets");
        }
    }
}
