﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class Disposal
    {
        public int Id { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Disposal Date")]
        public DateTime DisposalDate { get; set; }

        //[Display(Name = "Disposal Price")]
        //public int DisposalPrice { get; set; }
        
        //public int CurrencyId { get; set; }
        
        public int AssetId { get; set; }

        //public int CustomerId { get; set; }

        //public virtual Currency Currency { get; set; }

          public virtual Asset Asset { get; set; }
        //public virtual ICollection<Asset> Assets { get; set; }

        // public virtual Customer Customer { get; set; }



    }
}
