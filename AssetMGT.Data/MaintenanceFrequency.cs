﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class MaintenanceFrequency
    {
        public int Id { get; set; }
        [Display(Name = "The Frequency")]
        public string TheFrequency { get; set; }
    }
}
