﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class ControllerRole
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Rolemodel")]
        public int RolemodelId { get; set; }

        [ForeignKey("ControllersTable")]
        public int ControllersTableId { get; set; }

        public virtual Rolemodel Rolemodel { get; set; }//Navigation Property

        public virtual ControllersTable ControllersTable { get; set; }//Navigation Property
    }
}
