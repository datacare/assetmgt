﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class Depreciation
    {
        public int Id { get; set; }

        [Display(Name = "Depreciation Method")]
        public string DepreciationMethod { get; set; }

        [Display(Name = "Depreciation Percentage")]
        public int DepreciationPercentage { get; set; }



        [Display(Name = "Depreciation Value")]
        public int DepreciationValue { get; set; }

        [Display(Name = "Netbook Value")]
        public int NetbookValue { get; set; }

        [Display(Name = "Salvage Value ")]
        public int salvageValue { get; set; }


        [Display(Name = "Depreciation Period")]
        public int DepreciationPeriod { get; set; }

        [Display(Name = "Currency Id")]
        public int CurrencyId { get; set; }

        [Display(Name = "Asset Id")]
        public int AssetId { get; set; }

        public virtual Currency Currency { get; set; }

        public virtual Asset Asset { get; set; }

    }
}
