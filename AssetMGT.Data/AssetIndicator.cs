﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class AssetIndicator
    {
        public int Id { get; set; }

        [Display(Name = "Asset Indicator Name")]
        public string AssetIndicatorName { get; set; }

        public EntityCategory Grouping { get; set; }

    }

    public enum EntityCategory
    {
        asset_indicator = 1,
        asset_subcategory = 2
    }
}
