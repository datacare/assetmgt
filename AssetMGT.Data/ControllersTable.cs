﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class ControllersTable
    {
        public int Id { get; set; }

        public ControllerName ControllerName { get; set; }

        public ActionName ActionName { get; set; }

        public virtual List<ControllerRole> ControllerRoles { get; set; }

        public ControllersTable()
        {
            ControllerRoles = new List<ControllerRole>();
        }
    }


    public enum ControllerName
    {
        Asset = 1,
        Account= 2,
        Depreciation = 3,
        Home = 5,
        Disposal = 6,
        RoleModel = 7,
        User = 8,
        ControllersTable = 9,

        AssetClass = 10,
        AssetIndicator = 11,
        Currency = 12,
        Customer = 13,
        MaintenanceFrequency = 14,
        Supplier = 15,
        Employee = 16

    }

    public enum ActionName
    {
        Index = 1,
        Create = 2,
        Edit = 3,
        Details = 4,
        Login = 5,
        Register = 6,
        Delete = 7,
        DuplicateDetails = 8,
        AssetReport = 9,
        ReadyForDisposal = 10


    }
}
