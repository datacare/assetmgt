﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class Rolemodel
    {

        [Key]
        public int RolemodelID { get; set; }

        [Display(Name = "Role")]
        public string RoleName { get; set; }

        public virtual List<UserRole> UserRoles { get; set; }

        public virtual List<ControllerRole> ControllerRoles { get; set; }

        public Rolemodel()
        {
            UserRoles = new List<UserRole>();
            ControllerRoles = new List<ControllerRole>();
        }

       

       
    }
}
