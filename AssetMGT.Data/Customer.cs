﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class Customer
    {
        public int Id { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Customer Telephone ")]
        public string CustomerTelephone { get; set; }

        [Display(Name = "Custome rEmail ")]
        public string CustomerEmail { get; set; }

        [Display(Name = "Custome rAddress")]
        public string CustomerAddress { get; set; }
    }

}
