﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public partial class User 
    {
      
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a valid email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Please enter a valid email")]
        public string Email { get; set; }


        [NotMapped]
        [Required(ErrorMessage = "Please enter a password")]
        //[RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$", ErrorMessage = "Please enter a strong password")]
        public string Password { get; set; }

        [NotMapped]
       // [Required(ErrorMessage = "Please re-enter the same password")]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string ConfirmPassword { get; set; }

         //[ForeignKey("Rolemodel")]
         //public int RolemodelId { get; set; }

        public virtual List<UserRole> UserRoles { get; set; }

        public User() {
            UserRoles = new List<UserRole>();
        }
    }

  
}
