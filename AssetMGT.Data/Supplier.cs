﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class Supplier
    {
        public int Id { get; set; }

        [Display(Name = "Supplier Name")]
        public string SupplierName { get; set; }

        [Display(Name = "Supplier Telephone")]
        public string SupplierTelephone { get; set; }

        [Display(Name = "Supplier Email")]
        public string SupplierEmail { get; set; }
        public int CurrencyId { get; set; }

        public virtual Currency Currency { get; set; }//Navigation property
    }
}
