﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class UserRole
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        [ForeignKey("Rolemodel")]
        public int RolemodelId { get; set; }

        public virtual User User { get; set; }//Navigation Property

        public virtual Rolemodel Rolemodel { get; set; }//Navigation Property


    }
}
