﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class Transaction
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        //formatted date getter
        public string FormattedDate
        {
            get
            {
                return Date.ToString();
            }
        }

        [Display(Name = "Action")]
        [Required(ErrorMessage = "Action is required")]
        public string Action { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        
    }
}
