﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class Asset
    {
        
        public int Id { get; set; }
        
        [Display(Name = "Asset Code")]
        public string AssetCode { get; set; }

        [Display(Name = "Asset Name")]
        public string AssetName { get; set; }

        [Display(Name = "Asset BarCode")]
        public string AssetBarCode { get; set; }

        [Display(Name = "Asset Status")]
        public string AssetStatus { get; set; }

       
        [Display(Name = "Asset Engravement Number")]
        public string AssetEngravementNumber { get; set; }

        [Display(Name = "Asset Subclass")]
        public string AssetSubclass { get; set; }


        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Purchase Date")]
        public DateTime AssetPurchaseDate { get; set; }

     
        [Display(Name = "Purchase Price")]
        public int AssetPurchasePrice { get; set; }

        [Display(Name = "Salvage Value")]
        public int? SalvageValue { get; set; }

        [Display(Name = "Asset Specifications")]
        public string AssetSpecifications { get; set; }

        [Display(Name = "Location")]
        public string AssetLocation { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }


        //[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start Depreciation Date")]
        public DateTime StartDepreciationDate { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Depreciation Date")]
        public DateTime? EndDepreciationDate { get; set; }


        [Display(Name = "Allocation Status")]
        public string AllocationStatus { get; set; }


        [Display(Name = "Asset Class")]
        public int AssetClassId { get; set; }

        [Display(Name = "Maintenance Frequency")]
        public int MaintenanceFrequencyId { get; set; }

        [Display(Name = "Employee")]
        public int? EmployeeId { get; set; }

        [Display(Name = "Supplier")]
        public int? SupplierId { get; set; }

        [Display(Name = "Asset Indicator")]
        public int AssetIndicatorId { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Asset Revalue Date")]
        public DateTime? RevalueDate { get; set; }

        public int? ParentAssetId { get; set; }
        
        public int? AssetRevalue { get; set; }

        [NotMapped]
        public int Year { get; set; }

        [NotMapped]
        public int Month { get; set; }

        [NotMapped]
        public string DepreciationMethod { get; set; }

        [NotMapped]
        public double DepPercentage { get; set; }

        [NotMapped]
        public string AssetClassName { get; set; }
        [NotMapped]
        public string IndicatorName { get; set; }

        [NotMapped]
        public int DepAmount { get; set; }

        [NotMapped]
        public int AccumulatedDepAmount { get; set; }

        [NotMapped]
        public int GrossAmount { get; set; }

        [NotMapped]
        public int accumulatedDepreciation { get; set; }

        [NotMapped]
        //public List<int> AList { get; set; }
        public int []Yearss { get; set; }

        [NotMapped]
        public int[] Months { get; set; }

        [NotMapped]
        public List<int> DepAmounts { get; set; }

        [NotMapped]
        public int[] NetbookValues { get; set; }

        public enum AssetStatuses
        {
            [Display(Name = "Open")]
            Open = 1,
            [Display(Name = "Closed")]
            Closed = 2
        }
        [NotMapped]
        public string FullAssetName
        {
            get
            {
                return AssetName + " - " + AssetEngravementNumber;
            }

        }

        public virtual AssetClass AssetClass { get; set; }
        
        public virtual AssetIndicator AssetIndicator { get; set; }

        public virtual MaintenanceFrequency MaintenanceFrequency { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual List<Depreciation> Depreciations { get; set; }
        
    }
}
