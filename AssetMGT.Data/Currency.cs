﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class Currency
    {
        public int Id { get; set; }

        [Display(Name = "Currency Name")]
        public string CurrencyName { get; set; }

        [Display(Name = "Currency Code")]
        public string CurrencyCode { get; set; }
        
        public string Country { get; set; }
    }
}
