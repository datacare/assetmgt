﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class CodeGenerator
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public EmailStatus Status { get; set; }
        public DateTime expiryTime { get; set; }

        [Required]
        public string Email { get; set; }
 
       public string userId { get; set; }


    }

    public enum EmailStatus
    {
        [Display(Name = "Confirmed")]
        Confirmed = 1,
        [Display(Name = "NotConfirmed")]
        NotConfirmed = 0
    }
}
