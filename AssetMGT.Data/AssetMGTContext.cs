﻿
using AssetMGT.ViewModels;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class AssetMGTContext : IdentityDbContext<ApplicationUser>
    {

        public AssetMGTContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AssetMGTContext, AssetMGT.Data.Migrations.Configuration>());

        }

        public DbSet<Asset> Assets { get; set; }

        public DbSet<AssetClass> AssetClasses { get; set; }

        public DbSet<AssetIndicator> AssetIndicators { get; set; }

        public DbSet<Currency> Currencies { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Depreciation> Depreciations { get; set; }

        public DbSet<Disposal> Disposals { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<MaintenanceFrequency> MaintenanceFrequencies { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        public DbSet<UserActivation> UserActivations { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<Rolemodel> Rolemodels { get; set; }

        public DbSet<ControllersTable> ControllersTables { get; set; }

        public DbSet<ControllerRole> ControllerRoles { get; set; }

        public DbSet<CodeGenerator> CodeGenerators { get; set; }

    }

    //public class ConfigurationDbContext : IdentityDbContext<ApplicationUser>
    //{ }

}