﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data
{
    public class AssetClass
    {
        public int Id { get; set; }
        [Display(Name = "Asset Class Name")]
        public string AssetClassName { get; set; }

        [Display(Name = "Asset Class Code")]
        public string AssetClassCode { get; set; }

        [Display(Name = "Asset Class Status")]
        public AssetClassStatuses assetClassStatus { get; set; }

        [Display(Name = "Depreciation Method")]
        public DepMethods DepreciationMethod { get; set; }

        [Display(Name = "Depreciation Percentage ")]
        public double DepPercentage { get; set; }

        public enum AssetClassStatuses
        {
            [Display(Name = "Open")]
            Open = 1,
            [Display(Name = "Closed")]
            Closed = 2
        }

        public enum DepMethods
        {
            [Display(Name = "Reducing Balance Method")]
            ReducingBalanceMethod = 1,
            [Display(Name = "Straight Line Method")]
            StraightLineMethod = 2,
         
        }

    }
}
