﻿using AssetMGT.Data;
using Microsoft.AspNet.Http.Authentication;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AssetMGT.Service
{
    public class AccountService
    {
        public IdentityUser LoginService(UserManager<IdentityUser> userManager, User theuser)
        {
            DateTime currentTime = DateTime.Now;
            DateTime x30MinsLater = currentTime.AddHours(3).AddMinutes(10);
            //_ = new TimeSpan();
            TimeSpan ts = x30MinsLater - currentTime;
            userManager.DefaultAccountLockoutTimeSpan = ts;
            var user = userManager.FindByEmail(theuser.Email);
            return user;
        }

        public void LogUser(User theuser, AssetMGTContext db)
        {
            //**************Create a transaction here and log the user****************//
            DateTime dateAndTime = DateTime.Now;
            string name = theuser.Email;
            //ViewBag.Name = name;       
            string action = "User Logged In";
            string description = name + " logged in to Asset MIS";
            ActivityLogService activityLogService = new ActivityLogService();
            activityLogService.LogAction(db, action, description, name, dateAndTime);

            //*******************End of transaction***********************//
        }

    }
}
