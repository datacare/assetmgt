﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Data.Migrations
{
    public class DatabaseDefaults
    {
        public bool CreateRolesandUsers()
        {
            //Initialisisng custom roles

            var userStore = new UserStore<IdentityUser>();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
            var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

            //first creating the admin Role and then creating a default admin user
            if (!roleManager.RoleExists("Admin"))
            {
                //creating the admin role
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                //creating an admin super user to maintain the site

                var user = new IdentityUser() { Email = "rukexcel@gmail.com", UserName = "rukexcel@gmail.com" };
                IdentityResult result = userManager.Create(user, "Lgg40highres");

                try
                {
                    var provider = new DpapiDataProtectionProvider("AssetMGT");
                    userManager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(
                        provider.Create("AssetMGTToken"));
                    string code = userManager.GenerateEmailConfirmationToken(user.Id);

                    IdentityResult resultConfirmEmail = userManager.ConfirmEmail(user.Id, code);


                }
                catch (Exception E)
                {
                    return false;
                }

                if (result.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Admin");
                    AssetMGTContext db = new AssetMGTContext();


                    //********************************************************************************//
                    //******************Create Controllers and actions in the database*****************//

                    //create a list to store all controllers
                    List<ControllersTable> allControllerRegister = new List<ControllersTable>();
                    //Account/Create


                    //var ControllerValues = Enum.GetValues(typeof(ControllerName));
                    foreach (ControllerName controllerName in Enum.GetValues(typeof(ControllerName)))
                    {

                        foreach (ActionName actionName in Enum.GetValues(typeof(ActionName)))
                        {
                            ControllersTable account = new ControllersTable();
                            account.ControllerName = controllerName;
                            account.ActionName = actionName;

                            if ((controllerName == ControllerName.Account) && (actionName == ActionName.Login))
                            {
                                continue;
                            }
                            else if ((controllerName == ControllerName.Account) && (actionName == ActionName.Register))
                            {
                                continue;
                            }
                            else if ((controllerName == ControllerName.Home) && (actionName == ActionName.Create))
                            {
                                continue;
                            }
                            else if ((controllerName == ControllerName.Home) && (actionName == ActionName.Edit))
                            {
                                continue;
                            }
                            else if ((controllerName == ControllerName.Home) && (actionName == ActionName.Details))
                            {
                                continue;
                            }
                            else if ((controllerName == ControllerName.Home) && (actionName == ActionName.Delete))
                            {
                                continue;
                            }
                            else if (actionName == ActionName.Register)
                            {
                                continue;
                            }
                            else if (actionName == ActionName.Login)
                            {
                                continue;
                            }
                            try
                            {
                                db.ControllersTables.Add(account);
                            }catch(Exception e)
                            {

                            }
                            
                            allControllerRegister.Add(account);
                        }

                    }


                    //Account/Edit
                    ControllersTable account3 = new ControllersTable();
                    account3.ControllerName = ControllerName.Account;
                    account3.ActionName = ActionName.Login;
                    db.ControllersTables.Add(account3);
                    allControllerRegister.Add(account3);

                    //Account/Edit
                    ControllersTable account4 = new ControllersTable();
                    account4.ControllerName = ControllerName.Account;
                    account4.ActionName = ActionName.Register;
                    db.ControllersTables.Add(account4);
                    allControllerRegister.Add(account4);


                    //******************Create Controllers and actions in the database*****************//
                    //********************************************************************************//


                    //create the admin role in the database
                    Rolemodel rolemodel = new Rolemodel();
                    rolemodel.RoleName = "Admin";
                    rolemodel.ControllerRoles = new List<ControllerRole>();

                    //Add Admin role to all the controllers and actions
                    //**for each item in registered controllers, add these controllers to admin role*****//
                    foreach (var item in allControllerRegister)
                    {
                        ControllerRole controllerRole = new ControllerRole();
                        controllerRole.ControllersTable = item;
                        rolemodel.ControllerRoles.Add(controllerRole);
                    }
                    db.Rolemodels.Add(rolemodel);

                    //User/Create (Create the Admin User)
                    User newUser = new User();
                    newUser.Name = "Admin";
                    newUser.Email = "rukexcel@gmail.com";
                    newUser.UserRoles = new List<UserRole>();

                    //Associate the created user with the Admin role
                    UserRole userrolle = new UserRole();
                    userrolle.Rolemodel = rolemodel;
                    newUser.UserRoles.Add(userrolle);



                    //save the user
                    try
                    {
                        db.Users.Add(newUser);
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                    }




                }



            }
            else
            {
                return false;
            }
            return true;
        }
        
    }
}
