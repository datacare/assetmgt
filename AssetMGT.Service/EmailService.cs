﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using AssetMGT.Data;
using System.Net;
using System.Web.Mvc;
using AssetMGT.Data;
using System.Security.Cryptography;

using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Web;

namespace AssetMGT.Service
{
    public class EmailService
    {
        //private void EmailSend(string SenderEmail, string Subject, string Message)
        //public bool EmailSend(string callbackurl, string userEmail)
        public void  EmailSend(string callbackurl, string userEmail,string body,string subject)
        {
            AssetMGTContext db = new AssetMGTContext();
            bool status = false;
            try
            {

                string HostAddress = ConfigurationManager.AppSettings["Host"].ToString();
                string FormEmailId = ConfigurationManager.AppSettings["mailAccount"].ToString();
                string Password = ConfigurationManager.AppSettings["mailPassword"].ToString();
                string Port = ConfigurationManager.AppSettings["Port"].ToString();

                MailMessage mailMessage = new MailMessage(System.Configuration.ConfigurationManager.AppSettings["mailAccount"].ToString(), userEmail);
                mailMessage.From = new MailAddress(FormEmailId);
                mailMessage.Subject = subject;
               
                mailMessage.Body = body;

                mailMessage.IsBodyHtml = true;
                //mm.To.Add(new MailAddress(SenderEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = HostAddress;
                smtp.EnableSsl = true;
                NetworkCredential networkCredential = new NetworkCredential();
                networkCredential.UserName = mailMessage.From.Address;
                networkCredential.Password = Password;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = networkCredential;
                smtp.Port = Convert.ToInt32(Port);
                smtp.Send(mailMessage);
                status = true;
                //return status;

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.Current.User.Identity.Name;
                string action = "User Created";
                string description = "Created the user whose email is "+ userEmail;
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

            }
            catch (Exception e)
                 {
                    //return status;
                 }

            }


        }

}



 
      

