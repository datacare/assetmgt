﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Service
{
    public class ActivityLogService
    {
        public bool LogAction(AssetMGTContext db, string action, string description, string name, DateTime dateAndTime)
        {
            try
            {
                //**************Create a transaction here and log the user****************//

                //DateTime transDate = DateTime.Now;
                Transaction transaction2 = new Transaction();
                transaction2.Date = dateAndTime;
                transaction2.UserName = name;
                transaction2.Action = action;
                transaction2.Description = description;
                //transaction2.Date = transDate;

                db.Transactions.Add(transaction2);
                db.SaveChanges();
                return true;
                //*******************End of transaction***********************//
            }
            catch (Exception e)
            {

            }
            return false;
        }
    }
}
