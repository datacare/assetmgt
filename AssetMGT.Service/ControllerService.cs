﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AssetMGT.Service
{
    public class ControllerService
    {
        public void CreateControllerInfo(Rolemodel rolemodel, string[] selectedControllers, AssetMGTContext db)
        {
            try
            {
                if (selectedControllers != null)
                {
                    rolemodel.ControllerRoles = new List<ControllerRole>();
                    foreach (var role in selectedControllers)
                    {
                        var ControllerToAdd = db.ControllersTables.Find(int.Parse(role));
                       // string roleName = roleToAdd.RoleName;

                        ControllerRole controllerrolle = new ControllerRole();
                        controllerrolle.ControllersTable = ControllerToAdd;

                        rolemodel.ControllerRoles.Add(controllerrolle);

                       // var result1 = userManager.AddToRole(AspNetuser.Id, roleName);
                    }
                }else if(selectedControllers == null)
                {
                    rolemodel.ControllerRoles = new List<ControllerRole>();
                    var ControllersToAdd = db.ControllersTables;
                    foreach(var controller in ControllersToAdd)
                    {
                        //skip senstive actions
                        if(controller.ActionName == ActionName.Create)
                        {
                            continue;
                        }
                        if(controller.ActionName == ActionName.Edit)
                        {
                            continue;
                        }
                        
                        if (controller.ActionName == ActionName.Register)
                        {
                            continue;
                        }
                        if (controller.ActionName == ActionName.Delete)
                        {
                            continue;
                        }
                        if (controller.ControllerName == ControllerName.User)
                        {
                            continue;
                        }
                        if (controller.ControllerName == ControllerName.RoleModel)
                        {
                            continue;
                        }
                        ControllerRole controllerrolle = new ControllerRole();
                        controllerrolle.ControllersTable = controller;

                        rolemodel.ControllerRoles.Add(controllerrolle);

                        //add the rest of the controllers to this role


                    }


                }

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.Current.User.Identity.Name;
                string action = "Role Created";
                string description = "Created the Role called " + rolemodel .RoleName;
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                db.Rolemodels.Add(rolemodel);
                db.SaveChanges();
            }

            catch(Exception e)
            {

            }
        }
        public void UpdateControllerInfo(int? id, Rolemodel rolemodel, string[] selectedControllers, AssetMGTContext db)
        {
            try
            {
                var roleToUpdate = db.Rolemodels.Where(i => i.RolemodelID == id).Single();

                if (selectedControllers != null)
                {
                    List<ControllerRole> controllerToUpdate = db.ControllerRoles.Where(i => i.RolemodelId == id).ToList();

                    db.ControllerRoles.RemoveRange(controllerToUpdate);

                    rolemodel.ControllerRoles = new List<ControllerRole>();
                    foreach (var role in selectedControllers)
                    {
                        var ControllerToAdd = db.ControllersTables.Find(int.Parse(role));
                        // string roleName = roleToAdd.RoleName;

                        ControllerRole controllerrolle = new ControllerRole();
                        controllerrolle.ControllersTable = ControllerToAdd;
                        controllerrolle.Rolemodel = roleToUpdate;


                        rolemodel.ControllerRoles.Add(controllerrolle);

                        db.ControllerRoles.Add(controllerrolle);
                        db.SaveChanges();

                        // var result1 = userManager.AddToRole(AspNetuser.Id, roleName);
                    }
                }
                else if (selectedControllers == null)
                {
                    List<ControllerRole> controllerToUpdate = db.ControllerRoles.Where(i => i.RolemodelId == id).ToList();

                    db.ControllerRoles.RemoveRange(controllerToUpdate);

                    rolemodel.ControllerRoles = new List<ControllerRole>();
                    var ControllersToAdd = db.ControllersTables.ToList();
                    foreach (var controller in ControllersToAdd)
                    {
                        //skip senstive actions
                        if (controller.ActionName == ActionName.Create)
                        {
                            continue;
                        }
                        if (controller.ActionName == ActionName.Edit)
                        {
                            continue;
                        }
                        if (controller.ActionName == ActionName.Register)
                        {
                            continue;
                        }
                        if (controller.ActionName == ActionName.Delete)
                        {
                            continue;
                        }
                        if (controller.ControllerName == ControllerName.User)
                        {
                            continue;
                        }
                        if (controller.ControllerName == ControllerName.RoleModel)
                        {
                            continue;
                        }

                        ControllerRole controllerrolle = new ControllerRole();
                        controllerrolle.ControllersTable = controller;
                        controllerrolle.Rolemodel = roleToUpdate;
                        rolemodel.ControllerRoles.Add(controllerrolle);
                   
                        db.ControllerRoles.Add(controllerrolle);
                        db.SaveChanges();
                        //add the rest of the controllers to this role


                    }


                }

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.Current.User.Identity.Name;
                string action = "Role Created";
                string description = "Created the Role called " + rolemodel.RoleName;
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                //db.Rolemodels.Add(rolemodel);
                db.SaveChanges();
            }

            catch (Exception e)
            {

            }
        }
    }
}
