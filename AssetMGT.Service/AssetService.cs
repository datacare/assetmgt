﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Service
{
    public class AssetService
    {
        public void GetRevaluedAssets(AssetMGTContext db, Asset asset, Asset theAsset)
        {
            Disposal disposal = db.Disposals.SingleOrDefault(m => m.AssetId == asset.Id);
            Asset oldAsset = new Asset();
            Asset newAsset = new Asset();

            newAsset = theAsset;
            oldAsset = asset;
            if (disposal != null)
            {
                oldAsset.AssetStatus = "Disposed";
            }
            oldAsset.EndDepreciationDate = new DateTime(2019, 1, 2);
            oldAsset.RevalueDate = new DateTime(2019, 1, 2);
            oldAsset.AssetStatus = "Revalued";

            //edit the old record
            db.Entry(oldAsset).State = EntityState.Modified;
            db.SaveChanges();

            //while revaluing, we recreate another asset
            var lastAsset = db.Assets.OrderByDescending(c => c.AssetCode).FirstOrDefault();
            if (lastAsset == null)
            {
                newAsset.AssetCode = "AC/DC/001";

            }
            else
            {//generating A random Asst class code
                Random random = new Random();
                newAsset.AssetCode = "AC/DC/" + random.Next(2000, 2200);
            }
            newAsset.AssetPurchasePrice = asset.AssetRevalue.GetValueOrDefault();
            newAsset.ParentAssetId = asset.Id;
            newAsset.AssetRevalue = null;
            newAsset.AssetPurchaseDate = new DateTime(2019, 1, 2);
            newAsset.StartDepreciationDate = new DateTime(2019, 1, 2);

            newAsset.AssetStatus = "Active";
            newAsset.RevalueDate = null;
            db.Assets.Add(newAsset);
            db.SaveChanges();

        }
    }
}
