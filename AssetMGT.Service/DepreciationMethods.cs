﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Service
{
    public class DepreciationMethods
    {
        private AssetMGTContext db = new AssetMGTContext();
        public int CalculateDepreciation(int? id, string DepreciationMethod, int? depPercentage)
        {
            // creating an assetLIst
            List<Asset> assetList = new List<Asset>();
            int WorthAmount = 0;
            if ((DepreciationMethod != null) || (depPercentage != null))
            {
                //getting the selected asset from the db
                Asset asset = db.Assets.Find(id);
                DateTime purchasedate = asset.AssetPurchaseDate;
                int usefullife = DateTime.UtcNow.Year - purchasedate.Year;

                int purchaseprice = asset.AssetPurchasePrice;


                int depAmount = Convert.ToInt32((Convert.ToDouble(depPercentage) / Convert.ToDouble(100)) * purchaseprice);

                WorthAmount = purchaseprice - depAmount;

                int Years = purchasedate.Year;

                string theDepMethod = asset.DepreciationMethod;

               //adding the dep values to a list
                assetList.Add(new Asset()
                {
                    Year = purchasedate.Year,

                    DepPercentage = (int)depPercentage,

                    DepAmount = depAmount,

                    GrossAmount = WorthAmount,

                    DepreciationMethod = DepreciationMethod

                });

                //when reducing balance is chosen
                if (DepreciationMethod == "rbm")
                {

                    //ViewBag.DisplayMethod = "Using the Reducing Balance Method";
                    int TheYears = 0;
                    for (int counter = 1; counter <= usefullife; counter++)
                    {
                        TheYears = Years + counter;
                        depPercentage = (int)depPercentage;
                        depAmount = Convert.ToInt32((Convert.ToDouble(depPercentage) / Convert.ToDouble(100)) * WorthAmount);
                        WorthAmount = WorthAmount - depAmount;

                        //add all the different dep values to a list
                        assetList.Add(new Asset()
                        {
                            Year = TheYears,

                            DepPercentage = (int)depPercentage,

                            DepAmount = depAmount,

                            GrossAmount = WorthAmount,

                            DepreciationMethod = DepreciationMethod

                        });

                    }

                }


                else if (DepreciationMethod == "slm")
                {
                    //ViewBag.DisplayMethod = "Using the Straight Line Method";
                    int TheYears = 0;
                    for (int counter = 1; counter <= usefullife; counter++)
                    {
                        TheYears = Years + counter;
                        depPercentage = (int)depPercentage;
                        depAmount = Convert.ToInt32((Convert.ToDouble(depPercentage) / Convert.ToDouble(100)) * WorthAmount);

                        //add all the dep values to the list
                        assetList.Add(new Asset()
                        {
                            Year = TheYears,

                            DepPercentage = (int)depPercentage,

                            DepAmount = depAmount,

                            GrossAmount = WorthAmount,

                            DepreciationMethod = DepreciationMethod

                        });

                    }
                }

                //ViewBag.assetList = assetList;

            }

            return WorthAmount;
        }
    }
}
