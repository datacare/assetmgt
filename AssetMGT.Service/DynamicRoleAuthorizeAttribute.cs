﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace AssetMGT.Service
{
    public class DynamicRoleAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("~/Account/UnAuthorised");
                //base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                filterContext.Result = new RedirectResult("~/Account/Index");
            }
            //filterContext.Result = new HttpUnauthorizedResult(); // Try this but i'm not sure
            
        }

        public ControllerName theControllerName { get; set; }
        public ActionName theActionName { get; set; }

        AssetMGTContext db = new AssetMGTContext();
        public DynamicRoleAuthorizeAttribute(ControllerName ControllerName, ActionName ActionName)
        {
           this.theControllerName = ControllerName;
           this.theActionName = ActionName;
        }
        public DynamicRoleAuthorizeAttribute()
        {
          
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            Roles = string.Join(",", Get(theControllerName, theActionName));
            return base.AuthorizeCore(httpContext);
        }
        

        public List<string> Get(ControllerName controller, ActionName action)
        {
            
            var UserEmail = HttpContext.Current.User.Identity.Name;
            try
            {
                var User = db.Users.Where(i => i.Email == UserEmail).Single();

            }
            catch (Exception e)
            {
               // return new RedirectResult("Index");
            }
            
            
            var getControllerId = db.ControllersTables.Where(v => v.ControllerName == controller && v.ActionName == action).Single();
            int theControllerID = getControllerId.Id;
            var Roles = db.ControllerRoles.Where(a => a.ControllersTableId == theControllerID).ToList();


            List<string> RoleList= new List<string>();
            foreach(var item in Roles)
            {
                var RoleName = db.Rolemodels.Where(a => a.RolemodelID == item.RolemodelId).ToList();
                foreach(var useritem in RoleName)
                {
                    RoleList.Add(useritem.RoleName);
                }
                
            }

            return RoleList;
        }
    }
   
   
}