﻿using AssetMGT.Data;
using AssetMGT.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace AssetMGT.Service
{
    public class UserService
    {
        //**********Method for fectching users***************//
        public void RetrieveUsers()
        {
            /*UserIndexData assignedRoleData = new UserIndexData();

            assignedRoleData.Users = db.Users;*/

        }
        //**********End of Method for fectching users***************//

        //************Method for updating a user**************//
        public void UpdateUser(int? id, User user, string[] selectedRoles, AssetMGTContext db)
        {
            try
            {

                var UserToUpdate = db.Users.Where(i => i.ID == id).Single();
                UpdateUserRoles(selectedRoles, UserToUpdate, db);

                var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

                var registedUser = userManager.FindByEmail(user.Email);
                var userId = registedUser.Id;


                if (selectedRoles != null)
                {
                    

                    //get all his roles.
                    List<string> allroles = userManager.GetRoles(userId).ToList();
                    List<UserRole> roleToUpdate = db.UserRoles.Where(i => i.UserId == id).ToList();

                    db.UserRoles.RemoveRange(roleToUpdate);


                    user.UserRoles = new List<UserRole>();
                    //make a list of roles that have been selected
                    List<string> allmarkedroles= new List<string>();
                    foreach (var role in selectedRoles)
                    {
                        var roleToAdd = db.Rolemodels.Find(int.Parse(role));
                        string roleName = roleToAdd.RoleName;
                        allmarkedroles.Add(roleName);

                        UserRole userrolle = new UserRole();
                        userrolle.Rolemodel = roleToAdd;
                        userrolle.UserId = id.Value;
                        userrolle.RolemodelId = roleToAdd.RolemodelID;

                        db.UserRoles.Add(userrolle);
                        db.SaveChanges();

                        bool x = userManager.IsInRole(userId, roleName);
                        
                        if (x == false)
                        {
                            var result1 = userManager.AddToRole(userId, roleName);

                        }
                    }

                    //remove the roles that have not been selected
                    foreach(var arole in allroles)
                    {
                        if (!allmarkedroles.Contains(arole))
                        {
                            userManager.RemoveFromRole(userId, arole);
                        }
                    }


                    

                }
                else if (selectedRoles == null)
                {
                    List<UserRole> roleToUpdate = db.UserRoles.Where(i => i.UserId == id).ToList();

                    db.UserRoles.RemoveRange(roleToUpdate);

                    var therole = db.Rolemodels.Where(i => i.RoleName == "Standard User").Single();

                    UserRole userrolle = new UserRole();
                    userrolle.Rolemodel = therole;
                    userrolle.RolemodelId = therole.RolemodelID;
                    userrolle.UserId = id.Value;

                    db.UserRoles.Add(userrolle);
                    db.SaveChanges();


                    user.UserRoles = new List<UserRole>();
                    user.UserRoles.Add(userrolle);
                    UserToUpdate.UserRoles = new List<UserRole>();

                }
                
                db.SaveChanges();
            }

            catch (Exception e)
            {

            }

        }


        private void UpdateUserRoles(string[] selectedRoles, User UserToUpdate, AssetMGTContext db)
        {
            if (selectedRoles == null)
            {
                

                UserToUpdate.UserRoles = new List<UserRole>();
                return;
/*
                foreach (var role in db.UserRoles)
                {
                    UserToUpdate.UserRoles.Remove(role);


                }
                var therole = db.Rolemodels.Where(i => i.RoleName == "Standard User").Single();
                UserRole userrolle = new UserRole();
                userrolle.Rolemodel = therole;

                UserToUpdate.UserRoles = new List<UserRole>();
                UserToUpdate.UserRoles.Add(userrolle);
                
              */  

            }
            else if (selectedRoles != null)
            {
                

            }

        }

        //************End of Method for updating a user**************//


        //************Method for creating a user**************//
        public IdentityResult CreateUser(User user, string[] selectedRoles, AssetMGTContext db, UserManager<IdentityUser> userManager, IdentityUser AspNetuser)
        {
            //lockout users for 10 minutes
            // userManager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);

            // takes 6 incorrect attempts to lockout 
            // userManager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // when new user is created, they will be "lockable". 

            AspNetuser.LockoutEnabled = true;
            IdentityResult result = null;
            try
            {
                

                 result = userManager.Create(AspNetuser, user.Password);

                var theuser = new ApplicationUser
                {
                    UserName = user.Email,
                    Email = user.Email,
                    EmailConfirmed = true

                };

                var theresult = userManager.Create(theuser, user.Password);


                if (selectedRoles != null)
                {
                    user.UserRoles = new List<UserRole>();
                    foreach (var role in selectedRoles)
                    {
                        var roleToAdd = db.Rolemodels.Find(int.Parse(role));
                        string roleName = roleToAdd.RoleName;

                        UserRole userrolle = new UserRole();
                        userrolle.Rolemodel = roleToAdd;

                        user.UserRoles.Add(userrolle);

                        var result1 = userManager.AddToRole(AspNetuser.Id, roleName);
                    }
                }
                else if (selectedRoles == null)
                {

                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
                    if (!roleManager.RoleExists("Standard User"))
                    {
                        //creating the standard user in aspnet roles
                        var role = new IdentityRole();
                        role.Name = "Standard User";
                        roleManager.Create(role);

                        //creating a standard user in role model
                        Rolemodel newrolemodel = new Rolemodel();
                        newrolemodel.RoleName = "Standard User";
                        newrolemodel.ControllerRoles = new List<ControllerRole>();
                        db.Rolemodels.Add(newrolemodel);
                        

                        //Associate the standard role with appropriate controllers and actions
                        var controllerActions = db.ControllersTables.ToList();

                        foreach(var item in controllerActions)
                        {
                            ControllerRole controllerRole = new ControllerRole();
                            controllerRole.ControllersTable = item;
                            switch (item.ActionName)
                            {
                                case ActionName.Create:
                                    continue;
                                case ActionName.Delete:
                                    continue;
                                case ActionName.Edit:
                                    continue;
                                case ActionName.Register:
                                    continue;
                                
                            }
                            switch (item.ControllerName)
                            {
                                case ControllerName.User:
                                    continue;
                                case ControllerName.RoleModel:
                                    continue;
                            }
                            newrolemodel.ControllerRoles.Add(controllerRole);
                        }
                        db.SaveChanges();
                    }
                    var therole = db.Rolemodels.Where(i => i.RoleName == "Standard User").Single();
                    var result1 = userManager.AddToRole(AspNetuser.Id, "Standard User");

                    UserRole userrolle = new UserRole();
                    userrolle.Rolemodel = therole;

                    user.UserRoles = new List<UserRole>();
                    user.UserRoles.Add(userrolle);
                }

                db.Users.Add(user);
                db.SaveChanges();

             
            }

            catch(Exception e)
            {

            }
            return result;
        }
        //************End of Method for creating a user**************//
    }

}
