﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetMGT.App_Start
{
    public class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user

            
                    app.UseCookieAuthentication(new CookieAuthenticationOptions
                    {
                        //ExpireTimeSpan = TimeSpan.FromMinutes(1),
                        AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                        LoginPath = new PathString("/Account/UnAuthorised"),
                       // SlidingExpiration = false,
                        /*Provider = new CookieAuthenticationProvider { 
                            OnResponseSignIn = context => { context.Properties.AllowRefresh = true; context.Properties.ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(0); } }
                        /*Provider = new CookieAuthenticationProvider
                        {
                            OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<userManager, user>(
                                    
                        regenerateIdentity: (userManager, user) => user.GenerateUserIdentityAsync(userManager))
                                )
                        }*/
                        /*ExpireTimeSpan = TimeSpan.FromMinutes(2),
                        SlidingExpiration = true,
                        LogoutPath = new PathString("/Account/Index")*/


                    });
           

            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            // app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            // clientId: "",
            // clientSecret: "");

            //app.UseTwitterAuthentication(
            // consumerKey: "",
            // consumerSecret: "");

            //app.UseFacebookAuthentication(
            // appId: "",
            // appSecret: "");

            //app.UseGoogleAuthentication();
        }
    }
}