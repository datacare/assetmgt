﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AssetMGT.ViewModels
{
    public class ResetPasswordViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        public string Password { get; set; }

        //  [Compare("Password",ErrorMessage ="Confirm Password doesn't match,type again")]
        [Required(ErrorMessage = "Please re-enter the same password")]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string ConfirmPassword { get; set; }

        [NotMapped]
        public string Code { get; set; }

    }
    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
    }
}