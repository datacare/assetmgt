﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace AssetMGT.ViewModels
{
    public class DynamicRoleAuthorizeAttribute : AuthorizeAttribute
    {
        public ControllerName theControllerName { get; set; }
        public ActionName theActionName { get; set; }

        AssetMGTContext db = new AssetMGTContext();
        public DynamicRoleAuthorizeAttribute(ControllerName ControllerName, ActionName ActionName)
        {
           this.theControllerName = ControllerName;
           this.theActionName = ActionName;
        }
        public DynamicRoleAuthorizeAttribute()
        {
          
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            Roles = string.Join(",", Get(theControllerName, theActionName));
            return base.AuthorizeCore(httpContext);
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
      
            filterContext.Result = new RedirectResult("/Home/UnAuthorised");
            base.HandleUnauthorizedRequest(filterContext);
        }

        public List<string> Get(ControllerName controller, ActionName action)
        {
            
            var UserEmail = HttpContext.Current.User.Identity.Name;
            var User = db.Users.Where(i => i.Email == UserEmail).Single();
            //int ID = User.ID;

            

            var getControllerId = db.ControllersTables.Where(v => v.ControllerName == controller && v.ActionName == action).Single();
            int theControllerID = getControllerId.Id;
            var Roles = db.ControllerRoles.Where(a => a.ControllersTableId == theControllerID).ToList();


            List<string> RoleList= new List<string>();
            foreach(var item in Roles)
            {
                var RoleName = db.Rolemodels.Where(a => a.RolemodelID == item.RolemodelId).ToList();
                foreach(var useritem in RoleName)
                {
                    RoleList.Add(useritem.RoleName);
                }
                
            }

            return RoleList;
        }
    }
   
   
}