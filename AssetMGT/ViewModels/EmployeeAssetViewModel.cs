﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssetMGT.ViewModels
{
    public class EmployeeAssetViewModel
    {

        public string EmpName { get; set; }
        public int EmployeeId { get; set; }
        public string AssetName { get; set; }
        public int AssetId { get; set; }
        public string AllocationStatus { get; set; }

        
        public List<Employee> Employees { get; set; }
        public List<Asset> Assets { get; set; }

        public IEnumerable<SelectListItem> AssetList { get; set; }
        public IEnumerable<SelectListItem> EmployeeList { get; set; }

    }
}