﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static AssetMGT.Data.AssetClass;

namespace AssetMGT.ViewModels
{
    public class CalculateDepreciations
    {
        public int[] CalculateDepAmount(int purchasePrice, string depMethod, int percentage, int? usefullife, DateTime startDepDate, DateTime currentDate)
        {
            int[] myArray = new int[3];
            int singledepAmount = 0;
            int singleWorthAmount = 0;
            int accDepreciation = 0;
            
            if (depMethod == "StraightLineMethod")
            {
                singledepAmount = Convert.ToInt32((Convert.ToDouble(percentage) / Convert.ToDouble(100)) * purchasePrice);

                singleWorthAmount = purchasePrice - singledepAmount;
                for (int count = 1; count <= usefullife; count++)
                {
                    accDepreciation += singledepAmount;
                }


                myArray[0] = singledepAmount;
                myArray[1] = singleWorthAmount;
                myArray[2] = accDepreciation;
            }

            else if (depMethod == "ReducingBalanceMethod")
            {


                int depAmount = Convert.ToInt32((Convert.ToDouble(percentage) / Convert.ToDouble(100)) * purchasePrice);
                int depPerDayAmount = depAmount / 365;
                int daysPassed = startDepDate.Date.Subtract(currentDate).Days;
                int firstYear = startDepDate.Year;

                DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
                int numberofDays = (firstYearEndDate - startDepDate).Days;
                int yearOneDepAmount = depPerDayAmount * numberofDays;
                int WorthAmount = purchasePrice - yearOneDepAmount;
                int Years = startDepDate.Year;
                int theaccumulatedDep = yearOneDepAmount;

                for (int counter = 1; counter < usefullife; counter++)
                {
                    yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(percentage) / Convert.ToDouble(100)) * WorthAmount);

                    WorthAmount = WorthAmount - yearOneDepAmount;
                    theaccumulatedDep += yearOneDepAmount;
                    int percent = Convert.ToInt32(percentage);
                    //checking whether worth amount == salvage value for automatic ready for disposal status
                    


                }

                myArray[0] = yearOneDepAmount;
                myArray[1] = WorthAmount;
                myArray[2] = theaccumulatedDep;

            }
            
            
            return myArray;
        }

        public List<Asset> Depreciator(DateTime purchaseDate, int purchasePrice, DepMethods depMethod, int percentage, int? usefullife, int? SalvageValue)
        {
            // creating an assetList
            List<Asset> theAssetList = new List<Asset>();
            Asset myasset = new Asset();

            int? salvageValue = SalvageValue;
           
            //List<int> theYears = new List<int>();
            int Years = purchaseDate.Year;

            int theSingleDep = 0;
            int singleWorthAmount = 0;
            if (depMethod == AssetClass.DepMethods.ReducingBalanceMethod)
            {
                int accumulatedDepreciation = 0;
                // ViewBag.DisplayMethod = "Using the Reducing Balance Method";
               // int TheYears = purchaseDate.Year;
                int singlepurchaseprice = purchasePrice;
                int singledepAmount = Convert.ToInt32((Convert.ToDouble(percentage) / Convert.ToDouble(100)) * singlepurchaseprice);


                singleWorthAmount = purchasePrice - singledepAmount;


                List<int> theDepAmounts = new List<int>();
                for (int counter = 1; counter <= usefullife; counter++)
                {

                    Years = Years + counter;
                    //theYears.Add(TheYears);
                    //Years++;

                    //TheDepPercentage = TheDepPercentage;
                    singledepAmount = Convert.ToInt32((Convert.ToDouble(percentage) / Convert.ToDouble(100)) * singleWorthAmount);


                    theDepAmounts.Add(singledepAmount);

                    accumulatedDepreciation += singledepAmount;
                    singleWorthAmount = singleWorthAmount - singledepAmount;

                    //checking whether worth amount == salvage value for automatic ready for disposal status
                    if ((singleWorthAmount <= salvageValue) || (singleWorthAmount == 0))
                    {
                        break;
                    }

                    theSingleDep = singledepAmount;
                    int percent = Convert.ToInt32(percentage);
                    //ViewBag.DepAmounts = theDepAmounts;


                }

                    
                myasset.DepAmounts = theDepAmounts;
                myasset.DepAmount = theSingleDep;
                myasset.GrossAmount = singleWorthAmount;

            }
                

            if (depMethod == AssetClass.DepMethods.StraightLineMethod)
            {
                int accumulatedDepreciation = 0;
                // ViewBag.DisplayMethod = "Using the Straight Line Methodtr";
                int singlepurchaseprice = purchasePrice;
                int mysingleWorthAmount = 0;
                int TheYears = 0;
                int singledepAmount = 0;
                List<int> mytheYears = new List<int>();
                for (int count = 1; count <= usefullife; count++)
                {
                    singledepAmount = Convert.ToInt32((Convert.ToDouble(percentage) / Convert.ToDouble(100)) * singlepurchaseprice);

                    accumulatedDepreciation = accumulatedDepreciation + singledepAmount;
                }

                for (int counter = 1; counter <= usefullife; counter++)
                {

                    mysingleWorthAmount = purchasePrice - singledepAmount;

                    int myYears = purchaseDate.Year;
                    mytheYears.Add(Years);
                    TheYears = Years + counter;
                    Years++;

                    //TheDepPercentage = TheDepPercentage;
                    //singledepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * singleWorthAmount);

                    int percent = Convert.ToInt32(percentage);
                    //checking whether worth amount == salvage value for automatic ready for disposal status
                    if ((singleWorthAmount <= salvageValue) || (singleWorthAmount == 0))
                    {
                        break;
                    }
                    theAssetList.Add(new Asset()
                    {
                        Year = TheYears,

                        DepPercentage = percent,

                        DepAmount = singledepAmount,

                        GrossAmount = singleWorthAmount,

                        DepreciationMethod = depMethod.ToString()

                    });

                }
                //ViewBag.Years = theYears;
                myasset.DepAmount = singledepAmount;
                myasset.GrossAmount = singleWorthAmount;
                myasset.accumulatedDepreciation = accumulatedDepreciation;
            }
            
            return theAssetList;
        }
    }
}