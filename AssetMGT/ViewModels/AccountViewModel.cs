﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetMGT.ViewModels
{
    public class AccountViewModel
    {
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Asset> Assets { get; set; }
        public IEnumerable<Employee> Employeese { get; set; }
        public IEnumerable<MaintenanceFrequency> MaintenanceFrequencies { get; set; }
        public IEnumerable<Supplier> Suppliers { get; set; }
        public IEnumerable<Customer> Customers { get; set; }
        public IEnumerable<AssetIndicator> AssetIndicators { get; set; }

        public IEnumerable<AssetClass> AssetClasses { get; set; }
        public IEnumerable<Currency> Currencies { get; set; }
      
        public IEnumerable<Depreciation> Depreciations { get; set; }

    }
}