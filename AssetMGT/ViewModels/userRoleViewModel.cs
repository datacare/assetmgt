﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AssetMGT.ViewModels
{
    public class UserRoleViewModel
    {
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Rolemodel> RoleModels { get; set; }

        public List<UserRegistering> UserRegisterings { get; set; }

        public UserRoleViewModel()
        {
            UserRegisterings = new List<UserRegistering>();
        }

        public List<UserRegistering> LoadRoles(AssetMGTContext assetMGTContext)
        {
            var dbUsers = assetMGTContext.Users.ToList();
            foreach (var user in dbUsers)
            {
                UserRegistering UserRegistering = new UserRegistering();
                UserRegistering.Name = user.Name;
                UserRegistering.Email = user.Email;
                UserRegistering.Password = user.Password;

                string roleName = UserRegistering.Name;

                UserRoling thisUserRolling = new UserRoling();
                thisUserRolling.RoleName = roleName;

                UserRegistering.AssetRoles.Add(thisUserRolling);

                UserRegisterings.Add(UserRegistering);
            }

            return UserRegisterings;
        }


        
    }
    public class UserRegistering
    {

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter valid email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Please enter a valid email")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Please enter password")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$", ErrorMessage = "Please enter a strong password")]
        public string Password { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Please re-enter the same password")]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string ConfirmPassword { get; set; }

        public virtual List<UserRoling> AssetRoles { get; set; }
        public UserRegistering()
        {
            AssetRoles = new List<UserRoling>();
        }
    }


    public class UserRoling
    {

        [Display(Name = "Role Name")]
        public string RoleName { get; set; }

    }
}