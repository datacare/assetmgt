﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssetMGT.ViewModels
{
    public class DisposalViewModel
    {
        //public IEnumerable<Asset> Assets { get; set; }
        public IEnumerable<Disposal> Disposals { get; set; }
        

        //enables to have a list of currencies in our viewmodel currencies

        public IEnumerable<SelectListItem> Currencies { get; set; }
        public IEnumerable<SelectListItem> Customers { get; set; }
        public List<Asset> Assets { get; set; }
        public int thePrice { get; set; }

        public IEnumerable<SelectListItem> CustomerId { get; set; }

        public IEnumerable<SelectListItem>  CurrencyId { get; set; }


}
}