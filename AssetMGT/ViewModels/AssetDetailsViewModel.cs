﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static AssetMGT.Data.AssetClass;

namespace AssetMGT.ViewModels
{
    public class AssetDetailsViewModel
    {
        public List<AssetRegister>  AssetRegisters { get; set; }

        public AssetDetailsViewModel() 
        {
            AssetRegisters = new List<AssetRegister>();
        }

        /*public List<AssetRegister> LoadAssets(AssetMGTContext assetMGTContext)
        {
            var dbAssets = assetMGTContext.Assets.ToList();

            foreach (var asset in dbAssets) 
            {
                AssetRegister AssetRegister = new AssetRegister();
                AssetRegister.AssetClass = asset.AssetClass.AssetClassName;
                AssetRegister.AssetCode = asset.AssetCode;
                AssetRegister.DepreciationMethod = asset.AssetClass.DepreciationMethod;

                if (AssetRegister.DepreciationMethod == DepMethods.StraightLineMethod)
                {
                    if (AssetRegister.EndDepreciationDate.HasValue)
                    {
                        int ForLife = AssetRegister.EndDepreciationDate.Value.Year - AssetRegister.AssetPurchaseDate.Year;
                        int Year = AssetRegister.AssetPurchaseDate.Year;
                        for (int count = 1; count <= ForLife; count++)
                        {
                            AssetDepreciation assetDepreciation = new AssetDepreciation();
                            assetDepreciation.Year = Year;
                            assetDepreciation.Percentage = asset.AssetClass.DepPercentage;

                            AssetRegister.AssetDepreciations.Add(assetDepreciation);
                            Year += 1;
                        }
                    }
                }

                AssetRegisters.Add(AssetRegister);
            }

            return AssetRegisters;
        }*/

        public AssetRegister LoadAssets(AssetMGTContext assetMGTContext, int Id) 
        {
            var dbAssets = assetMGTContext.Assets.Where(r => r.Id == Id).SingleOrDefault();
            AssetRegister AssetRegister = new AssetRegister();
            AssetRegister.AssetClass = dbAssets.AssetClass.AssetClassName;
            AssetRegister.AssetCode = dbAssets.AssetCode;
            AssetRegister.DepreciationMethod = dbAssets.AssetClass.DepreciationMethod ;

            if (AssetRegister.DepreciationMethod == DepMethods.StraightLineMethod) 
            {
                if (AssetRegister.EndDepreciationDate.HasValue) 
                {
                    int ForLife = AssetRegister.EndDepreciationDate.Value.Year - AssetRegister.AssetPurchaseDate.Year;
                    int Year = AssetRegister.AssetPurchaseDate.Year;
                    for (int count = 1; count <= ForLife; count++) 
                    {
                        AssetDepreciation assetDepreciation = new AssetDepreciation();
                        assetDepreciation.Year = Year;
                        assetDepreciation.Percentage = (int)dbAssets.AssetClass.DepPercentage;

                        AssetRegister.AssetDepreciations.Add(assetDepreciation);
                        Year += 1;
                    }
                }
            }

            return AssetRegister;
        }

    }

    public class AssetRegister 
    {
        public string AssetCode { get; set; }

        [Display(Name = "Asset Name")]
        public string AssetName { get; set; }

        [Display(Name = "Asset Quantity")]
        public string AssetQuantity { get; set; }

        [Display(Name = "Asset Status")]
        public string AssetStatus { get; set; }

        [Display(Name = "Asset Serial Number")]
        public string AssetSerialNumber { get; set; }

        [Display(Name = "Asset Engravement Number")]
        public string AssetEngravementNumber { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Purchase Date")]
        public DateTime AssetPurchaseDate { get; set; }



        [Display(Name = "Purchase Price")]
        public int AssetPurchasePrice { get; set; }

        [Display(Name = "Salvage Value")]
        public int? SalvageValue { get; set; }

        [Display(Name = "Comment")]
        public string AssetComment { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start Depreciation Date")]
        public DateTime? StartDepreciationDate { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Depreciation Date")]
        public DateTime? EndDepreciationDate { get; set; }

        [Display(Name = "Asset Class")]
        public string AssetClass { get; set; }

        [Display(Name = "Maintenance Frequency")]
        public string MaintenanceFrequency { get; set; }

        [Display(Name = "Employee")]
        public string Employee { get; set; }

        [Display(Name = "Supplier")]
        public string Supplier { get; set; }

        [Display(Name = "Asset Indicator")]
        public string AssetIndicator { get; set; }

        [Display(Name = "Depreciation Percentage")]
        public int DepreciationPercentage { get; set; }

        [Display(Name = "Depreciation Method")]
        public DepMethods DepreciationMethod { get; set; }

        public virtual List<AssetDepreciation> AssetDepreciations { get; set; }

        public AssetRegister() 
        {
            AssetDepreciations = new List<AssetDepreciation>();
        }

    }

    public class AssetDepreciation 
    {
        [Display(Name = "Supplier")]
        public int Year { get; set; }

        [Display(Name = "Asset Indicator")]
        public int Percentage { get; set; }

        [Display(Name = "Depreciation Percentage")]
        public int DepreciationAmount { get; set; }

        [Display(Name = "Depreciation Method")]
        public int  NetBookValue { get; set; }
    }

}