﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetMGT.ViewModels
{
    public class UserIndexData
    {

        public IEnumerable<Rolemodel> Roles { get; set; }
        public IEnumerable<User> Users { get; set; }
        //public IEnumerable<userRole> userRoles { get; set; }

    }
}