﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetMGT.ViewModels
{
    public class AssetViewModel
    {
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Asset> Assets { get; set; }

        public IEnumerable<Asset> Assetsa { get; set; }
        public IEnumerable<Disposal> Disposals { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
        public IEnumerable<MaintenanceFrequency> MaintenanceFrequencies { get; set; }
        public IEnumerable<Supplier> Suppliers { get; set; }
        public IEnumerable<AssetIndicator> AssetIndicators { get; set; }
        public IEnumerable<AssetClass> AssetClasses { get; set; }
        public string AssetClassName { get; set; }

        public IEnumerable<int> DepAmounts { get; set; }


        //about the single asset
        public string AssetName { get; set; }
        public List<int> DepreciationAmounts { get; set; }
        public List<int> Years { get; set; }
        public List<int> NetbookValues { get; set; }
    }
}