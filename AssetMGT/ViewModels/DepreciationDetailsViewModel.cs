﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static AssetMGT.Data.AssetClass;

namespace AssetMGT.ViewModels
{
    public class SingleAsset
    {
        public string AssetName { get; set; }
        public List<int> Depreciations { get; set; }
        public List<int> Years { get; set; }
        public List<int> NetbookValues { get; set; }
    }



    public class DepreciationDetailsViewModel
    {
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Asset> Assets { get; set; }
        public IEnumerable<Asset> Assetsa { get; set; }

        public string AssetClassName { get; set; }


        public List<AssetRegistering> AssetRegisterings { get; set; }

        public DepreciationDetailsViewModel()
        {
            AssetRegisterings = new List<AssetRegistering>();
        }

        public List<AssetRegistering> LoadAssets(AssetMGTContext assetMGTContext, string searchString, string assetClassId, string yearEntered)
        {
            var dbAssets = assetMGTContext.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier).ToList();
            
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = char.ToUpper(searchString[0]) + searchString.Substring(1);
                dbAssets = assetMGTContext.Assets.Where(d => d.AssetStatus == "Active").Where(s => s.AssetName.Contains(realName)).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier).ToList();
            }
            else if (!String.IsNullOrEmpty(assetClassId))
            {
                int theAssetClassId = Convert.ToInt32(assetClassId);
                // assetDetailsViewModel.AssetRegisterings = assetDetailsViewModel.AssetRegisterings.Where(s => s.AssetName.Contains(realName)).ToList();
                dbAssets = assetMGTContext.Assets.Where(d => d.AssetClassId == theAssetClassId).Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier).ToList();
            }
            else
            {
                dbAssets = assetMGTContext.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier).ToList();
            }
            if(dbAssets.Count == 0)
            {
                AssetRegistering AssetRegistering = new AssetRegistering();
                AssetRegisterings.Add(AssetRegistering);
                return new List<AssetRegistering>();
            }
            int yearInteger = Convert.ToInt32(yearEntered);
            
            foreach (var asset in dbAssets)
            {
                int dateToCheck = asset.StartDepreciationDate.Year;
                
                AssetRegistering AssetRegistering = new AssetRegistering();
                AssetRegistering.AssetClass = asset.AssetClass.AssetClassName;
                AssetRegistering.DepreciationMethod = asset.AssetClass.DepreciationMethod;
                AssetRegistering.AssetName = asset.AssetName;
                AssetRegistering.StartDepreciationDate = asset.StartDepreciationDate;
                AssetRegistering.DepreciationPercentage = (int)asset.AssetClass.DepPercentage;
                AssetRegistering.AssetPurchasePrice = asset.AssetPurchasePrice;
                AssetRegistering.AssetCode = asset.AssetCode;

                int minimum = AssetRegistering.StartDepreciationDate.Year;

                
                DateTime currentDate = DateTime.Now;
                DateTime startDepYear = AssetRegistering.StartDepreciationDate;

                //Checking whethet End Depreciation date has value 

                if (yearInteger == dateToCheck)
                {
                    if (asset.EndDepreciationDate != null)
                    {
                        currentDate = asset.EndDepreciationDate.GetValueOrDefault();
                    }

                    int usefullife = currentDate.Year - startDepYear.Year;

                    //if the method is reducingbalance
                    if (AssetRegistering.DepreciationMethod == DepMethods.ReducingBalanceMethod)
                    {
                        int singlepurchaseprice = asset.AssetPurchasePrice;
                        int singledepAmount = Convert.ToInt32((Convert.ToDouble(asset.AssetClass.DepPercentage) / Convert.ToDouble(100)) * singlepurchaseprice);

                        int depPerDayAmount = singledepAmount / 365;

                        int firstYear = startDepYear.Year;

                        DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
                        int numberofDays = (firstYearEndDate - startDepYear).Days;
                        int yearOneDepAmount = depPerDayAmount * numberofDays;
                        int WorthAmount = singlepurchaseprice - yearOneDepAmount;
                        int Years = yearInteger;

                        int accumulatedDepreciation = yearOneDepAmount;

                        int singleWorthAmount = asset.AssetPurchasePrice - yearOneDepAmount;

                        for (int c = minimum; c < Years; c++)
                        {
                            AssetDepreciating thisassetDepreciation = new AssetDepreciating();
                            thisassetDepreciation.Year = minimum;
                            thisassetDepreciation.Percentage = Convert.ToInt32(asset.AssetClass.DepPercentage);
                            thisassetDepreciation.DepreciationAmount = 0;
                            thisassetDepreciation.NetBookValue = 0;
                            thisassetDepreciation.AccumulatedDepreciation = 0;
                            AssetRegistering.AssetDepreciatings.Add(thisassetDepreciation);
                        }

                        for (int counter = 1; counter <= usefullife; counter++)
                        {
                            AssetDepreciating assetDepreciation = new AssetDepreciating();

                            int percent = Convert.ToInt32(asset.AssetClass.DepPercentage);
                            assetDepreciation.Year = Years;
                            assetDepreciation.Percentage = percent;
                            assetDepreciation.DepreciationAmount = yearOneDepAmount;
                            assetDepreciation.NetBookValue = singleWorthAmount;
                            assetDepreciation.AccumulatedDepreciation = accumulatedDepreciation;
                            AssetRegistering.AssetDepreciatings.Add(assetDepreciation);

                            yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(asset.AssetClass.DepPercentage) / Convert.ToDouble(100)) * singleWorthAmount);
                            accumulatedDepreciation += yearOneDepAmount;
                            singleWorthAmount = singleWorthAmount - yearOneDepAmount;

                            Years++;

                        }

                    }
                    else if(AssetRegistering.DepreciationMethod == DepMethods.StraightLineMethod)
                    {
                        int singlepurchaseprice = asset.AssetPurchasePrice;
                        int singledepAmount = Convert.ToInt32((Convert.ToDouble(asset.AssetClass.DepPercentage) / Convert.ToDouble(100)) * singlepurchaseprice);

                        int depPerDayAmount = singledepAmount / 365;

                        int firstYear = startDepYear.Year;

                        DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
                        int numberofDays = (firstYearEndDate - startDepYear).Days;
                        int yearOneDepAmount = depPerDayAmount * numberofDays;
                        int WorthAmount = singlepurchaseprice - yearOneDepAmount;
                        int Years = yearInteger;

                        int accumulatedDepreciation = yearOneDepAmount;

                        int singleWorthAmount = asset.AssetPurchasePrice - yearOneDepAmount;

                        for (int c = minimum; c < Years; c++)
                        {
                            AssetDepreciating thisassetDepreciation = new AssetDepreciating();
                            thisassetDepreciation.Year = minimum;
                            thisassetDepreciation.Percentage = Convert.ToInt32(asset.AssetClass.DepPercentage);
                            thisassetDepreciation.DepreciationAmount = 0;
                            thisassetDepreciation.NetBookValue = 0;
                            thisassetDepreciation.AccumulatedDepreciation = 0;
                            AssetRegistering.AssetDepreciatings.Add(thisassetDepreciation);
                        }

                        for (int counter = 1; counter <= usefullife; counter++)
                        {
                            AssetDepreciating assetDepreciation = new AssetDepreciating();

                            int percent = Convert.ToInt32(asset.AssetClass.DepPercentage);
                            assetDepreciation.Year = Years;
                            assetDepreciation.Percentage = percent;
                            assetDepreciation.DepreciationAmount = yearOneDepAmount;
                            assetDepreciation.NetBookValue = singleWorthAmount;
                            assetDepreciation.AccumulatedDepreciation = accumulatedDepreciation;
                            AssetRegistering.AssetDepreciatings.Add(assetDepreciation);

                            yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(asset.AssetClass.DepPercentage) / Convert.ToDouble(100)) * singleWorthAmount);
                            accumulatedDepreciation += yearOneDepAmount;

                            Years++;

                        }

                    }
                    AssetRegisterings.Add(AssetRegistering);
                }
            }
            return AssetRegisterings;
        }

        public List<AssetRegistering> LoadMonthlyAssets(AssetMGTContext assetMGTContext, string YearFilter, string assetClassId)
        {
            var dbAssets = assetMGTContext.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier).ToList();

            if (!String.IsNullOrEmpty(assetClassId))
            {
                int theAssetClassId = Convert.ToInt32(assetClassId);
                // assetDetailsViewModel.AssetRegisterings = assetDetailsViewModel.AssetRegisterings.Where(s => s.AssetName.Contains(realName)).ToList();
                dbAssets = assetMGTContext.Assets.Where(d => d.AssetClassId == theAssetClassId).Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier).ToList();

            }
            else
            {
                dbAssets = assetMGTContext.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier).ToList();

            }

            List<Asset> monthsCounter = new List<Asset>();
            DateTime thecurrentDate;
            int theNowYear;
            int theEnteredYear = DateTime.Now.Year;
            int? salvageValue;
            
            foreach (var myMonth in dbAssets)
            {
                AssetRegistering AssetRegistering = new AssetRegistering();
                AssetRegistering.AssetClass = myMonth.AssetClass.AssetClassName;
                AssetRegistering.DepreciationMethod = myMonth.AssetClass.DepreciationMethod;
                AssetRegistering.AssetName = myMonth.AssetName;
                AssetRegistering.StartDepreciationDate = myMonth.StartDepreciationDate;
                AssetRegistering.DepreciationPercentage = (int)myMonth.AssetClass.DepPercentage;
                AssetRegistering.AssetPurchasePrice = myMonth.AssetPurchasePrice;
                AssetRegistering.AssetCode = myMonth.AssetCode;
                if(myMonth.SalvageValue != null)
                {
                    salvageValue = myMonth.SalvageValue.GetValueOrDefault();
                }
                

                // Asset asset = new Asset();
                int singlepurchaseprice = myMonth.AssetPurchasePrice;
                int TheDepPercentage = (int)myMonth.AssetClass.DepPercentage;
                var depMethod = myMonth.AssetClass.DepreciationMethod;
                string DepreciationMethod = depMethod.ToString();

               
                DateTime startDepDate = myMonth.StartDepreciationDate;
                
                if (myMonth.EndDepreciationDate != null)
                {
                    thecurrentDate = myMonth.EndDepreciationDate.GetValueOrDefault();
                }
                else
                {
                    thecurrentDate = DateTime.UtcNow;
                }

                theNowYear = thecurrentDate.Year;
                int thePurchaseYear = startDepDate.Year;

                if (!String.IsNullOrEmpty(YearFilter))
                {
                    theEnteredYear = Convert.ToInt32(YearFilter);
                    theNowYear = theEnteredYear;
                    
                }
            
                int usefullife = theNowYear - thePurchaseYear;
                if (theNowYear > thePurchaseYear)
                {
                    if (AssetRegistering.DepreciationMethod == DepMethods.ReducingBalanceMethod)
                    {
                        int singledepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * singlepurchaseprice);
                        int depPerDayAmount = singledepAmount / 365;

                        int firstYear = startDepDate.Year;
                        DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
                        int numberofDays = (firstYearEndDate - startDepDate).Days;
                        int yearOneDepAmount = depPerDayAmount * numberofDays;
                        int singleWorthAmount = singlepurchaseprice - yearOneDepAmount;
                        int Years = startDepDate.Year;
                        int TheYears = 0;
                        int accumulatedDepreciation = yearOneDepAmount;

                        for (int counter = 1; counter <= (usefullife); counter++)
                        {
                            AssetDepreciating assetDepreciation = new AssetDepreciating();

                            TheYears = Years + counter;
                            if (TheYears == theEnteredYear)
                            {
                                assetDepreciation.DepreciationAmount = yearOneDepAmount;
                                assetDepreciation.NetBookValue = singleWorthAmount;
                                assetDepreciation.AccumulatedDepreciation = accumulatedDepreciation;
                                AssetRegistering.AssetDepreciatings.Add(assetDepreciation);

                            }
                            yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * singleWorthAmount);

                            singleWorthAmount = singleWorthAmount - yearOneDepAmount;
                            accumulatedDepreciation = accumulatedDepreciation + yearOneDepAmount;

                            if (myMonth.SalvageValue != null)
                            {
                                if ((singleWorthAmount <= myMonth.SalvageValue) || (singleWorthAmount == 0))
                                {
                                    break;

                                }
                            }

                        }
                        AssetRegisterings.Add(AssetRegistering);
                    }
                    else if(AssetRegistering.DepreciationMethod == DepMethods.ReducingBalanceMethod)
                    {
                        int singledepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * singlepurchaseprice);
                        int depPerDayAmount = singledepAmount / 365;

                        int firstYear = startDepDate.Year;
                        DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
                        int numberofDays = (firstYearEndDate - startDepDate).Days;
                        int yearOneDepAmount = depPerDayAmount * numberofDays;
                        int singleWorthAmount = singlepurchaseprice - yearOneDepAmount;
                        int Years = startDepDate.Year;
                        int TheYears = 0;
                        int accumulatedDepreciation = yearOneDepAmount;

                        for (int counter = 1; counter <= (usefullife); counter++)
                        {
                            AssetDepreciating assetDepreciation = new AssetDepreciating();

                            TheYears = Years + counter;
                            if (TheYears == theEnteredYear)
                            {
                                assetDepreciation.DepreciationAmount = yearOneDepAmount;
                                assetDepreciation.NetBookValue = singleWorthAmount;
                                assetDepreciation.AccumulatedDepreciation = accumulatedDepreciation;
                                AssetRegistering.AssetDepreciatings.Add(assetDepreciation);

                            }
                            yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * singleWorthAmount);

                            accumulatedDepreciation = accumulatedDepreciation + yearOneDepAmount;

                            if (myMonth.SalvageValue != null)
                            {
                                if ((singleWorthAmount <= myMonth.SalvageValue) || (singleWorthAmount == 0))
                                {
                                    break;

                                }
                            }

                        }
                        AssetRegisterings.Add(AssetRegistering);
                    }
                }
            }
            return AssetRegisterings;
        }
    }


    public class AssetRegistering
    {
        public string AssetCode { get; set; }

        [Display(Name = "Asset Name")]
        public string AssetName { get; set; }

        [Display(Name = "Asset Quantity")]
        public string AssetQuantity { get; set; }

        [Display(Name = "Asset Status")]
        public string AssetStatus { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Purchase Date")]
        public DateTime AssetPurchaseDate { get; set; }

        [Display(Name = "Purchase Price")]
        public int AssetPurchasePrice { get; set; }

        [Display(Name = "Salvage Value")]
        public int? SalvageValue { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start Depreciation Date")]
        public DateTime StartDepreciationDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "End Depreciation Date")]
        public DateTime? EndDepreciationDate { get; set; }

        [Display(Name = "Asset Class")]
        public string AssetClass { get; set; }
        //public IEnumerable<SelectListItem> AssetClass { get; set; }

        [Display(Name = "Maintenance Frequency")]
        public string MaintenanceFrequency { get; set; }


        [Display(Name = "Depreciation Percentage")]
        public int DepreciationPercentage { get; set; }

        [Display(Name = "Depreciation Method")]
        public DepMethods DepreciationMethod { get; set; }

        public AssetClass assetClass { get; set; }

        public virtual List<AssetDepreciating> AssetDepreciatings { get; set; }
        public AssetRegistering()
        {
            AssetDepreciatings = new List<AssetDepreciating>();
        }
    }


    public class AssetDepreciating
    {
        [Display(Name = "Year")]
        public int Year { get; set; }

        [Display(Name = "Dep %")]
        public int Percentage { get; set; }

        [Display(Name = "Depreciation Amount")]
        public int DepreciationAmount { get; set; }

        [Display(Name = "NetBookValue")]
        public int NetBookValue { get; set; }

        [Display(Name = "Accumulated Depreciation")]
        public int AccumulatedDepreciation { get; set; }


    }
    
    
}
    

