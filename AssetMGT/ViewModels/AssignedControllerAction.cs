﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetMGT.ViewModels
{
    public class AssignedControllerAction
    {
        public int ControllerActionId { get; set; }
        public ControllerName ControllerName { get; set; }
        public ActionName ActionName { get; set; }
        public bool Assigned { get; set; }
    }
}