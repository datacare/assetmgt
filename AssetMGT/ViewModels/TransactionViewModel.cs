﻿using AssetMGT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetMGT.ViewModels
{
    public class TransactionViewModel
    {
        public IEnumerable<Transaction> Transactions { get; set; }
    }
}