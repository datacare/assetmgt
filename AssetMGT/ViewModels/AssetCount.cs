﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetMGT.ViewModels
{
    public class AssetCount
    {
        public string AssetClassName { get; set; }
        public string AssetNumber { get; set; }
    }
}