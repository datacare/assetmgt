﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using PagedList;

namespace AssetMGT.Controllers.AssetLookups
{

    public class SupplierController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: Supplier
        [DynamicRoleAuthorize(ControllerName.Supplier, ActionName.Index)]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? thepageSize, string CurrencyId)
        {

            //to enable search, page and sort
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SupplierNameSorting = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CurrencyNameSorting = sortOrder == "CurrencyName" ? "CurrencyName_desc" : "CurrencyName";
            ViewBag.SupplierEmailSorting = sortOrder == "SupplierEmail" ? "SupplierEmail_desc" : "SupplierEmail";

            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var viewModel = new AccountViewModel();
            viewModel.Suppliers = db.Suppliers.Include(s => s.Currency);

            //***********************Genesis of Filtering************************//

            //if ((!String.IsNullOrEmpty(CurrencyId)))
            //{
            //Supplier supplier = new Supplier();

            //var theCurrency = supplier.Currency;


            if (!String.IsNullOrEmpty(CurrencyId))
            {
                viewModel.Suppliers = db.Suppliers.Where(d => d.Currency.CurrencyName == CurrencyId);

            }
            else
            {
                viewModel.Suppliers = db.Suppliers.Include(s => s.Currency);

            }

            //else
            //{
            //    viewModel.AssetClasses = db.AssetClasses.Where(d => d.assetClassStatus == status);

            //}

            ViewBag.LocationFilter = CurrencyId;




            //***********************Revelation of Filtering ********************//

            //searching functionality starts here
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = char.ToUpper(searchString[0]) + searchString.Substring(1);
                viewModel.Suppliers = viewModel.Suppliers.Where(s => s.SupplierName.Contains(realName));

                ViewBag.SearchedItem = searchString;
            }

            //sort format
            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.Suppliers = viewModel.Suppliers.OrderByDescending(s => s.SupplierName);
                    break;
                case "CurrencyName":
                    viewModel.Suppliers = viewModel.Suppliers.OrderBy(s => s.Currency.CurrencyName);
                    break;
                case "CurrencyName_desc":
                    viewModel.Suppliers = viewModel.Suppliers.OrderByDescending(s => s.Currency.CurrencyName);
                    break;

                case "SupplierEmail":
                    viewModel.Suppliers = viewModel.Suppliers.OrderBy(s => s.SupplierEmail);
                    break;
                case "SupplierEmail_desc":
                    viewModel.Suppliers = viewModel.Suppliers.OrderByDescending(s => s.SupplierEmail);
                    break;

                default:
                    viewModel.Suppliers = viewModel.Suppliers.OrderBy(s => s.SupplierName);
                    break;

            }

            ViewBag.CurrencyId = new SelectList(db.Suppliers.Include(s => s.Currency).Select(y => y.Currency.CurrencyName).Distinct());

            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;

            return View(viewModel.Suppliers.ToPagedList(pageNumber, pageSize));


        }

        // GET: Supplier/Details/5
        [DynamicRoleAuthorize(ControllerName.Supplier, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // GET: Supplier/Create
        [DynamicRoleAuthorize(ControllerName.Supplier, ActionName.Create)]
        public ActionResult Create()
        {
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName");
            return View();
        }

        // POST: Supplier/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Supplier, ActionName.Create)]
        public ActionResult Create([Bind(Include = "Id,SupplierName,SupplierTelephone,SupplierEmail,CurrencyId")] Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                db.Suppliers.Add(supplier);
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created A Supplier";
                string description = "A Supplier whose Email Address is " + supplier.SupplierEmail + " was created";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//
                return RedirectToAction("Index");
            }

            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", supplier.CurrencyId);
            return View(supplier);
        }

        // GET: Supplier/Edit/5
        [DynamicRoleAuthorize(ControllerName.Supplier, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", supplier.CurrencyId);
            return View(supplier);
        }

        // POST: Supplier/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Supplier, ActionName.Edit)]
        public ActionResult Edit([Bind(Include = "Id,SupplierName,SupplierTelephone,SupplierEmail,CurrencyId")] Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                db.Entry(supplier).State = EntityState.Modified;
                db.SaveChanges();


                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Edited A Supplier";
                string description = "A Supplier whose Email Address is " + supplier.SupplierEmail + " was edited";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//
                return RedirectToAction("Index");
            }
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", supplier.CurrencyId);
            return View(supplier);
        }

        // GET: Supplier/Delete/5
        [DynamicRoleAuthorize(ControllerName.Supplier, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // POST: Supplier/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Supplier, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            Supplier supplier = db.Suppliers.Find(id);
            db.Suppliers.Remove(supplier);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
