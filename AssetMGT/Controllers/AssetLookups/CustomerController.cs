﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using PagedList;

namespace AssetMGT.Controllers.AssetLookups
{
    public class CustomerController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: Customers
        [DynamicRoleAuthorize(ControllerName.Customer, ActionName.Index)]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? thepageSize)
        {
            //to enable search, page and sort
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CustomerNameSorting = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CustomerEmailSorting = sortOrder == "CustomerEmail" ? "CustomerEmail_desc" : "CustomerEmail";
            ViewBag.CustomerAddressSorting = sortOrder == "CustomerAddress" ? "CustomerAddress_desc" : "CustomerAddress";

            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var viewModel = new AccountViewModel();
            viewModel.Customers = db.Customers;

            //searching functionality starts here
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = char.ToUpper(searchString[0]) + searchString.Substring(1);
                viewModel.Customers = viewModel.Customers.Where(s => s.CustomerName.Contains(realName));
                ViewBag.SearchedItem = searchString;
            }

            //sort format
            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.Customers = viewModel.Customers.OrderByDescending(s => s.CustomerName);
                    break;
                case "CustomerEmail":
                    viewModel.Customers = viewModel.Customers.OrderBy(s => s.CustomerEmail);
                    break;
                case "CustomerEmail_desc":
                    viewModel.Customers = viewModel.Customers.OrderByDescending(s => s.CustomerEmail);
                    break;
                case "CustomerAddress":
                    viewModel.Customers = viewModel.Customers.OrderBy(s => s.CustomerAddress);
                    break;
                case "CustomerAddress_desc":
                    viewModel.Customers = viewModel.Customers.OrderByDescending(s => s.CustomerAddress);
                    break;
                default:
                    viewModel.Customers = viewModel.Customers.OrderBy(s => s.CustomerName);
                    break;

            }

            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;


            return View(viewModel.Customers.ToPagedList(pageNumber, pageSize));


        }

        // GET: Customers/Details/5
        [DynamicRoleAuthorize(ControllerName.Customer, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customers/Create
        [DynamicRoleAuthorize(ControllerName.Customer, ActionName.Create)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Customer, ActionName.Create)]
        public ActionResult Create([Bind(Include = "Id,CustomerName,CustomerTelephone,CustomerEmail,CustomerAddress")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Customers.Add(customer);
                db.SaveChanges();


                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created A Customer";
                string description = "A Customer whose Name is " + customer.CustomerName + " was created";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }

            return View(customer);
        }

        // GET: Customers/Edit/5
        [DynamicRoleAuthorize(ControllerName.Customer, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Customer, ActionName.Edit)]
        public ActionResult Edit([Bind(Include = "Id,CustomerName,CustomerTelephone,CustomerEmail,CustomerAddress")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created A Customer";
                string description = "A Customer whose Name is " + customer.CustomerName + " was edited";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }
            return View(customer);
        }

        // GET: Customers/Delete/5
        [DynamicRoleAuthorize(ControllerName.Customer, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Customer, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
