﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using PagedList;
using static AssetMGT.Data.Employee;

namespace AssetMGT.Controllers.AssetLookups
{
    public class EmployeeController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: Employee
        [DynamicRoleAuthorize(ControllerName.Employee, ActionName.Index)]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? thepageSize, string GenderId)
        {
            //to enable search, page and sort
            ViewBag.CurrentSort = sortOrder;
            ViewBag.EmpNameSorting = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.LocationSorting = sortOrder == "EmployeeId" ? "EmployeeId_desc" : "EmployeeId";
            ViewBag.GenderSorting = sortOrder == "Gender" ? "Gender_desc" : "Gender";
            ViewBag.EmployeeNumberSorting = sortOrder == "EmployeeNumber" ? "EmployeeNumber_desc" : "EmployeeNumber";
            ViewBag.EmailSorting = sortOrder == "Email" ? "Email_desc" : "Email";
            
                
            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }


            var viewModel = new AccountViewModel();
            viewModel.Employeese = db.Employees;

            //***********************Genesis of Filtering************************//

            if ((!String.IsNullOrEmpty(GenderId)))
            {
                Gender gender = Gender.Male;

                if (GenderId == "Male")
                {
                    gender = Gender.Male;
                }
                else
                {
                    gender = Gender.Female;
                }
                viewModel.Employeese = db.Employees.Where(d => d.gender == gender);
                
                ViewBag.GenderFilter = gender;

            }

            else
            {
                viewModel.Employeese = db.Employees;
            }
            //***********************Revelation of Filtering ********************//

            //searching functionality starts here
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = searchString.ToUpper();
                //viewModel.AssetClasses = viewModel.AssetClasses.Where(s => s.AssetClassCode.ToUpper().Contains(realName) || s.AssetClassName.ToUpper().Contains(realName));

                // viewModel.Employees = viewModel.Employees.Where(s => s.FirstName.ToUpper().Contains(realName) || s.LastName.ToUpper().Contains(realName) || s.EmployeeNumber.ToUpper().Contains(realName)|| s.Email.ToUpper().Contains(realName));

                try
                {
                   // viewModel.Employeese = viewModel.Employeese.Where(s => s.FirstName.ToUpper().Contains(realName));
                     viewModel.Employeese = viewModel.Employeese.Where(s => s.FirstName.ToUpper().Contains(realName) || s.LastName.ToUpper().Contains(realName));

                    // viewModel.Employees = viewModel.Employees.Where(s => s.EmployeeNumber.ToUpper().Contains(realName));

                }
                catch (Exception e)
                {

                }

                ViewBag.SearchedItem = searchString;
            }

            //sort format
            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.Employeese = viewModel.Employeese.OrderByDescending(s => s.FirstName);
                    break;
                case "EmployeeId":
                    viewModel.Employeese = viewModel.Employeese.OrderBy(s => s.EmployeeNumber);
                    break;
                case "EmployeeId_desc":
                    viewModel.Employeese = viewModel.Employeese.OrderByDescending(s => s.EmployeeNumber);
                    break;
                case "Gender":
                    viewModel.Employeese = viewModel.Employeese.OrderBy(s => s.gender);
                    break;
                case "Gender_desc":
                    viewModel.Employeese = viewModel.Employeese.OrderByDescending(s => s.gender);
                    break;
                case "EmployeeNumber":
                    viewModel.Employeese = viewModel.Employeese.OrderBy(s => s.EmployeeNumber);
                    break;
                case "EmployeeNumber_desc":
                    viewModel.Employeese = viewModel.Employeese.OrderByDescending(s => s.EmployeeNumber);
                    break;
                case "Email":
                    viewModel.Employeese = viewModel.Employeese.OrderBy(s => s.Email);
                    break;
                case "Email_desc":
                    viewModel.Employeese = viewModel.Employeese.OrderByDescending(s => s.Email);
                    break;

                default:
                    viewModel.Employeese = viewModel.Employeese.OrderBy(s => s.FirstName);
                    break;

            }

            ViewBag.GenderId = new SelectList(db.Employees.Select(y => y.gender).Distinct());

            ViewBag.numEmployees = viewModel.Employeese.Count();
            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;

            return View(viewModel.Employeese.ToPagedList(pageNumber, pageSize));

            //return View(db.Employees.ToList());
        }

        // GET: Employee/Details/5
        [DynamicRoleAuthorize(ControllerName.Employee, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employee/Create
        [DynamicRoleAuthorize(ControllerName.Employee, ActionName.Create)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Employee, ActionName.Create)]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created An Employee";
                string description = "An Employee whose Name is " + employee.FirstName +" "+employee.LastName + " was created";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }

            return View(employee);
        }

        // GET: Employee/Edit/5
        [DynamicRoleAuthorize(ControllerName.Employee, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Employee, ActionName.Edit)]
        public ActionResult Edit(Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created An Employee";
                string description = "An Employee whose Name is " + employee.FirstName +" "+ employee.LastName+" was Edited";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }
            return View(employee);
        }

        // GET: Employee/Delete/5
        [DynamicRoleAuthorize(ControllerName.Employee, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Employee, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
