﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using PagedList;
using static AssetMGT.Data.Currency;

namespace AssetMGT.Controllers.AssetLookups
{

    public class CurrencyController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: Currency
        [DynamicRoleAuthorize(ControllerName.Currency, ActionName.Index)]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? thepageSize)
        {
            //to enable search, page and sort
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrencyNameSorting = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CurrencyCodeSorting = sortOrder == "CurrencyCode" ? "CurrencyCode_desc" : "CurrencyCode";
            ViewBag.CountrySorting = sortOrder == "Country" ? "Country_desc" : "Country";
            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }



            var viewModel = new AccountViewModel();
            viewModel.Currencies = db.Currencies;


            //searching functionality starts here
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = char.ToUpper(searchString[0]) + searchString.Substring(1);
                viewModel.Currencies = viewModel.Currencies.Where(s => s.CurrencyName.Contains(realName));
                ViewBag.SearchedItem = searchString;
            }

            //sort format
            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.Currencies = viewModel.Currencies.OrderByDescending(s => s.CurrencyName);
                    break;
                case "CurrencyCode":
                    viewModel.Currencies = viewModel.Currencies.OrderBy(s => s.CurrencyCode);
                    break;
                case "CurrencyCode_desc":
                    viewModel.Currencies = viewModel.Currencies.OrderByDescending(s => s.CurrencyCode);
                    break;
                case "Country":
                    viewModel.Currencies = viewModel.Currencies.OrderBy(s => s.Country);
                    break;
                case "Country_desc":
                    viewModel.Currencies = viewModel.Currencies.OrderByDescending(s => s.Country);
                    break;

                default:
                    viewModel.Currencies = viewModel.Currencies.OrderBy(s => s.CurrencyName);
                    break;

            }

            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;

            return View(viewModel.Currencies.ToPagedList(pageNumber, pageSize));
        }

        // GET: Currency/Details/5
        [DynamicRoleAuthorize(ControllerName.Currency, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Currency currency = db.Currencies.Find(id);
            if (currency == null)
            {
                return HttpNotFound();
            }
            return View(currency);
        }

        // GET: Currency/Create
        [DynamicRoleAuthorize(ControllerName.Currency, ActionName.Create)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Currency/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Currency, ActionName.Create)]
        public ActionResult Create([Bind(Include = "Id,CurrencyName,CurrencyCode,Country")] Currency currency)
        {
            if (ModelState.IsValid)
            {
                db.Currencies.Add(currency);
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created A Currency";
                string description = "A Currency whose Currency Code is " + currency.CurrencyCode + " and Currency Name is " + currency.CurrencyName + " was created";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }

            return View(currency);
        }

        // GET: Currency/Edit/5
        [DynamicRoleAuthorize(ControllerName.Currency, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Currency currency = db.Currencies.Find(id);
            if (currency == null)
            {
                return HttpNotFound();
            }
            return View(currency);
        }

        // POST: Currency/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Currency, ActionName.Edit)]
        public ActionResult Edit([Bind(Include = "Id,CurrencyName,CurrencyCode,Country")] Currency currency)
        {
            if (ModelState.IsValid)
            {
                db.Entry(currency).State = EntityState.Modified;
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Edited A Currency";
                string description = "A Currency whose Currency Code is " + currency.CurrencyCode + " and Currency Name is " + currency.CurrencyName + " was edited";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }
            return View(currency);
        }

        // GET: Currency/Delete/5
        [DynamicRoleAuthorize(ControllerName.Currency, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Currency currency = db.Currencies.Find(id);
            if (currency == null)
            {
                return HttpNotFound();
            }
            return View(currency);
        }

        // POST: Currency/Delete/5
        [HttpPost, ActionName("Delete")]
        [DynamicRoleAuthorize(ControllerName.Currency, ActionName.Delete)]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Currency currency = db.Currencies.Find(id);
            db.Currencies.Remove(currency);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
