﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using PagedList;

namespace AssetMGT.Controllers.AssetLookups
{

    public class AssetIndicatorController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: AssetIndicator
        [DynamicRoleAuthorize(ControllerName.AssetIndicator, ActionName.Index)]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? thepageSize, EntityCategory Grouping)
        {
            //to enable search, page and sort
            ViewBag.CurrentSort = sortOrder;
            ViewBag.AssetIndicatorSorting = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }


            var viewModel = new AccountViewModel();
            viewModel.AssetIndicators = db.AssetIndicators.Where(s=>s.Grouping == Grouping);
            ViewBag.Grouping = Grouping;

            //searching functionality starts here
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = char.ToUpper(searchString[0]) + searchString.Substring(1);
                viewModel.AssetIndicators = viewModel.AssetIndicators.Where(s => s.AssetIndicatorName.Contains(realName));
                ViewBag.SearchedItem = searchString;
            }

            //sort format
            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.AssetIndicators = viewModel.AssetIndicators.OrderByDescending(s => s.AssetIndicatorName);
                    break;


                default:
                    viewModel.AssetIndicators = viewModel.AssetIndicators.OrderBy(s => s.AssetIndicatorName);
                    break;

            }

            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;


            return View(viewModel.AssetIndicators.ToPagedList(pageNumber, pageSize));
        }

        // GET: AssetIndicator/Details/5
        [DynamicRoleAuthorize(ControllerName.AssetIndicator, ActionName.Details)]
        public ActionResult Details(int? id, EntityCategory Grouping)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetIndicator assetIndicator = db.AssetIndicators.Find(id);
            if (assetIndicator == null)
            {
                return HttpNotFound();
            }

            ViewBag.Grouping = Grouping;
            return View(assetIndicator);
        }

        // GET: AssetIndicator/Create
        [DynamicRoleAuthorize(ControllerName.AssetIndicator, ActionName.Create)]
        public ActionResult Create(EntityCategory Grouping)
        {
            ViewBag.Grouping = Grouping;
            AssetIndicator assetIndicator = new AssetIndicator();
            assetIndicator.Grouping = Grouping;
            return View(assetIndicator);
        }

        // POST: AssetIndicator/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.AssetIndicator, ActionName.Create)]
        public ActionResult Create(AssetIndicator assetIndicator)
        {
            if (ModelState.IsValid)
            {
                
                db.AssetIndicators.Add(assetIndicator);
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created An Asset Indicator";
                string description = "An Asset Class whose Asset Indicator Name is " + assetIndicator.AssetIndicatorName + " was created";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index", new {assetIndicator.Grouping });
            }

            return View(assetIndicator);
        }

        // GET: AssetIndicator/Edit/5
        [DynamicRoleAuthorize(ControllerName.AssetIndicator, ActionName.Edit)]
        public ActionResult Edit(int? id, EntityCategory Grouping)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetIndicator assetIndicator = db.AssetIndicators.Find(id);
           // AssetIndicator indicator = db.AssetIndicators.Where(x => x.Grouping == Grouping && x.Id == id).SingleOrDefault();
            ViewBag.Grouping = assetIndicator.Grouping;

           // assetIndicator.Grouping = Grouping;
            if (assetIndicator == null)
            {
                return HttpNotFound();
            }
            return View(assetIndicator);
        }

        // POST: AssetIndicator/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.AssetIndicator, ActionName.Edit)]
        public ActionResult Edit(AssetIndicator assetIndicator)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(assetIndicator).State = EntityState.Modified;
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Edited An Asset Indicator";
                string description = "An Asset Indicator whose Asset Indicator Name is " + assetIndicator.AssetIndicatorName + " was created";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//


                return RedirectToAction("Index", new { assetIndicator.Grouping });
            }
            return View(assetIndicator);
        }

        // GET: AssetIndicator/Delete/5
        [DynamicRoleAuthorize(ControllerName.AssetIndicator, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetIndicator assetIndicator = db.AssetIndicators.Find(id);
            if (assetIndicator == null)
            {
                return HttpNotFound();
            }
            return View(assetIndicator);
        }

        // POST: AssetIndicator/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.AssetIndicator, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            AssetIndicator assetIndicator = db.AssetIndicators.Find(id);
            db.AssetIndicators.Remove(assetIndicator);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
