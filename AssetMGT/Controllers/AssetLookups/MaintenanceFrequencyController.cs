﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using PagedList;

namespace AssetMGT.Controllers.AssetLookups
{

    public class MaintenanceFrequencyController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: MaintenanceFrequency
        [DynamicRoleAuthorize(ControllerName.MaintenanceFrequency, ActionName.Index)]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? thepageSize)
        {
            //to enable search, page and sort
            ViewBag.CurrentSort = sortOrder;
            ViewBag.FrequencyNameSorting = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var viewModel = new AccountViewModel();
            viewModel.MaintenanceFrequencies = db.MaintenanceFrequencies;

            //searching functionality starts here
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = char.ToUpper(searchString[0]) + searchString.Substring(1);
                viewModel.MaintenanceFrequencies = viewModel.MaintenanceFrequencies.Where(s => s.TheFrequency.Contains(realName));
                ViewBag.SearchedItem = searchString;
            }

            //sort format
            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.MaintenanceFrequencies = viewModel.MaintenanceFrequencies.OrderByDescending(s => s.TheFrequency);
                    break;


                default:
                    viewModel.MaintenanceFrequencies = viewModel.MaintenanceFrequencies.OrderBy(s => s.TheFrequency);
                    break;

            }

            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;


            return View(viewModel.MaintenanceFrequencies.ToPagedList(pageNumber, pageSize));
        }

        // GET: MaintenanceFrequency/Details/5
        [DynamicRoleAuthorize(ControllerName.MaintenanceFrequency, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaintenanceFrequency maintenanceFrequency = db.MaintenanceFrequencies.Find(id);
            if (maintenanceFrequency == null)
            {
                return HttpNotFound();
            }
            return View(maintenanceFrequency);
        }

        // GET: MaintenanceFrequency/Create
        [DynamicRoleAuthorize(ControllerName.MaintenanceFrequency, ActionName.Create)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: MaintenanceFrequency/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.MaintenanceFrequency, ActionName.Create)]
        public ActionResult Create([Bind(Include = "Id,TheFrequency")] MaintenanceFrequency maintenanceFrequency)
        {
            if (ModelState.IsValid)
            {
                db.MaintenanceFrequencies.Add(maintenanceFrequency);
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created A Maintenance Frequency";
                string description = "A Maintenance Frequency whose Name is " + maintenanceFrequency.TheFrequency + " was created";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }

            return View(maintenanceFrequency);
        }

        // GET: MaintenanceFrequency/Edit/5
        [DynamicRoleAuthorize(ControllerName.MaintenanceFrequency, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaintenanceFrequency maintenanceFrequency = db.MaintenanceFrequencies.Find(id);
            if (maintenanceFrequency == null)
            {
                return HttpNotFound();
            }
            return View(maintenanceFrequency);
        }

        // POST: MaintenanceFrequency/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.MaintenanceFrequency, ActionName.Edit)]
        public ActionResult Edit([Bind(Include = "Id,TheFrequency")] MaintenanceFrequency maintenanceFrequency)
        {
            if (ModelState.IsValid)
            {
                db.Entry(maintenanceFrequency).State = EntityState.Modified;
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Edited A Maintenance Frequency";
                string description = "A Maintenance Frequency whose Name is " + maintenanceFrequency.TheFrequency + " was edited";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }
            return View(maintenanceFrequency);
        }

        // GET: MaintenanceFrequency/Delete/5
        [DynamicRoleAuthorize(ControllerName.MaintenanceFrequency, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaintenanceFrequency maintenanceFrequency = db.MaintenanceFrequencies.Find(id);
            if (maintenanceFrequency == null)
            {
                return HttpNotFound();
            }
            return View(maintenanceFrequency);
        }

        // POST: MaintenanceFrequency/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.MaintenanceFrequency, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            MaintenanceFrequency maintenanceFrequency = db.MaintenanceFrequencies.Find(id);
            db.MaintenanceFrequencies.Remove(maintenanceFrequency);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
