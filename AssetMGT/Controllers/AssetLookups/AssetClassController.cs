﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using PagedList;
using static AssetMGT.Data.AssetClass;

namespace AssetMGT.Controllers.AssetLookups
{
    public class AssetClassController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: AssetClass
        [DynamicRoleAuthorize(ControllerName.AssetClass, ActionName.Index)]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? id, string assetStatusId, string assetClassId, string textStartDate, string textEndDate, int? thepageSize)
        {
            //to enable search, page and sort
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSorting = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ClassCodeSorting = sortOrder == "ClassCode" ? "ClassCode_desc" : "ClassCode";
            ViewBag.DepMethodSorting = sortOrder == "DepMethod" ? "DepMethod_desc" : "DepMethod";
            ViewBag.DepPercentageSorting = sortOrder == "DepPercentage" ? "DepPercentage_desc" : "DepPercentage";
            ViewBag.ClassStatusSorting = sortOrder == "ClassStatus" ? "ClassStatus_desc" : "ClassStatus";
            ViewBag.CurrentFilter = searchString;
            
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            

            var viewModel = new AccountViewModel();
            viewModel.AssetClasses = db.AssetClasses;

            //***********************Genesis of Filtering************************//

            if ((!String.IsNullOrEmpty(assetStatusId)) || (!String.IsNullOrEmpty(assetClassId)))
            {
                
                AssetClassStatuses status = AssetClassStatuses.Open;
                
                if (assetStatusId == "Open")
                {
                    status = AssetClassStatuses.Open;
                }
                else
                {
                    status = AssetClassStatuses.Open;
                }
                if (!String.IsNullOrEmpty(assetClassId))
                {
                    viewModel.AssetClasses = db.AssetClasses.Where(d => d.assetClassStatus == status && d.AssetClassName == assetClassId);

                }
                else
                {
                    viewModel.AssetClasses = db.AssetClasses.Where(d => d.assetClassStatus == status);

                }

                ViewBag.assetClassFilter = assetClassId;
                ViewBag.assetStatusFilter = assetStatusId;
            }

            else
            {
                viewModel.AssetClasses = db.AssetClasses;
            }
            //***********************Revelation of Filtering ********************//

            //searching functionality starts here
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = searchString.ToUpper();
                viewModel.AssetClasses = viewModel.AssetClasses.Where(s => s.AssetClassCode.ToUpper().Contains(realName) || s.AssetClassName.ToUpper().Contains(realName));

               // viewModel.AssetClasses = viewModel.AssetClasses.Where(s => s.AssetClassName.Contains(realName));
                ViewBag.SearchedItem = searchString;
            }
            //sort format
            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderByDescending(s => s.AssetClassName);
                    break;
                case "ClassCode":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderBy(s => s.AssetClassName);
                    break;
                case "ClassCode_desc":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderByDescending(s => s.AssetClassName);
                    break;
                case "DepMethod":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderBy(s => s.DepreciationMethod);
                    break;
                case "DepMethod_desc":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderByDescending(s => s.DepreciationMethod);
                    break;
                case "DepPercentage":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderBy(s => s.DepPercentage);
                    break;
                case "DepPercentage_desc":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderByDescending(s => s.DepPercentage);
                    break;
                case "ClassStatus":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderBy(s => s.assetClassStatus);
                    break;
                case "ClassStatus_desc":
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderByDescending(s => s.assetClassStatus);
                    break;
                default:
                    viewModel.AssetClasses = viewModel.AssetClasses.OrderBy(s => s.AssetClassName);
                    break;

            }

            ViewBag.assetClassId = new SelectList(db.AssetClasses.Select(y => y.AssetClassName).Distinct());

            ViewBag.assetStatusId = new SelectList(db.AssetClasses.Select(y => y.assetClassStatus).Distinct());

            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;

            return View(viewModel.AssetClasses.ToPagedList(pageNumber, pageSize));
        }

        // GET: AssetClass/Details/5
        [DynamicRoleAuthorize(ControllerName.AssetClass, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetClass assetClass = db.AssetClasses.Find(id);
            if (assetClass == null)
            {
                return HttpNotFound();
            }
            return View(assetClass);
        }

        // GET: AssetClass/Create
        [DynamicRoleAuthorize(ControllerName.AssetClass, ActionName.Create)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: AssetClass/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.AssetClass, ActionName.Create)]
        public ActionResult Create([Bind(Include = "Id,AssetClassCode,AssetClassName,AssetClassStatus,DepPercentage,DepreciationMethod")] AssetClass assetClass)
        {
            
            
                if (ModelState.IsValid)
                {

                List<AssetClass> assetClassList = new List<AssetClass>();
                assetClassList = db.AssetClasses.Where(s=>s.AssetClassName == assetClass.AssetClassName).ToList();
                if (assetClassList.Count > 0)
                {
                    ModelState.AddModelError(string.Empty, "The Asset Class Entered already exists");
                    return View(assetClass);
                }

                var lastAssetClass = db.AssetClasses.OrderByDescending(c => c.AssetClassCode).FirstOrDefault();
                    if (lastAssetClass == null)
                    {
                        assetClass.AssetClassCode = "ACC001";
                        assetClass.DepreciationMethod = assetClass.DepreciationMethod;
                        assetClass.DepPercentage = assetClass.DepPercentage;

                    }
                    else
                    {//generating A random Asst class code

                        Random random = new Random();
                        assetClass.AssetClassCode = "ACC" + random.Next(1000, 1200);
                        assetClass.DepreciationMethod = assetClass.DepreciationMethod;
                        assetClass.DepPercentage = assetClass.DepPercentage;
                    }
                    db.AssetClasses.Add(assetClass);
                    db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;                
                string action = "Created An Asset Class";
                string description = "An Asset Class whose Asset Class Code is " + assetClass.AssetClassCode + " and Asset Class Name " + assetClass.AssetClassName + " was created";
                 ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index","AssetClass");
                }

            
            

            return View(assetClass);
        }

        // GET: AssetClass/Edit/5
        [DynamicRoleAuthorize(ControllerName.AssetClass, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetClass assetClass = db.AssetClasses.Find(id);
            if (assetClass == null)
            {
                return HttpNotFound();
            }
            return View(assetClass);
        }

        // POST: AssetClass/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.AssetClass, ActionName.Edit)]
        public ActionResult Edit([Bind(Include = "Id,AssetClassCode,AssetClassName,AssetClassStatus,DepPercentage,DepreciationMethod")] AssetClass assetClass)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assetClass).State = EntityState.Modified;
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;              
                string action = "Edited An Asset Class";
                string description = "An Asset Class whose Asset Class Code is " + assetClass.AssetClassCode + " and Asset Class Name " + assetClass.AssetClassName + " has been edited";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }
            return View(assetClass);
        }

        // GET: AssetClass/Delete/5
        [DynamicRoleAuthorize(ControllerName.AssetClass, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetClass assetClass = db.AssetClasses.Find(id);
            if (assetClass == null)
            {
                return HttpNotFound();
            }
            return View(assetClass);
        }

        // POST: AssetClass/Delete/5
        [HttpPost, ActionName("Delete")]
        [DynamicRoleAuthorize(ControllerName.AssetClass, ActionName.Delete)]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AssetClass assetClass = db.AssetClasses.Find(id);
            db.AssetClasses.Remove(assetClass);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpPost]
        public ActionResult CreateAssetClassOntheFly(AssetClass assetClass, string AssetClassName, AssetClassStatuses AssetClassStatus, DepMethods DepreciationMethod, string DepPercentage)
        {
            if (AssetClassName != null)
            {
                var lastAssetClass = db.AssetClasses.OrderByDescending(c => c.AssetClassCode).FirstOrDefault();
                if (lastAssetClass == null)
                {
                    assetClass.AssetClassCode = "ACC001";
                    assetClass.AssetClassName = AssetClassName;
                    assetClass.assetClassStatus = AssetClassStatus;
                    assetClass.DepreciationMethod = DepreciationMethod;
                    assetClass.DepPercentage = Convert.ToInt32(DepPercentage);


                }
                else
                {//generating A random Asst class code

                    Random random = new Random();
                    assetClass.AssetClassCode = "ACC" + random.Next(1000, 1200);
                    assetClass.AssetClassName = AssetClassName;
                    assetClass.assetClassStatus = AssetClassStatus;
                    assetClass.DepreciationMethod = DepreciationMethod;
                    assetClass.DepPercentage = Convert.ToInt32(DepPercentage);

                }
                db.AssetClasses.Add(assetClass);
                db.SaveChanges();

                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created An Asset Class";
                string description = "An Asset Class whose Asset Class Code is " + assetClass.AssetClassCode + " and Asset Class Name " + assetClass.AssetClassName + " was created";
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);
                //*******************End of transaction***********************//


                return RedirectToAction("Create", "Asset");
            }
            
            return View(assetClass);
        }
    }
}
