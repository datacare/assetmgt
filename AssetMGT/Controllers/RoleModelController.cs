﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AssetMGT.Controllers
{
    public class RoleModelController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: RoleModel
        [DynamicRoleAuthorize(ControllerName.RoleModel, ActionName.Index)]
        public ActionResult Index()
        {
            UserIndexData assignedRoleData = new UserIndexData();

            assignedRoleData.Roles = db.Rolemodels;
            return View(assignedRoleData);
        }

        // GET: RoleModel/Details/5
        [DynamicRoleAuthorize(ControllerName.RoleModel, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rolemodel roleModel = db.Rolemodels.Find(id);
            if (roleModel == null)
            {
                return HttpNotFound();
            }
            return View(roleModel);
        }

        // GET: RoleModel/Create
        [DynamicRoleAuthorize(ControllerName.RoleModel, ActionName.Create)]
        public ActionResult Create()
        {
            var arole = new Rolemodel();
            arole.ControllerRoles = new List<ControllerRole>();
            PopulateAssignedControllerData(arole);
            return View();
        }
        
        private void PopulateAssignedControllerData(Rolemodel arole)
        {

            var allControllers = db.ControllersTables;
            var userRoles = new HashSet<int>(arole.ControllerRoles.Select(x => x.ControllersTableId));
            var viewModel = new List<AssignedControllerAction>();
            foreach (var role in allControllers)
            {
                viewModel.Add(new AssignedControllerAction
                {
                    ControllerActionId = role.Id,
                    ControllerName = role.ControllerName,
                    ActionName = role.ActionName,
                    Assigned = userRoles.Contains(role.Id)
                });
            }
            ViewBag.Roles = viewModel;
        }

        // POST: RoleModel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.RoleModel, ActionName.Create)]
        public ActionResult Create(Rolemodel roleModel, string[] selectedControllers)
        {
            if (ModelState.IsValid)
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
                if (!roleManager.RoleExists(roleModel.RoleName))
                {
                    //creating the admin role
                    var role = new IdentityRole();
                    role.Name = roleModel.RoleName;
                    roleManager.Create(role);

                    //creating an admin super user to maintain the site

                }

                ControllerService controllerservice = new ControllerService();

                controllerservice.CreateControllerInfo(roleModel, selectedControllers, db);

                return RedirectToAction("Index");
            }
            
            return View(roleModel);
        }



        // GET: RoleModel/Edit/5
        [DynamicRoleAuthorize(ControllerName.RoleModel, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rolemodel roleModel = db.Rolemodels.Find(id);
            PopulateAssignedControllerData(roleModel);

            if (roleModel == null)
            {
                return HttpNotFound();
            }
            return View(roleModel);

        

        }

        // POST: RoleModel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.RoleModel, ActionName.Edit)]
        public ActionResult Edit(int? id, Rolemodel roleModel, string[] selectedControllers)
        {
            

                ControllerService controllerservice = new ControllerService();

                controllerservice.UpdateControllerInfo( id, roleModel, selectedControllers, db);
           
                return RedirectToAction("Index");
          
        }

        // GET: RoleModel/Delete/5
       // [DynamicRoleAuthorize(ControllerName.RoleModel, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rolemodel roleModel = db.Rolemodels.Find(id);
            if (roleModel == null)
            {
                return HttpNotFound();
            }
            return View(roleModel);
        }

        // POST: RoleModel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.RoleModel, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            Rolemodel roleModel = db.Rolemodels.Find(id);
            db.Rolemodels.Remove(roleModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
