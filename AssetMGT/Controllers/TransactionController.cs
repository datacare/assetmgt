﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.ViewModels;
using PagedList;
using OfficeOpenXml;
using System.IO;
using AssetMGT.Service;

namespace AssetMGT.Controllers
{
    [Authorize(Roles ="Admin")]
    public class TransactionController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: Transaction
        public ActionResult Index(string sortFormat, string searchString, int? page, int? id)
        {
            //to enable search, page and sort
            ViewBag.NameSorting = String.IsNullOrEmpty(sortFormat) ? "name_desc" : "";
            ViewBag.DateSorting = sortFormat == "Date" ? "date_desc" : "Date";

            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                //searchString = currentFilter;
            }
            var viewModel = new TransactionViewModel();
            viewModel.Transactions = db.Transactions;
            if (!String.IsNullOrEmpty(searchString))
            {
                string realName = char.ToUpper(searchString[0]) + searchString.Substring(1);
                viewModel.Transactions = db.Transactions.Where(s => s.Action.Contains(realName));
            }

            //sort format
            switch (sortFormat)
            {
                case "name_desc":
                    viewModel.Transactions = viewModel.Transactions.OrderByDescending(s => s.Action);
                    break;
                case "Date":
                    viewModel.Transactions = viewModel.Transactions.OrderBy(s => s.Date);
                    break;
                case "date_desc":
                    viewModel.Transactions = viewModel.Transactions.OrderByDescending(s => s.Date);
                    break;
                default:
                    viewModel.Transactions = viewModel.Transactions.OrderByDescending(s => s.Date);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(viewModel.Transactions.ToPagedList(pageNumber, pageSize));
        }

        // GET: Transaction/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // GET: Transaction/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Transaction/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                db.Transactions.Add(transaction);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(transaction);
        }

        // GET: Transaction/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // POST: Transaction/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserName,Date,Action,Description")] Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transaction).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(transaction);
        }

        // GET: Transaction/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return HttpNotFound();
            }
            return View(transaction);
        }

        // POST: Transaction/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Transaction transaction = db.Transactions.Find(id);
            db.Transactions.Remove(transaction);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        // POST: Asset/AssetList
        [HttpPost]

        public ActionResult TransactionList()
        {
            var theTransaction = db.Transactions;
            
            //***********************Revelation of Filtering by Asset Class************************//
            
            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("AssetList");
            Sheet.Cells["A1"].Value = "USERNAME";
            Sheet.Cells["B1"].Value = "DATE";
            Sheet.Cells["C1"].Value = "ACTION";
            Sheet.Cells["D1"].Value = "DESCRIPTION";

            int row = 2;


            foreach (var item in theTransaction)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = item.UserName;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.Date.ToString();
                Sheet.Cells[string.Format("C{0}", row)].Value = item.Action;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Description;

                row++;
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "AssetList.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();
            
            using (MemoryStream stream = new MemoryStream())
            {
                Ep.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "AssetList.xlsx");

            }
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
