﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.App_Start;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace AssetMGT.Controllers
{
    public class UserController : Controller
    {
       
        public AssetMGTContext db = new AssetMGTContext();

        // GET: User
        [DynamicRoleAuthorize(ControllerName.User, ActionName.Index)]
        public ActionResult Index()
        {
            UserIndexData assignedRoleData = new UserIndexData();

            assignedRoleData.Users = db.Users ;
            //PopulateAssignedRoleData(user);


            return View(assignedRoleData);
        }

        // GET: User/Details/5
        [DynamicRoleAuthorize(ControllerName.User, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: User/Create
        [DynamicRoleAuthorize(ControllerName.User, ActionName.Create)]
        public ActionResult Create()
        {
             var auser = new User();
            auser.UserRoles = new List<UserRole>();
            PopulateAssignedRoleData(auser);
            if (Session["Error"] != null)
            {
                ViewBag.error = Session["Error"];
                Session.Clear();
            }
            return View();
        }

        private void PopulateAssignedRoleData(User auser)
        {

            var allRoles = db.Rolemodels;
            var userRoles = new HashSet<int>(auser.UserRoles.Select(x => x.RolemodelId));
            var viewModel = new List<AssignedRoleData>();
            foreach (var role in allRoles)
            {
                viewModel.Add(new AssignedRoleData
                {
                    RoleId = role.RolemodelID,
                    RoleName = role.RoleName,
                    Assigned = userRoles.Contains(role.RolemodelID)
                });
            }
            ViewBag.Roles = viewModel;
        }


        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.User, ActionName.Create)]
        public ActionResult Create(User user, string[] selectedRoles, CreateUserViewModel createUserViewModel)
        {
            if (ModelState.IsValid)
           
            {
                UserService userservice = new UserService();
                var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());


               
                var AspNetuser = new IdentityUser() { Email = user.Email, UserName = user.Email };
               // IdentityResult result =  userservice.CreateUser(user, selectedRoles, db, userManager, AspNetuser);
                var result = userservice.CreateUser(user, selectedRoles, db, userManager, AspNetuser);
                if (result.Succeeded)
                {

                    // Guid code = new Guid(AspNetuser.Id.ToString());
                    var provider = new DpapiDataProtectionProvider("AssetMGT");
                    userManager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(
                        provider.Create("AssetMGTToken"));
                    // string code = userManager.GenerateEmailConfirmationToken(AspNetuser.Id);
                    //Random random = new Random();
                    //string code = "CC" + random.Next(1000000, 7000000);
                   string code = userManager.GenerateEmailConfirmationToken(AspNetuser.Id);
                    string callbackurl = Url.Action("ConfirmEmail", "Account",
                      new { userId = AspNetuser.Id, code = code }, protocol: Request.Url.Scheme);

                    CodeGenerator tokenGenerator = new CodeGenerator();
                    tokenGenerator.Email = user.Email;
                    tokenGenerator.userId = AspNetuser.Id;
                    tokenGenerator.Token = code;
                    tokenGenerator.expiryTime = DateTime.Now.AddMinutes(60);
                    tokenGenerator.Status = EmailStatus.NotConfirmed;

                    db.CodeGenerators.Add(tokenGenerator);
                    db.SaveChanges();

                    DateTime theExpiryTime = tokenGenerator.expiryTime;


                    // userManager.SendEmail(AspNetuser.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackurl + "\">here</a>");  
                    string body = "Hello <b> " + user.Email + "</b> , ";
                    body += "<br/>Your account has been created with Data Care Asset Management System.";
                    body += "<br/> <br/> Please click the following link to activate your account <a href=\"" + callbackurl + "\">here</a>";
                    body += "<br/>It will expire in " + theExpiryTime + "";
                    body += "</br/></br/> Thanks,";
                    body += "</br/></br/> <b><span  style='color:#17A2B8'>Data Care(U) Ltd.</span></b>";

                    string subject = "Account Activation";
                    
                    EmailService emailService = new EmailService();

                    emailService.EmailSend(callbackurl, user.Email,body,subject);
                    return RedirectToAction("Index");
                }


            }

            else
            {
                Session["error"] = "Passwords do not match. Please Try Again.";
    
            }

            // PopulateAssignedRoleData(user);

            // return RedirectToAction("Index");

            return RedirectToAction("Create");

        }

        // GET: User/Edit/5
        [DynamicRoleAuthorize(ControllerName.User, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //User user = db.Users.Find(id);
            User user = db.Users.Find(id); 
            PopulateAssignedRoleData(user);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        // [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.User, ActionName.Edit)]
        public ActionResult Edit(int? id, User user, string[] selectedRoles)
        {
            try
            {
                
                    UserService userService = new UserService();
                    userService.UpdateUser(id, user, selectedRoles, db);
                
            }
            catch (Exception e)
            {

            }
            //var UserToUpdate = db.Users.Where(i => i.ID == id).Single();
            //UpdateUserRoles(selectedRoles, UserToUpdate);
         
          //  var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

          //  var registedUser = userManager.FindByEmail(user.Email);
          //  var userId = registedUser.Id;
          

            //  db.Entry(user).State = EntityState.Modified;
           // db.SaveChanges();
                return RedirectToAction("Index");
            

        }
        /*private void UpdateUserRoles(string[] selectedRoles, User UserToUpdate)
        {
            
        }*/

        // GET: User/Delete/5
        [DynamicRoleAuthorize(ControllerName.User, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.User, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
