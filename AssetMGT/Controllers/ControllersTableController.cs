﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;

namespace AssetMGT.Controllers
{
    public class ControllersTableController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: ControllersTable
        [DynamicRoleAuthorize(ControllerName.ControllersTable, ActionName.Index)]
        public ActionResult Index()
        {
            return View(db.ControllersTables.ToList());
        }

        // GET: ControllersTable/Details/5
        [DynamicRoleAuthorize(ControllerName.ControllersTable, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControllersTable controllersTable = db.ControllersTables.Find(id);
            if (controllersTable == null)
            {
                return HttpNotFound();
            }
            return View(controllersTable);
        }

        // GET: ControllersTable/Create
        [DynamicRoleAuthorize(ControllerName.ControllersTable, ActionName.Create)]
        public ActionResult Create()
        {
            var yourController = new ControllersTable();
            yourController.ControllerRoles = new List<ControllerRole>();
            PopulateAssignedRoleData(yourController);
            return View();
        }

        private void PopulateAssignedRoleData(ControllersTable yourController)
        {

            var allRoles = db.Rolemodels;
            var controllerRoles = new HashSet<int>(yourController.ControllerRoles.Select(x => x.RolemodelId));
            var viewModel = new List<AssignedRoleData>();
            foreach (var role in allRoles)
            {
                viewModel.Add(new AssignedRoleData
                {
                    RoleId = role.RolemodelID,
                    RoleName = role.RoleName,
                    Assigned = controllerRoles.Contains(role.RolemodelID)
                });
            }
            ViewBag.Roles = viewModel;
        }

        // POST: ControllersTable/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.ControllersTable, ActionName.Create)]
        public ActionResult Create(ControllersTable controllersTable, string[] selectedRoles)
        {

            if (ModelState.IsValid)
            {
                ControllerService controllerservice = new ControllerService();

               
// controllerservice.CreateControllerInfo(controllersTable, selectedRoles, db);

                return RedirectToAction("Index");
            }
            /*
            if (ModelState.IsValid)
            {

                db.ControllersTables.Add(controllersTable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }*/

            return View(controllersTable);
        }

        // GET: ControllersTable/Edit/5
        [DynamicRoleAuthorize(ControllerName.ControllersTable, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControllersTable controllersTable = db.ControllersTables.Find(id);
            if (controllersTable == null)
            {
                return HttpNotFound();
            }
            return View(controllersTable);
        }

        // POST: ControllersTable/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.ControllersTable, ActionName.Edit)]
        public ActionResult Edit([Bind(Include = "Id,ControllerName,ActionName")] ControllersTable controllersTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(controllersTable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(controllersTable);
        }

        // GET: ControllersTable/Delete/5
        [DynamicRoleAuthorize(ControllerName.ControllersTable, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControllersTable controllersTable = db.ControllersTables.Find(id);
            if (controllersTable == null)
            {
                return HttpNotFound();
            }
            return View(controllersTable);
        }

        // POST: ControllersTable/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.ControllersTable, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            ControllersTable controllersTable = db.ControllersTables.Find(id);
            db.ControllersTables.Remove(controllersTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
