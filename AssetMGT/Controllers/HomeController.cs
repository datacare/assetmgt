﻿using AssetMGT.Data;
using AssetMGT.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssetMGT.Controllers
{
    public class HomeController : Controller
    {
        [DynamicRoleAuthorize(ControllerName.Home, ActionName.Index)]
        public ActionResult Index()
        {
                return View();
          
        }

        

        [AllowAnonymous]
        public ActionResult UnAuthorised()
        {
            
            return View();
        }
        public ActionResult AssetClass()
        {
            return View("~/Views/AssetClass/Create.cshtml");
        }
    }
}