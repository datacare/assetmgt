﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;

namespace AssetMGT.Controllers
{
    public class DisposalController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: Disposal
        [DynamicRoleAuthorize(ControllerName.Disposal, ActionName.Index)]
        public ActionResult Index()
        {
            //var disposals = db.Disposals.Include(d => d.Asset).Include(d => d.Currency).Include(d => d.Customer);
            var disposals = db.Disposals.Include(d => d.Asset);

            //var disposals = db.Disposals.Include(d => d.Asset).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);

            return View(disposals.ToList());
        }

        // GET: Disposal/Details/5
        [DynamicRoleAuthorize(ControllerName.Disposal, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Disposal disposal = db.Disposals.Find(id);
            ViewBag.DisposalDate = disposal.DisposalDate;
            Asset anAsset = db.Assets.Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier).SingleOrDefault(x => x.Id == disposal.AssetId);
            ViewBag.AssetName = anAsset.AssetName;
            if (anAsset == null)
            {
                return HttpNotFound();
            }
            return View(anAsset);
        }

        // GET: Disposal/Create
        //public ActionResult Create(int? id, string DepreciationMethod, int? depPercentage)
        [DynamicRoleAuthorize(ControllerName.Disposal, ActionName.Create)]
        public ActionResult Create(int? id, string[] selectedAssets)
        {
           
            
            if (selectedAssets != null)
            {
                
                foreach (var asset in selectedAssets)
                {
                    Disposal disposal = new Disposal();
                    //disposal.Assets = new List<Asset>();
                    
                    Asset assetToAdd = db.Assets.Find(int.Parse(asset));
                   // disposal.Assets.Add(assetToAdd);
                    
                    assetToAdd.AssetStatus = "Disposed";
                    db.SaveChanges();
                    disposal.DisposalDate = DateTime.UtcNow;
                    disposal.AssetId = assetToAdd.Id;
                    db.Disposals.Add(disposal);
                    db.SaveChanges();

                    //**************Create a transaction here and log the user****************//
                    DateTime dateAndTime = DateTime.Now;
                    string name = HttpContext.User.Identity.Name;
                    ViewBag.Name = name;

                    string action = "Disposed an Asset";
                    string description = "An Asset whose Asset Code is " + assetToAdd.AssetCode + " and Asset Name " + assetToAdd.AssetName + " has been disposed";
                     ActivityLogService activityLogService = new ActivityLogService();
                    activityLogService.LogAction(db, action, description, name, dateAndTime);

                    //*******************End of transaction***********************//
                }
            }
            else
            {
                if(id != null)
                {
                    Disposal disposal = new Disposal();
                    Asset Theasset = db.Assets.Find(id);
                    Theasset.AssetStatus = "Disposed";
                    db.SaveChanges();
                    disposal.DisposalDate = DateTime.UtcNow;
                    disposal.AssetId = Theasset.Id;
                    db.Disposals.Add(disposal);
                    db.SaveChanges();


                    //**************Create a transaction here and log the user****************//
                    DateTime dateAndTime = DateTime.Now;
                    string name = HttpContext.User.Identity.Name;
                    ViewBag.Name = name;                
                    string action = "Disposed an Asset";
                    string description = "An Asset whose Asset Code is " + Theasset.AssetCode + " and Asset Name " + Theasset.AssetName + " has been disposed";
                     ActivityLogService activityLogService = new ActivityLogService();
                    activityLogService.LogAction(db, action, description, name,dateAndTime);

                    //*******************End of transaction***********************//
                }
                else
                {
                    return RedirectToAction("ReadyForDisposal", "Asset");
                }
                
            }

           
           // Disposal disposal = new Disposal();
            

            return RedirectToAction("ReadyForDisposal", "Asset");

            /*
            Asset asset = db.Assets.Where(c=>c.Id == id).Include(w => w.AssetClass).SingleOrDefault();
            DepreciationMethod = asset.DepreciationMethod;
            depPercentage = asset.DepPercentage;

            DisposalViewModel viewModel = new DisposalViewModel();
            if (id != null)
            {
               //send currency and customer list to viewmodel not forgetting the asset list object 
                //viewModel.Currencies = new SelectList(db.Currencies, "Id", "CurrencyCode");
                //viewModel.Customers = new SelectList(db.Customers, "Id", "CustomerName");
                //viewModel.Assets = db.Assets.Where(m => m.Id == id).ToList();

            }*/

            //get the selling price of the asset from the depreciation method
            //DepreciationMethods depreciationMethods = new DepreciationMethods();
            //viewModel.thePrice = depreciationMethods.CalculateDepreciation(id, DepreciationMethod, depPercentage);

            //return View();
        }

        // POST: Disposal/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Disposal, ActionName.Create)]
        //public ActionResult Create([Bind(Include = "Id,DisposalPrice,disposalStatus,CurrencyId,AssetId,CustomerId")] Disposal disposal)
        public ActionResult Create(string AssetCode, string thePrice, string AssetId, string CurrencyId, string CustomerId)
        {
            int theId = Convert.ToInt32(AssetId);

            /*Asset asset = db.Assets.SingleOrDefault(m => m.Id == theId);
            asset.AssetStatus = "Disposed";*/
            Disposal disposal = new Disposal();
            if (ModelState.IsValid)
                {

                /*Disposal disposedItem = new Disposal();

                disposedItem.CustomerId = disposal.CustomerId;
                disposedItem.AssetId = disposal.AssetName;
                disposedItem.CustomerId = disposal.*/

                Asset asset = db.Assets.SingleOrDefault(m => m.Id == theId);
                asset.AssetStatus = "Disposed";

                disposal.DisposalDate = DateTime.UtcNow;
                    
                    disposal.AssetId = theId;
                /*
                    disposal.DisposalPrice = Convert.ToInt32(thePrice);
                    disposal.CurrencyId = Convert.ToInt32(CurrencyId);
                    disposal.CustomerId = Convert.ToInt32(CustomerId);
                    */

                db.Disposals.Add(disposal);
                    db.SaveChanges();

                return RedirectToAction("Index");
                }

            /*
           ViewBag.AssetId = new SelectList(db.Assets, "Id", "AssetCode", disposal.AssetId);
           ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", disposal.CurrencyId);
           ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName", disposal.CustomerId);
      */

            //ViewBag.DisposalDate = new SelectList(db.Assets.Where(x => x.AssetStatus == "Active"), "Id", "AssetDisposalDate");
            return View(disposal);
        }

        // GET: Disposal/Edit/5
        [DynamicRoleAuthorize(ControllerName.Disposal, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Disposal disposal = db.Disposals.Find(id);
            
            Disposal disposal = db.Disposals.SingleOrDefault(m => m.Id == id);
            int disposedItemId = disposal.AssetId;
            if (disposal == null)
            {
                return HttpNotFound();
            }

            //get the disposed asset details
            Asset asset = db.Assets.SingleOrDefault(m => m.Id == disposedItemId);
            ViewBag.AssetName = asset.AssetName;

            ViewBag.AssetCode = asset.AssetCode;
            ViewBag.AssetId = asset.Id;

            //ViewBag.AssetId = new SelectList(db.Assets, "Id", "AssetCode", disposal.AssetId);
            //ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", disposal.CurrencyId);
            //ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName", disposal.CustomerId);
            return View(disposal);
        }

        // POST: Disposal/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Disposal, ActionName.Edit)]
        public ActionResult Edit([Bind(Include = "CurrencyId,CustomerId")] string CustomerId, string CurrencyId, int id)
        {
            Disposal disposedItem = db.Disposals.Find(id);
            if (ModelState.IsValid)
            {
                int Customer_Id = Convert.ToInt32(CustomerId);
                int Currency_Id = Convert.ToInt32(CurrencyId);

                
               // disposedItem.CustomerId = Customer_Id;
                //disposedItem.CurrencyId = Currency_Id;
                db.Entry(disposedItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssetId = new SelectList(db.Assets, "Id", "AssetCode", disposedItem.AssetId);
           // ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", disposedItem.CurrencyId);
           // ViewBag.CustomerId = new SelectList(db.Customers, "Id", "CustomerName", disposedItem.CustomerId);
            return View(disposedItem);
        }

        // GET: Disposal/Delete/5
        [DynamicRoleAuthorize(ControllerName.Disposal, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disposal disposal = db.Disposals.Find(id);
            if (disposal == null)
            {
                return HttpNotFound();
            }
            return View(disposal);
        }

        // POST: Disposal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Disposal, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            Disposal disposal = db.Disposals.Find(id);
            db.Disposals.Remove(disposal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
