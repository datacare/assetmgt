﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetMGT.ViewModels;
using AssetMGT.Data;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Web.Security;
using AssetMGT.Service;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity.Owin;
using AssetMGT.App_Start;
using System.Data.Entity;

namespace AssetMGT.Controllers
{

    public class AccountController : Controller
    {
       

        AssetMGTContext db = new AssetMGTContext();

        // GET: Login

        public ActionResult Index()
        {
            if (Session["Error"] != null)
            {
                ViewBag.error = Session["Error"];
                Session.Clear();
            }
            
            return View();
        }

        //POST: Login

        public ActionResult Login(User theuser)
        {
            AccountService accountService = new AccountService();

            try
            {
                var userStore = new UserStore<IdentityUser>();
                var userManager = new UserManager<IdentityUser>(userStore);
                userManager.MaxFailedAccessAttemptsBeforeLockout = 5;
              
                //user account service
                var user = accountService.LoginService(userManager, theuser);
                if (user == null)
                {
                    Session["Error"] = "Your Entered Incorrect Credentials. Please Try Again";
                    return RedirectToAction("Index", "Account");
                }
                if (!userManager.IsEmailConfirmed(user.Id))
                {
                    Session["Error"] = "Your account is not activated. Visit your email to do so.";
                    return RedirectToAction("Index", "Account");
                }

                
                if (userManager.CheckPassword(user, theuser.Password))
                {
                    bool isLocked = userManager.IsLockedOut(user.Id);
                    
                    //convert datetime offset to real dates
                    DateTime realEndDate = userManager.GetLockoutEndDate(user.Id).DateTime;
                    DateTime realNow = DateTimeOffset.Now.DateTime;

                    bool isTimePast = realNow > realEndDate;
                    
                    if ((!isLocked) || isTimePast)
                    {
                        userManager.ResetAccessFailedCount(user.Id);
                        DateTimeOffset? date = null;
                        userManager.SetLockoutEndDate(user.Id, date.GetValueOrDefault());
                        // Authenticate user
                        var authenticationManager = HttpContext.GetOwinContext().Authentication;
                        var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                        authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);

                        //log the user
                        accountService.LogUser(theuser, db);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        int minute = userManager.GetLockoutEndDate(user.Id).DateTime.Minute - DateTimeOffset.Now.DateTime.Minute;

                        if(minute < 1)
                        {
                             minute = userManager.GetLockoutEndDate(user.Id).DateTime.Second - DateTimeOffset.Now.DateTime.Second;
                            Session["Error"] = "Your Account was Locked Due to Many Failed Login Attempts. Try again after " + minute + " seconds";
                            return RedirectToAction("Index", "Account");
                        }

                        Session["Error"] = "Your Account was Locked Due to Many Failed Login Attempts. Try again after " + minute + " minutes";
                        return RedirectToAction("Index", "Account");
                    }
                    
                }
                else
                {
                    if (userManager.GetLockoutEnabled(user.Id))
                    {
                        if (userManager.IsLockedOut(user.Id))
                        {
                            DateTime realEndDate = userManager.GetLockoutEndDate(user.Id).DateTime;
                            DateTime realNow = DateTimeOffset.Now.DateTime;

                            bool isTimePast = realNow > realEndDate;
                            if (isTimePast)
                            {
                                DateTimeOffset? date = null;
                                userManager.SetLockoutEndDate(user.Id, date.GetValueOrDefault());

                                int numberOfTimes = userManager.MaxFailedAccessAttemptsBeforeLockout - userManager.GetAccessFailedCount(user.Id);
                                Session["Error"] = "Your Entered a wrong password. Your account will get locked after " +numberOfTimes+ " more failing attempts";
                                userManager.AccessFailed(user.Id);

                                return RedirectToAction("Index", "Account");

                            }
                            else
                            {
                                int minute = userManager.GetLockoutEndDate(user.Id).DateTime.Minute - DateTimeOffset.Now.DateTime.Minute;
                                if (minute < 1)
                                {
                                    minute = userManager.GetLockoutEndDate(user.Id).DateTime.Second - DateTimeOffset.Now.DateTime.Second;
                                    Session["Error"] = "Your Account was Locked Due to Many Failed Login Attempts. Try again after " + minute + " seconds";
                                    return RedirectToAction("Index", "Account");
                                }
                                Session["Error"] = "Your Account was Locked Due to Many Failed Login Attempts. Try again after " + minute + " minutes"; userManager.ResetAccessFailedCount(user.Id);
                                return RedirectToAction("Index", "Account");
                            }
                            //bool ilocked = userManager.IsLockedOut(user.Id);
                            
                        }
                        else
                        {
                            int numberOfTimes = userManager.MaxFailedAccessAttemptsBeforeLockout - userManager.GetAccessFailedCount(user.Id);
                            Session["Error"] = "Your Entered a wrong password. Your account will get locked after " + numberOfTimes + " more failing attempts"; userManager.AccessFailed(user.Id);
                            return RedirectToAction("Index", "Account");
                        }

                        
                      //  bool islocked = userManager.IsLockedOut(user.Id);
                       // return RedirectToAction("Index", "Account");
                    }
                    else
                    {
                        Session["Error"] = "You Entered a wrong password. Please Try Again";

                    }
                    return RedirectToAction("Index", "Account");
                }

            }
            catch(Exception e)
            {
                return View("Index");
            }

        }

        public ActionResult UnAuthorised()
        {
            return View();
        }

        //GET: Register

        [DynamicRoleAuthorize(ControllerName.Account, ActionName.Register)]
        public ActionResult Register()
        {

            return View();
        }




        //POST: Register
        [DynamicRoleAuthorize(ControllerName.Account, ActionName.Register)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User theuser)
        {

            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            var user = new IdentityUser() { Email = theuser.Email, UserName = theuser.Email };
            IdentityResult result = manager.Create(user, theuser.Password);

            if (result.Succeeded)
            {
                //return RedirectToAction("Index", "Account");
                // string name = HttpContext.User.Identity.Name;
                var authenticationManager = HttpContext.GetOwinContext().Authentication;
                var userIdentity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                authenticationManager.SignIn(new AuthenticationProperties() { }, userIdentity);
                // Response.Redirect("~/Login.aspx");
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Register", "Account");
            }


        }

        //GET: ForgotPassword
        public ActionResult ForgotPassword()
        {
            if (Session["Error"] != null)
            {
                ViewBag.error = Session["Error"];
                Session.Clear();
            }
            return View();
        }

        //POST: Forgot Password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(User user)
        {           
            
                var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
                var theuser = userManager.FindByName(user.Email);
        
                if(theuser == null || (theuser.EmailConfirmed==false))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View();
                }

                 var provider = new DpapiDataProtectionProvider("AssetMGTReset");
                 userManager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(provider.Create("AssetMGTResetToken"));
            
            /*
            Random random = new Random();
            string generatedCode = "CC" + random.Next(1000000, 7000000);
            */
            string code = userManager.GeneratePasswordResetToken(theuser.Id);
            
            var callbackurl = Url.Action("ResettingPassword", "Account",
            new { userId = theuser.Id, code = code, email = theuser.Email }, protocol: Request.Url.Scheme);

            
            
            CodeGenerator tokenGenerator = new CodeGenerator();
            tokenGenerator.Email = user.Email;
            tokenGenerator.userId = theuser.Id;
            tokenGenerator.Token = code;
            tokenGenerator.expiryTime = DateTime.Now.AddMinutes(60);
            tokenGenerator.Status = EmailStatus.NotConfirmed;

            db.CodeGenerators.Add(tokenGenerator);
            db.SaveChanges();


            DateTime theExpiryTime = tokenGenerator.expiryTime;

            string body = "Please reset your password by clicking <a href=\"" + callbackurl + "\"> here:</a>";
            body += "<br/>It will expire on " + theExpiryTime + " ";
            body += "</br/> Thanks,";
            body += "</br/> <b><span  style='color:#17A2B8'>Data Care(U) Ltd.</span></b>";
            string subject = "Password Reset";

            PasswordRecoveryService passwordRecoveryService = new PasswordRecoveryService();
            passwordRecoveryService.ForgotPassword(user, callbackurl, code, body, subject);

            EmailService emailService = new EmailService();
            emailService.EmailSend(callbackurl, user.Email, body,subject);
            Session["Error"] = "Please check your email for a link to reset your password which expires in an hour";
            //ViewBag.ResetEmailMessage = "Please check your email for a link to reset your password";
            return RedirectToAction("ForgotPassword", "Account");
            
        }

        
        [HttpGet]
        [Route("ResettingPassword", Name = "ResetPasswordRoute")]
        public ActionResult ResettingPassword(string userId = "", string code = "", string email = "")
        {
            if (Session["Error"] != null)
            {
                ViewBag.error = Session["Error"];
                Session.Clear();
                return View();
            }




            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
            {

                ViewBag.Message = "User Id and Code are required";
                return View();
            }

            ResetPasswordViewModel resetPasswordViewModel = new ResetPasswordViewModel();
            resetPasswordViewModel.Code = code;
            ViewBag.Code = code;
            resetPasswordViewModel.Email = email;
            ViewBag.Email = email;
            
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            var provider = new DpapiDataProtectionProvider("AssetMGTReset");
            manager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(provider.Create("AssetMGTResetToken"));
            
            try
            {
                var generatedCode = db.CodeGenerators.Where(X => X.Token == code && X.userId == userId).Single();
                string theCode = generatedCode.Token;
                DateTime expiryTime = generatedCode.expiryTime;
                EmailStatus status = generatedCode.Status;

                DateTime currentTime = DateTime.Now;
                if ((theCode == code) && (currentTime < expiryTime)&&(status == EmailStatus.NotConfirmed))
                {

                    ViewBag.Message = "User is valid";
                    return View(resetPasswordViewModel);

                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = "User is not valid";
                return View(resetPasswordViewModel);
            }

            ViewBag.ErrorMessage = "The Link has already been used or has expired";
            return View(resetPasswordViewModel);
        }

        //POST:Account/ResettingPassword
        [HttpPost]
        public ActionResult ResettingPassword(ResetPasswordViewModel user)
        {
            if (ModelState.IsValid)
            {





                try
                {

                    var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
                    // var AspNetuser = new IdentityUser() { Email = user.Email, UserName = user.Email };

                    var theuser = userManager.FindByName(user.Email);
                    if (theuser == null)
                    {
                        // Don't reveal that the user does not exist or is not confirmed
                        return RedirectToAction("ResetPassword", "Account");
                    }

                    var provider = new DpapiDataProtectionProvider("AssetMGTReset");
                    userManager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(provider.Create("AssetMGTResetToken"));

                    var firstPassword = user.Password;
                    var secondPassword = user.ConfirmPassword;
                    if (firstPassword != secondPassword)
                    {
                        Session["error"] = "Passwords don't match. Please try again.";
                        return View();
                    }

                    else
                    {
                        var result = userManager.ResetPassword(theuser.Id, user.Code, user.Password);

                        if (result.Succeeded)
                        {
                            //ViewBag.MessageForReset = "Your password has been reset.";
                            Session["Error"] = "Your password has been reset";

                            try
                            {
                                CodeGenerator generatedCode = db.CodeGenerators.Where(X => X.Token == user.Code && X.userId == theuser.Id).Single();

                                generatedCode.Status = EmailStatus.Confirmed;

                                db.Entry(generatedCode).State = EntityState.Modified;

                                db.SaveChanges();
                            }
                            catch (Exception x)
                            {

                            }

                            return RedirectToAction("Index", "Account");
                        }
                        else
                        {
                            ViewBag.ErrorMessages = "User is not valid";
                            //Session["error"] = "Passwords do not match. Please Try Again.";
                            return View();
                        }
                    }
                    //Session["Error"] = "Issues";


                }
                catch (Exception e)
                {

                }
                Session["error"] = "Passwords do not match. Please Try Again.";
                return RedirectToAction("ResettingPassword", "Account");
            }

            else
            {
                ModelState.AddModelError(string.Empty, "The Passwords entered do not match");

                ViewBag.Message = "User is not valid";
                /*Session["error"] = "Passwords do not match. Please Try Again.";*/
                return View(user);
            }

            //return View();
          //  return RedirectToAction("ResettingPassword", "Account");
        }


        public void SendEmailConfirmationToken(string UserId ,string subject)
        {
            User user = new User();
            var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());
            var AspNetuser = new IdentityUser() { Email = user.Email, UserName = user.Email };
            var provider = new DpapiDataProtectionProvider("AssetMGT");
            userManager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(
   provider.Create("AssetMGTToken"));
            string code = userManager.GenerateEmailConfirmationToken(AspNetuser.Id);
            var callbackurl = Url.Action("ConfirmEmail", "Account",
                 new { userId = AspNetuser.Id, code = code }, protocol: Request.Url.Scheme);
            userManager.SendEmail(AspNetuser.Id, subject,
                "Please confirm your account by clicking <a href=\"" + callbackurl + "\">here</a>");
         
           // return callbackurl;
        }



        public ActionResult Activation()
        {
            //UserManager<ApplicationUser> userManager;
            ViewBag.Message = "Invalid Activation code.";
            if (RouteData.Values["Id"] != null)
            {
                Guid activationCode = new Guid(RouteData.Values["Id"].ToString());
                AssetMGTContext assetMGTContext = new AssetMGTContext();
                UserActivation userActivation = assetMGTContext.UserActivations.Where(p => p.ActivationCode == activationCode).FirstOrDefault();

                if (userActivation != null)
                {
                    assetMGTContext.UserActivations.Remove(userActivation);
                    assetMGTContext.SaveChanges();
                    return RedirectToAction("Index", "Account");
                }
            }
            return View("Index", "Account");
        }

        [HttpGet]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        public ActionResult ConfirmEmail(string userId = "", string code = "")
        {
            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
            {
               
                ViewBag.Message = "User Id and Code are required";
                return View();
            }

            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);
            
            var provider = new DpapiDataProtectionProvider("AssetMGT");
            manager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(
                provider.Create("AssetMGTToken"));
            
            var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

            var generatedCode = db.CodeGenerators.Where(X => X.userId == userId).Single();
            string theCode = generatedCode.Token;
            DateTime expiryTime = generatedCode.expiryTime;
            EmailStatus status = generatedCode.Status;


            DateTime currentTime = DateTime.Now;
            if((theCode == code)&&(currentTime < expiryTime)&&(status == EmailStatus.NotConfirmed))
            {
                var result = manager.ConfirmEmail(userId, code);
                if (result.Succeeded)
                {
                    ViewBag.Confirmed = "Email Confirmed";
                    // generatedCode.Status = EmailStatus.Confirmed;
                    try
                    {
                        CodeGenerator codeGenerator = db.CodeGenerators.Where(X => X.userId == userId).Single();
                        codeGenerator.Status = EmailStatus.Confirmed;

                        db.Entry(codeGenerator).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                    catch(Exception x)
                    {

                    }
                    
                    return View();
                }
            }

          
            
            return View();
        }

        public string getName()
        {
            string name = HttpContext.User.Identity.Name;
            return name;
        }
        //GET: Logout   

        public ActionResult Logout()
        {
            //**************Create a transaction here and log the user****************//
            DateTime dateAndTime = DateTime.Now;
            string name = HttpContext.User.Identity.Name;
            //ViewBag.Name = name;       
            string action = "User Logged Out";
            string description = name + " logged out of the Asset MIS";
            ActivityLogService activityLogService = new ActivityLogService();
            activityLogService.LogAction(db, action, description, name, dateAndTime);

            //*******************End of transaction***********************//


            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut();

            return RedirectToAction("Index", "Account");
        }
        [DynamicRoleAuthorize]
        public ActionResult RegisterUsers()
        {
            return View();
        }


    }
}