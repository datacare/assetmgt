﻿using AssetMGT.Data;
using AssetMGT.Service;
using AssetMGT.ViewModels;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;

namespace AssetMGT.Controllers
{
    
    public class DepreciationController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: TestDepreciation
        [DynamicRoleAuthorize(ControllerName.Depreciation, ActionName.Index)]
        public ActionResult Index()
        {
           var depreciations = db.Depreciations.Include(d => d.Asset).Include(d => d.Currency);
            var Assets = db.Assets.Include(a => a.AssetClass);
            DepreciationDetailsViewModel depreciationDetailsViewModel = new DepreciationDetailsViewModel();
            depreciationDetailsViewModel.Assets = Assets;
            return View("DepreciationDetails",depreciationDetailsViewModel);
            //return View(depreciations.ToList());
        }

        // GET: TestDepreciation/Details/5
        [DynamicRoleAuthorize(ControllerName.Depreciation, ActionName.Details)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depreciation depreciation = db.Depreciations.Find(id);
            if (depreciation == null)
            {
                return HttpNotFound();
            }
            return View(depreciation);
        }

        // GET: TestDepreciation/Create
        [DynamicRoleAuthorize(ControllerName.Depreciation, ActionName.Create)]
        public ActionResult Create()
        {
            ViewBag.AssetId = new SelectList(db.Assets, "Id", "AssetCode");
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName");
            return View();
        }

        // POST: TestDepreciation/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Depreciation, ActionName.Create)]
        public ActionResult Create([Bind(Include = "Id,DepreciationMethod,DepreciationPercentage,salvageValue,DepreciationValue,NetbookValue,DepreciationPeriod,CurrencyId,AssetId")] Depreciation depreciation)
        {
            Asset asset = db.Assets.SingleOrDefault(a => a.Id == depreciation.AssetId);
            //getting the basevalue
            int purchaseprice = asset.AssetPurchasePrice;
            int salvagevalue = depreciation.salvageValue;
            int totaldepreciablecost = purchaseprice - salvagevalue;

            //getting the useful life
            DateTime purchasedate = asset.AssetPurchaseDate;
            int usefullife = DateTime.UtcNow.Year - purchasedate.Year;
            int depreciationamount = totaldepreciablecost / usefullife;
            int depPercentage = Convert.ToInt32((Convert.ToDouble(depreciationamount) / Convert.ToDouble(totaldepreciablecost)) * 100);
            int netbookvalue = purchaseprice - depreciationamount;


            if (ModelState.IsValid)
            {
                depreciation.DepreciationPercentage = depPercentage;
                depreciation.DepreciationValue = depreciationamount;
                depreciation.NetbookValue = netbookvalue;
                depreciation.DepreciationPeriod = usefullife;
                db.Depreciations.Add(depreciation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssetId = new SelectList(db.Assets, "Id", "AssetCode", depreciation.AssetId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", depreciation.CurrencyId);
            return View(depreciation);
        }

        // GET: TestDepreciation/Edit/5
        [DynamicRoleAuthorize(ControllerName.Depreciation, ActionName.Edit)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depreciation depreciation = db.Depreciations.Find(id);
            if (depreciation == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssetId = new SelectList(db.Assets, "Id", "AssetCode", depreciation.AssetId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", depreciation.CurrencyId);
            return View(depreciation);
        }

        // POST: TestDepreciation/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Depreciation, ActionName.Edit)]
        public ActionResult Edit([Bind(Include = "Id,DepreciationMethod,DepreciationPercentage,salvageValue,DepreciationValue,NetbookValue,DepreciationPeriod,CurrencyId,AssetId")] Depreciation depreciation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(depreciation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssetId = new SelectList(db.Assets, "Id", "AssetCode", depreciation.AssetId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "CurrencyName", depreciation.CurrencyId);
            return View(depreciation);
        }

        // GET: TestDepreciation/Delete/5
        [DynamicRoleAuthorize(ControllerName.Depreciation, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depreciation depreciation = db.Depreciations.Find(id);
            if (depreciation == null)
            {
                return HttpNotFound();
            }
            return View(depreciation);
        }

        // POST: TestDepreciation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Depreciation, ActionName.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            Depreciation depreciation = db.Depreciations.Find(id);
            db.Depreciations.Remove(depreciation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Depreciation/Details
        public ActionResult DuplicateDetails(string sortFormat, string searchString, int? page, int? id, string yearEntered,string textyearEntered, string assetClassId, string timefreq)
        {
            ViewBag.yearEntered = new SelectList(db.Assets.Where(y => y.AssetStatus == "Active").Select(y => y.StartDepreciationDate.Year).Distinct());
            

            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                //searchString = currentFilter;
            }
            ViewBag.assetClassId = new SelectList(db.AssetClasses, "Id", "AssetClassName");
            
            DepreciationDetailsViewModel assetDetailsViewModel = new DepreciationDetailsViewModel();
            

            if (timefreq == "Monthly")
            {
                if (!String.IsNullOrEmpty(assetClassId))
                {
                    int theAssetClassId = Convert.ToInt32(assetClassId);
                    assetDetailsViewModel.Assets = db.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Where(d => d.AssetClassId == theAssetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.assetClassFilter = assetClassId;
                }
                else
                {
                    assetDetailsViewModel.Assets = db.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                }

                // searching functionality starts here
                if (!String.IsNullOrEmpty(searchString))
                {
                    string realName = char.ToUpper(searchString[0]) + searchString.Substring(1);
                    assetDetailsViewModel.Assets = assetDetailsViewModel.Assets.Where(s => s.AssetName.Contains(realName));
                    ViewBag.SearchedItem = searchString;
                }

                bool isMonthly = true;
                ViewBag.isMonthly = isMonthly;
                List<Asset> assetList = new List<Asset>();
                int theNowYear;
                
                foreach (var item in assetDetailsViewModel.Assets)
                {
                    Asset asset = new Asset();
                    DateTime startDepreciationDate;
                    startDepreciationDate = item.StartDepreciationDate;
                    DateTime currentDate;

                    if (item.EndDepreciationDate != null)
                    {
                        currentDate = item.EndDepreciationDate.GetValueOrDefault();
                    }
                    else
                    {
                        currentDate = DateTime.UtcNow;
                    }

                    
                    theNowYear = currentDate.Year;
                    int startDepYear = startDepreciationDate.Year;
                    
                    int theEnteredYear = DateTime.Now.Year;
                    if (!String.IsNullOrEmpty(yearEntered))
                    {
                        theEnteredYear = Convert.ToInt32(yearEntered);
                        theNowYear = theEnteredYear;
                        ViewBag.Year = theEnteredYear;
                    }
                    else
                    {
                        theNowYear = DateTime.Now.Year;
                        ViewBag.Year = theNowYear;
                    }
                    if (!String.IsNullOrEmpty(textyearEntered))
                    {
                        Asset minDateAsset = db.Assets.OrderBy(a => a.StartDepreciationDate).First();
                        
                        theEnteredYear = Convert.ToInt32(textyearEntered);
                        if(theEnteredYear <= minDateAsset.StartDepreciationDate.Year)
                        {
                            theNowYear = (minDateAsset.StartDepreciationDate.Year)+1;
                            ViewBag.Year = theNowYear;
                        }
                        else
                        {
                            theNowYear = theEnteredYear;
                            ViewBag.Year = theNowYear;
                        }
                        
                       
                    }
                    else
                    {
                        theNowYear = DateTime.Now.Year;
                        theNowYear--;
                        ViewBag.Year = theNowYear;
                    }

                    int usefullife = theNowYear - startDepYear;

                    // creating an assetList
    
                    int? salvageValue = item.SalvageValue;
                    var depMethod = item.AssetClass.DepreciationMethod;
                    double TheDepPercentage = item.AssetClass.DepPercentage;

                    if(theNowYear > startDepYear)
                    {
                        if (depMethod == AssetClass.DepMethods.ReducingBalanceMethod)
                        {
                            ViewBag.DisplayMethod = "Using the Reducing Balance Methodrm";

                            int purchaseprice = item.AssetPurchasePrice;
                            int depAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * purchaseprice);

                            int depPerDayAmount = depAmount / 365;
                            int daysPassed = startDepreciationDate.Date.Subtract(currentDate).Days;
                            int firstYear = startDepreciationDate.Year;

                            DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
                            int numberofDays = (firstYearEndDate - startDepreciationDate).Days;
                            int yearOneDepAmount = depPerDayAmount * numberofDays;
                            int WorthAmount = purchaseprice - yearOneDepAmount;
                            int Years = startDepreciationDate.Year;
                            int TheYears = 0;
                            int accumulatedDep = yearOneDepAmount;

                            for (int counter = 1; counter <= (usefullife + 1); counter++)
                            {

                                int percent = Convert.ToInt32(TheDepPercentage);

                                TheYears = Years + counter;
                                // yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * WorthAmount);

                                if (TheYears == theNowYear)
                                {
                                    asset.DepAmount = yearOneDepAmount;
                                    asset.GrossAmount = WorthAmount;
                                    asset.AccumulatedDepAmount = accumulatedDep;
                                }
                                yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * WorthAmount);
                                WorthAmount = WorthAmount - yearOneDepAmount;
                                accumulatedDep = accumulatedDep + yearOneDepAmount;
                                //checking whether worth amount == salvage value for automatic ready for disposal status
                                if (salvageValue != null)
                                {
                                    if ((WorthAmount <= salvageValue) || (WorthAmount == 0))
                                    {
                                        break;

                                    }
                                }


                            }
                            asset.DepPercentage = (int)item.AssetClass.DepPercentage;
                            asset.AssetName = item.AssetName;
                            assetList.Add(asset);
                            assetDetailsViewModel.Assets = assetList;
                        }
                        else if(depMethod == AssetClass.DepMethods.StraightLineMethod)
                        {

                            ViewBag.DisplayMethod = "Using the Reducing Balance Methodrm";

                            int purchaseprice = item.AssetPurchasePrice;
                            int depAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * purchaseprice);

                            int depPerDayAmount = depAmount / 365;
                            int daysPassed = startDepreciationDate.Date.Subtract(currentDate).Days;
                            int firstYear = startDepreciationDate.Year;

                            DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
                            int numberofDays = (firstYearEndDate - startDepreciationDate).Days;
                            int yearOneDepAmount = depPerDayAmount * numberofDays;
                            int WorthAmount = purchaseprice - yearOneDepAmount;
                            int Years = startDepreciationDate.Year;
                            int TheYears = 0;
                            int accumulatedDep = yearOneDepAmount;

                            for (int counter = 1; counter <= (usefullife + 1); counter++)
                            {

                                int percent = Convert.ToInt32(TheDepPercentage);

                                TheYears = Years + counter;
                                // yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * WorthAmount);

                                if (TheYears == theNowYear)
                                {
                                    asset.DepAmount = yearOneDepAmount;
                                    asset.GrossAmount = WorthAmount;
                                    asset.AccumulatedDepAmount = accumulatedDep;
                                }
                                yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * WorthAmount);
                                
                                accumulatedDep = accumulatedDep + yearOneDepAmount;
                                //checking whether worth amount == salvage value for automatic ready for disposal status
                                if (salvageValue != null)
                                {
                                    if ((WorthAmount <= salvageValue) || (WorthAmount == 0))
                                    {
                                        break;

                                    }
                                }


                            }
                            asset.DepPercentage = (int)item.AssetClass.DepPercentage;
                            asset.AssetName = item.AssetName;
                            assetList.Add(asset);
                            assetDetailsViewModel.Assets = assetList;
                        }
                    }
                    

                }
                

            }

            else
            {
                
                if(yearEntered == null)
                {
                    try
                    {
                        Asset minDateAsset = db.Assets.Where(d=>d.AssetStatus == "Active").OrderBy(a => a.StartDepreciationDate).First();
                        yearEntered = minDateAsset.StartDepreciationDate.Year.ToString();
                    }
                    catch(Exception e)
                    {
                       
                        Session["Error"] = "Please Add some assets to be able to do depreciation on them";
                        return RedirectToAction("Index","Asset");

                    }
                    
                }
                List<AssetRegistering> assetRegister = assetDetailsViewModel.LoadAssets(db, searchString, assetClassId, yearEntered);
                assetDetailsViewModel.AssetRegisterings = assetRegister;
                
                int realYear = Convert.ToInt32(yearEntered);
                ViewBag.enteredYear = realYear;

            }

            int pageSize =  10;
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;



            return View(assetDetailsViewModel);
        }

        [HttpPost]
        public FileResult DepList(string statusFilter, string classFilter, string startDateFilter, string endDateFilter, string yearEntered, string searchString)
        {
            //export dep list

            DepreciationDetailsViewModel assetDetailsViewModel = new DepreciationDetailsViewModel();

            if (yearEntered == null)
            {
                Asset minDateAsset = db.Assets.OrderBy(a => a.StartDepreciationDate).First();
                yearEntered = minDateAsset.StartDepreciationDate.Year.ToString();
            }
            

            List<AssetRegistering> assetRegister = assetDetailsViewModel.LoadAssets(db, searchString, classFilter, yearEntered);
            assetDetailsViewModel.AssetRegisterings = assetRegister;
            
            int dateStart = Convert.ToInt32(yearEntered);
            //getting the asset with minimum year
            
            int myTodayDate = DateTime.Now.Year;

            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("DepreciationList");
            Sheet.Cells["A1"].Value = "ASSET MIS ANNUAL REPORT";
            Sheet.Cells["A1"].Style.Font.Bold = true;
            Sheet.Cells["A1:D1"].Merge = true;
            Sheet.Cells["A3"].Value = "ASSET NAME";
            Sheet.Cells["B3"].Value = "DEP %";
            Sheet.Cells["A3"].Style.Font.Bold = true;
            Sheet.Cells["B3"].Style.Font.Bold = true;
            int myrow = 3;
            int myheaderCol = 3;
            int myheaderCol2 = 4;
            int myheaderCol3 = 5;
            int row = 4;
            int mainHeaderRow = 2;
            int mergeCell1 = 3;
            //int mergeCell2 = 4;
            int mergeCell3 = 5;
            //setting year headers
            for (int count = dateStart; count < myTodayDate; count++)
            {
                Sheet.Cells[mainHeaderRow, mergeCell1, mainHeaderRow, mergeCell3].Merge = true;
                Sheet.Cells[mainHeaderRow, mergeCell1].Value = "Year: " + count;
                Sheet.Cells[mainHeaderRow, mergeCell1].Style.Font.Bold = true;
                mergeCell1 +=3;
                mergeCell3+=3;
            }

            //setting second headers
            int thisTodayDate = DateTime.Now.Year;

            //setting the second headers
            for(int count = (dateStart + 1); count <= myTodayDate; count++)
            {
                Sheet.Cells[myrow, myheaderCol].Value = "DEP AMOUNT";
                Sheet.Cells[myrow, myheaderCol2].Value = "NETBOOK VALUE";
                Sheet.Cells[myrow, myheaderCol3].Value = "ACCUMULATED DEP";
                Sheet.Cells[myrow, myheaderCol].Style.Font.Bold = true;
                Sheet.Cells[myrow, myheaderCol2].Style.Font.Bold = true;
                Sheet.Cells[myrow, myheaderCol3].Style.Font.Bold = true;
                myheaderCol += 3;
                myheaderCol2 += 3;
                myheaderCol3 += 3;
            }
            
            foreach (var item in assetDetailsViewModel.AssetRegisterings)
            {
                int purchaseprice = item.AssetPurchasePrice;
                int TheDepPercentage = item.DepreciationPercentage;
               
                Sheet.Cells[string.Format("A{0}", row)].Value = item.AssetName;
                Sheet.Cells[string.Format("A{0}", row)].Style.Font.Bold = true;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.DepreciationPercentage;
                int myCol = 3;
                int myCol2 = 4;
                int myCol3 = 5;
                
                foreach (var asetdeprec in item.AssetDepreciatings)
                {
                    //values with commas
                    Sheet.Cells[row, myCol].Style.Numberformat.Format = "#,##0";
                    Sheet.Cells[row, myCol2].Style.Numberformat.Format = "#,##0";
                    Sheet.Cells[row, myCol3].Style.Numberformat.Format = "#,##0";

                    //row data
                    Sheet.Cells[row, myCol].Value = asetdeprec.DepreciationAmount;
                    Sheet.Cells[row, myCol2].Value = asetdeprec.AccumulatedDepreciation;
                    Sheet.Cells[row, myCol3].Value = asetdeprec.NetBookValue;
                    myCol +=3;
                    myCol2+=3;
                    myCol3 += 3;
                    myheaderCol += 3;
                    myheaderCol2 += 3;
                    myheaderCol3 += 3;
                }
                row++;
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "DepreciationList.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();

            //**************Create a transaction here and log the user****************//
            DateTime dateAndTime = DateTime.Now;
            string name = HttpContext.User.Identity.Name;
            ViewBag.Name = name;           
            string action = "Exported to Excel";
            string description = "Exported the Annual Asset Depreciation Report from " + dateStart +" to " + myTodayDate;
            ActivityLogService activityLogService = new ActivityLogService();
            activityLogService.LogAction(db, action, description, name, dateAndTime);

            //*******************End of transaction***********************//

            using (MemoryStream stream = new MemoryStream())
            {
                Ep.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "DepreciationList.xlsx");
            }
        }

        [HttpPost]
        public FileResult DepMonthlyList(string statusFilter, string YearFilter, string classFilter, string startDateFilter, string endDateFilter, string searchString)
        {
            DepreciationDetailsViewModel assetDetailsViewModel = new DepreciationDetailsViewModel();

            List<AssetRegistering> assetRegister = assetDetailsViewModel.LoadMonthlyAssets(db, YearFilter, classFilter);
            assetDetailsViewModel.AssetRegisterings = assetRegister;

            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("DepreciationList");
            Sheet.Cells["A1"].Value = "ASSET MIS MONTHLY REPORT OF THE YEAR: "+ YearFilter;
            Sheet.Cells["A1"].Style.Font.Bold = true;
            Sheet.Cells["A1:D1"].Merge = true;
            Sheet.Cells["A3"].Value = "ASSET NAME";
            Sheet.Cells["B3"].Value = "DEP %";
            Sheet.Cells["A3"].Style.Font.Bold = true;
            Sheet.Cells["B3"].Style.Font.Bold = true;
            int myrow = 3;
            int myheaderCol = 3;
            int myheaderCol2 = 4;
            int myheaderCol3 = 5;
            int row = 4;
            int mainHeaderRow = 2;
            int mergeCell1 = 3;
            int mergeCell2 = 4;

            //merge monthly header cells
            for (int count = 1; count <= 12; count++)
            {
                Sheet.Cells[mainHeaderRow, mergeCell1, mainHeaderRow, mergeCell2].Merge = true;
                Sheet.Cells[mainHeaderRow, mergeCell1].Value = "Month: " + count;
                Sheet.Cells[mainHeaderRow, mergeCell1].Style.Font.Bold = true;
                mergeCell1 += 3;
                mergeCell2 += 3;
            }

            //setting the second headers
            for (int count = 1; count <= 12; count++)
            {
                Sheet.Cells[myrow, myheaderCol].Value = "DEP AMOUNT";
                Sheet.Cells[myrow, myheaderCol2].Value = "NETBOOK VALUE";
                Sheet.Cells[myrow, myheaderCol3].Value = "ACCUMULATED DEP";
                Sheet.Cells[myrow, myheaderCol].Style.Font.Bold = true;
                Sheet.Cells[myrow, myheaderCol2].Style.Font.Bold = true;
                Sheet.Cells[myrow, myheaderCol3].Style.Font.Bold = true;
                myheaderCol += 3;
                myheaderCol2 += 3;
                myheaderCol3 += 3;
            }

            //setting the content to the excel sheet
            foreach (var item in assetDetailsViewModel.AssetRegisterings)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = item.AssetName;
                Sheet.Cells[string.Format("A{0}", row)].Style.Font.Bold = true;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.DepreciationPercentage;
                int myCol = 3;
                int myCol2 = 4;
                int myCol3 = 5;

                foreach (var asetdeprec in item.AssetDepreciatings)
                {
                    for (int c = 1; c <= 12; c++)
                    {
                        int myItemDepreciation = Convert.ToInt32(asetdeprec.DepreciationAmount) / 12;
                        int myItemNetbookvalue = Convert.ToInt32(asetdeprec.NetBookValue) / 12;
                        int myItemAccumDepvalue = Convert.ToInt32(asetdeprec.AccumulatedDepreciation) / 12;
                        
                        //values with commas
                        Sheet.Cells[row, myCol].Style.Numberformat.Format = "#,##0";
                        Sheet.Cells[row, myCol2].Style.Numberformat.Format = "#,##0";
                        Sheet.Cells[row, myCol3].Style.Numberformat.Format = "#,##0";

                        Sheet.Cells[row, myCol].Value = myItemDepreciation;
                        Sheet.Cells[row, myCol2].Value = myItemAccumDepvalue;
                        Sheet.Cells[row, myCol3].Value = myItemNetbookvalue;

                        myCol += 3;
                        myCol2 += 3;
                        myCol3+= 3;
                        myheaderCol += 3;
                        myheaderCol2 += 3;
                        myheaderCol3 += 3;
                    }
                }
                row++;
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "DepreciationList.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();

            //**************Create a transaction here and log the user****************//
            DateTime dateAndTime = DateTime.Now;
            string name = HttpContext.User.Identity.Name;
            ViewBag.Name = name;
            string action = "Exported to Excel";
            string description = "Exported the Monthly Depreciation List of the year of "+ YearFilter + " to Excel";
             ActivityLogService activityLogService = new ActivityLogService();
            activityLogService.LogAction(db, action, description, name,dateAndTime);

            //*******************End of transaction***********************//


            using (MemoryStream stream = new MemoryStream())
            {
                Ep.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "DepreciationList.xlsx");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
