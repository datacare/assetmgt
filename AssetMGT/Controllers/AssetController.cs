﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using AssetMGT.Data;
using AssetMGT.ViewModels;
using PagedList;
using OfficeOpenXml;
using System.IO;
using static AssetMGT.Data.Asset;
using Microsoft.AspNet.Identity;
using AssetMGT.Service;

namespace AssetMGT.Controllers
{

  public class AssetController : Controller
    {
        private AssetMGTContext db = new AssetMGTContext();

        // GET: Asset
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Index)]
        public ActionResult Index(string sortFormat, string currentFilter, string searchString, int? page, int? id, string assetStatusId, string assetClassId, string textStartDate, string textEndDate, int? thepageSize, AssetClass assetClass, string filterStatus, EntityCategory Grouping, string Group)
        {
            if (Session["Error"] != null)
            {
                ViewBag.error = Session["Error"];
                Session.Clear();
            }


            //to enable search, page and sort
            ViewBag.NameSorting = String.IsNullOrEmpty(sortFormat) ? "name_desc" : "";
            ViewBag.DateSorting = sortFormat == "Date" ? "date_desc" : "Date";
            ViewBag.ClassSorting = sortFormat == "Class" ? "class_desc" : "Class";
            ViewBag.CodeSorting = sortFormat == "Code" ? "code_desc" : "Code";
            ViewBag.PriceSorting = sortFormat == "Price" ? "price_desc" : "Price";
            ViewBag.StatusSorting = sortFormat == "Status" ? "status_desc" : "Status";
            ViewBag.EngNumberSorting = sortFormat == "EngNumber" ? "EngNumber_desc" : "EngNumber";
            

            List<AssetClass> assetClassList = new List<AssetClass>();
            assetClassList = db.AssetClasses.ToList();
            List<AssetCount> assetListCount = new List<AssetCount>();
            foreach (var assetclass in assetClassList)
            {
                AssetCount assetCount = new AssetCount();
                string Name = assetclass.AssetClassName;
                int theId = assetclass.Id;
                List<Asset> assetList = new List<Asset>();
                int NumberOfAssets = db.Assets.Where(d => d.AssetClass.AssetClassName == Name && d.AssetStatus == "Active").Count();
                assetCount.AssetClassName = Name;
                assetCount.AssetNumber = NumberOfAssets.ToString();
                assetListCount.Add(assetCount);             
 
            }

            ViewBag.Grouping = Grouping;

            ViewBag.AssetClasses = assetListCount;

                   
            ViewBag.CurrentFilter = searchString;

            if (searchString != null)
            {
               // page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            var viewModel = new AssetViewModel();
            //viewModel.Assets = db.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
            viewModel.Assets = db.Assets.Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);


            //*********************Genesis of Filtering**********************************//
           
            DateTime startDate;
            DateTime endDate;
            if ((!String.IsNullOrEmpty(assetStatusId)) || (!String.IsNullOrEmpty(assetClassId)) || (textStartDate != null) || (textEndDate != null))
            {


                if ((!String.IsNullOrEmpty(textStartDate)) && (!String.IsNullOrEmpty(textEndDate)) && (!String.IsNullOrEmpty(assetClassId)))
                {
                    startDate = DateTime.Parse(textStartDate);
                    endDate = DateTime.Parse(textEndDate);
                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId).Where(c => startDate <= c.AssetPurchaseDate && endDate >= c.AssetPurchaseDate).Include(a => a.AssetClass).Where(d => d.AssetClassName == assetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                }
                else if ((!String.IsNullOrEmpty(textStartDate)) && (!String.IsNullOrEmpty(textEndDate)))
                {
                    startDate = DateTime.Parse(textStartDate);
                    endDate = DateTime.Parse(textEndDate);

                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId).Where(c => startDate <= c.AssetPurchaseDate && endDate >= c.AssetPurchaseDate).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                }
                else if ((!String.IsNullOrEmpty(textStartDate)) && (!String.IsNullOrEmpty(textEndDate))&& (!String.IsNullOrEmpty(Group)))
                {
                    startDate = DateTime.Parse(textStartDate);
                    endDate = DateTime.Parse(textEndDate);

                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId && d.AssetSubclass == Group).Where(c => startDate <= c.AssetPurchaseDate && endDate >= c.AssetPurchaseDate).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                }
                else if (!String.IsNullOrEmpty(assetClassId) && (!String.IsNullOrEmpty(assetStatusId))&&(!String.IsNullOrEmpty(Group)))
                {
                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId && d.AssetSubclass == Group).Include(a => a.AssetClass).Where(d => d.AssetClass.AssetClassName == assetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(assetClassId)&&(!String.IsNullOrEmpty(assetStatusId)))
                {
                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId).Include(a => a.AssetClass).Where(d => d.AssetClass.AssetClassName == assetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(Group) && (!String.IsNullOrEmpty(assetStatusId)))
                {
                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId && d.AssetSubclass == Group).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(assetClassId))
                {
                    
                    viewModel.Assets = db.Assets.Include(a => a.AssetClass).Where(d => d.AssetClass.AssetClassName == assetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(assetStatusId))
                {
                    viewModel.Assets = db.Assets.Where(s=>s.AssetStatus == assetStatusId).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(Group))
                {
                    viewModel.Assets = db.Assets.Where(s=>s.AssetSubclass == Group).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                ViewBag.assetClassFilter = assetClassId;
                ViewBag.assetStatusFilter = assetStatusId;
                ViewBag.startDate = textStartDate;
                ViewBag.endDate = textEndDate;
            }

            else
            {
                viewModel.Assets = db.Assets.Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);

               // ViewBag.assetStatusFilter = "Active";
            }

            if (!String.IsNullOrEmpty(searchString))
            {
               // string title = "STRING";
                //bool contains = title.Contains("string", StringComparison.OrdinalIgnoreCase);

                string realName = searchString.ToUpper();
                viewModel.Assets = viewModel.Assets.Where(s => s.AssetEngravementNumber.ToUpper().Contains(realName) || s.AssetName.ToUpper().Contains(realName) || s.AssetClass.AssetClassName.ToUpper().Contains(realName) || s.AssetStatus.ToUpper().Contains(realName));
            }

            if (!String.IsNullOrEmpty(filterStatus))
            {
                ViewBag.FilterStatus = filterStatus;
                if(filterStatus == "show")
                {
                    ViewBag.FilterStatus = "show";
                }
                else
                {
                    ViewBag.FilterStatus = "hide";
                }
            }
            else
            {
                ViewBag.FilterStatus = "hide";
            }

            //*********************Revelation of Filtering*******************************//

            Asset asset = new Asset();
            ViewBag.AllocationStatus = asset.AllocationStatus;
            if (asset.AllocationStatus == "Unallocated")
            {
                ViewBag.AllocationStatus = "Unallocated";

            }


            //sort format
            switch (sortFormat)
            {
                case "name_desc":
                    viewModel.Assets = viewModel.Assets.OrderByDescending(s => s.AssetName);
                    break;
                case "Date":
                    viewModel.Assets = viewModel.Assets.OrderBy(s => s.AssetPurchaseDate);
                    break;
                case "date_desc":
                    viewModel.Assets = viewModel.Assets.OrderByDescending(s => s.AssetPurchaseDate);
                    break;
                case "Class":
                    viewModel.Assets = viewModel.Assets.OrderBy(s => s.AssetClass.AssetClassName);
                    break;
                case "class_desc":
                    viewModel.Assets = viewModel.Assets.OrderByDescending(s => s.AssetClass.AssetClassName);
                    break;
                case "Code":
                    viewModel.Assets = viewModel.Assets.OrderBy(s => s.AssetCode);
                    break;
                case "code_desc":
                    viewModel.Assets = viewModel.Assets.OrderByDescending(s => s.AssetCode);
                    break;
                case "Price":
                    viewModel.Assets = viewModel.Assets.OrderBy(s => s.AssetPurchasePrice);
                    break;
                case "price_desc":
                    viewModel.Assets = viewModel.Assets.OrderByDescending(s => s.AssetPurchasePrice);
                    break;
                case "Status":
                    viewModel.Assets = viewModel.Assets.OrderBy(s => s.AssetStatus);
                    break;
                case "status_desc":
                    viewModel.Assets = viewModel.Assets.OrderByDescending(s => s.AssetStatus);
                    break;
                case "EngNumber":
                    viewModel.Assets = viewModel.Assets.OrderBy(s => s.AssetEngravementNumber);
                    break;
                case "EngNumber_desc":
                    viewModel.Assets = viewModel.Assets.OrderByDescending(s => s.AssetEngravementNumber);
                    break;
                default:
                    viewModel.Assets = viewModel.Assets.OrderBy(s => s.AssetName);
                    break;
            }
            
            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;
            ViewBag.numEmployees = viewModel.Assets.Count();


            ViewBag.assetClassId = new SelectList(db.AssetClasses.Select(y => y.AssetClassName).Distinct());

            ViewBag.assetStatusId = new SelectList(db.Assets.Select(y => y.AssetStatus).Distinct());
            ViewBag.Group = new SelectList(db.AssetIndicators.Where(s=>s.Grouping == EntityCategory.asset_subcategory).Select(y => y.AssetIndicatorName).Distinct());


            return View(viewModel.Assets.ToPagedList(pageNumber, pageSize));
        }

       
        // GET: Asset/Details/5
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Details)]
        public ActionResult Details(int? id, int? barCode, EntityCategory Grouping)
        {
            if(barCode != null)
            {
                Asset Thisasset = db.Assets.Find(id);
            }
            Asset Theasset = db.Assets.Find(id);
           // string w = Theasset.Employee.FullName;
            //get the new Asset
            var NewAsset = db.Assets.Where(s => s.AssetEngravementNumber == Theasset.AssetEngravementNumber && s.AssetBarCode == Theasset.AssetBarCode).OrderByDescending(x => x.AssetPurchaseDate).FirstOrDefault();

            ViewBag.AssetStatus = Theasset.AssetStatus;
            ViewBag.AssetPurchaseDate = Theasset.AssetPurchaseDate;
            ViewBag.RevalueDate = Theasset.RevalueDate;
            ViewBag.EmployeeId = Theasset.EmployeeId;
            ViewBag.Grouping = Grouping;

            ViewBag.Id = NewAsset.Id;
            var revaluedAssets = db.Assets.Where(s => s.AssetBarCode == Theasset.AssetBarCode && s.AssetEngravementNumber == Theasset.AssetEngravementNumber).ToList();
            if(revaluedAssets.Count > 1)
            {
                ViewBag.IsRevalued = true;
                ViewBag.RevaluedAssets = revaluedAssets;
                Theasset.AllocationStatus = "Unallocated";
                db.SaveChanges();

            }
            if (Theasset.RevalueDate != null)
            {
                ViewBag.Revalued = true;
                ViewBag.AllocationStatus = "Unallocated";
                ViewBag.EmployeeId = Theasset.EmployeeId;
            

                // get all asset revalue history using asset engravement and bar code
                // var revaluedAssets = db.Assets.Where(s => s.AssetBarCode == Theasset.AssetBarCode && s.AssetEngravementNumber == Theasset.AssetEngravementNumber).ToList();

            }
        
            
            //*******************Genesis of depreciation in Asset DETAILS***********************//
            var depMethod = Theasset.AssetClass.DepreciationMethod;
            double TheDepPercentage = Theasset.AssetClass.DepPercentage;
            ViewBag.DepMethod = depMethod;
            ViewBag.DepPercentage = TheDepPercentage;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            //getting the useful life
            DateTime startDepreciationDate;
            startDepreciationDate = Theasset.StartDepreciationDate;
            DateTime currentDate;
           
            if (Theasset.EndDepreciationDate != null)
            {
                currentDate = Theasset.EndDepreciationDate.GetValueOrDefault();
            }
            else
            {
                currentDate = DateTime.UtcNow;
            }

            int usefullife = currentDate.Year - startDepreciationDate.Year;
            
            // creating an assetList
            List<Asset> assetList = new List<Asset>();

            //Depreciation starts here
            int? salvageValue = Theasset.SalvageValue;
            if (depMethod == AssetClass.DepMethods.ReducingBalanceMethod)
            {
                ViewBag.DisplayMethod = "Using the Reducing Balance Method";
            }
            else
            {
                ViewBag.DisplayMethod = "Using the StraightLine Method";
            }          
            int purchaseprice = Theasset.AssetPurchasePrice;
            int depAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * purchaseprice);
                
            int depPerDayAmount = depAmount / 365;
           // int daysPassed = startDepreciationDate.Date.Subtract(currentDate).Days;
            int firstYear = startDepreciationDate.Year;

            DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
            int numberofDays = (firstYearEndDate - startDepreciationDate).Days;
            int yearOneDepAmount = depPerDayAmount * numberofDays;
            int WorthAmount = purchaseprice - yearOneDepAmount;
            int Years = startDepreciationDate.Year;
            int accumulatedDep = yearOneDepAmount;

            for (int counter = 1; counter <= usefullife; counter++)
            {
                //int percent = Convert.ToInt32(TheDepPercentage);
                assetList.Add(new Asset()
                {
                    Year = Years,

                    DepPercentage = TheDepPercentage,

                    DepAmount = yearOneDepAmount,

                    GrossAmount = WorthAmount,

                    AccumulatedDepAmount = accumulatedDep,

                    DepreciationMethod = depMethod.ToString()

                });

                yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * WorthAmount);
                
                if (depMethod == AssetClass.DepMethods.ReducingBalanceMethod)
                {
                    WorthAmount = WorthAmount - yearOneDepAmount;
                }
                accumulatedDep = accumulatedDep + yearOneDepAmount;
                //checking whether worth amount == salvage value for automatic ready for disposal status
                if (salvageValue != null)
                {
                    if ((WorthAmount <= salvageValue) || (WorthAmount == 0))
                    {
                        break;

                    }
                }
                           
                Years++;
            }
              
            
            ViewBag.assetList = assetList;
            
            /******************End of depreciation in Asset DETAILS*************************/

            if (Theasset == null)
            {
                return HttpNotFound();
            }

            //call the logservice to add a log
            DateTime dateAndTime = DateTime.Now;
            string name = HttpContext.User.Identity.Name;
            string action = "Depreciation Calculation";
            string description = "Depreciation for Asset whose name is " + Theasset.AssetName + " and Asset Code is " + Theasset.AssetCode + " was calculated";

            ActivityLogService activityLogService = new ActivityLogService();
            activityLogService.LogAction(db, action, description, name, dateAndTime);

            return View(Theasset);
        }
        
        //POST: Asset/Disposal

        public ActionResult ActivateReadyForDisposal(int? id)
        {
            Asset Theasset = db.Assets.Find(id);
            Theasset.AssetStatus = "Ready For Disposal";
            db.SaveChanges();



            //**************Create a transaction here and log the user****************//
            DateTime dateAndTime = DateTime.Now;
            string name = HttpContext.User.Identity.Name;
            ViewBag.Name = name;
            string action = "Changed Asset Status";
            string description = "Changed the Asset Status whose Asset Code is " + Theasset.AssetCode + " and Asset Name " + Theasset.AssetName + " to Ready For Disposal";
            ActivityLogService activityLogService = new ActivityLogService();
            activityLogService.LogAction(db, action, description, name, dateAndTime);

            //*******************End of transaction***********************//

            return RedirectToAction("Index", "Asset");
        }

        //GET: Asset/Disposal
        public ActionResult ReadyForDisposal()
        {
            var viewModel = new AssetViewModel();
            viewModel.Assets = db.Assets.Where(m => m.AssetStatus == "Ready For Disposal").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);

            return View(viewModel);
        }

        //GET: Asset/Revalue
        public ActionResult Revalue(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            ViewBag.AssetStatus = asset.AssetStatus;
            if (asset == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssetClassId = new SelectList(db.AssetClasses, "Id", "AssetClassName", asset.AssetClass.AssetClassName);
            ViewBag.AssetIndicatorId = new SelectList(db.AssetIndicators, "Id", "AssetIndicatorName", asset.AssetIndicatorId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName", asset.EmployeeId);
            ViewBag.MaintenanceFrequencyId = new SelectList(db.MaintenanceFrequencies, "Id", "TheFrequency", asset.MaintenanceFrequencyId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "SupplierName", asset.SupplierId);
            return View(asset);
        }

        //POST: Asset/Revalue
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Edit)]
        public ActionResult Revalue(Asset asset, Asset theAsset)
        {
            if (ModelState.IsValid)
            {
                //Revalue service
                AssetService assetService = new AssetService();
                assetService.GetRevaluedAssets(db, asset, theAsset);
               
                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Edited an Asset";
                string description = "Edited an asset whose Asset Code is " + asset.AssetCode + " and Asset Name " + asset.AssetName;
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index");
            }
            //InitializeViewBagData();
            ViewBag.AssetClassId = new SelectList(db.AssetClasses, "Id", "AssetClassCode", asset.AssetClassId);
            ViewBag.AssetIndicatorId = new SelectList(db.AssetIndicators, "Id", "AssetIndicatorName", asset.AssetIndicatorId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FirstName", asset.EmployeeId);
            ViewBag.MaintenanceFrequencyId = new SelectList(db.MaintenanceFrequencies, "Id", "TheFrequency", asset.MaintenanceFrequencyId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "SupplierName", asset.SupplierId);
            return View(asset);
        }


        // GET: Asset/Create

        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Create)]
        public ActionResult Create(EntityCategory Grouping)
        {
            ViewBag.Grouping = Grouping;
            ViewBag.AssetSubclass = new SelectList(db.AssetIndicators.Where(s => s.Grouping == EntityCategory.asset_subcategory).Select(y => y.AssetIndicatorName).Distinct());
            ViewBag.AssetClassId = new SelectList(db.AssetClasses, "Id", "AssetClassName");
            ViewBag.AssetIndicatorId = new SelectList(db.AssetIndicators, "Id", "AssetIndicatorName");
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName");
            ViewBag.MaintenanceFrequencyId = new SelectList(db.MaintenanceFrequencies, "Id", "TheFrequency");
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "SupplierName");
            // ViewBag.AssetClassId = new SelectList(db.AssetClasses, "Id", "DepreciationMethod");

            return View();
        }


        //GET:Asset /UnallocatedAssets
        public ActionResult UnallocatedAssets(EntityCategory Grouping)
        {
            ViewBag.AssetClassId = new SelectList(db.Assets, "Id", "AssetName");
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FirstName");
            ViewBag.Grouping = Grouping;
            //List<Employee> employeeNames = new List<Employee>();
            //Employee employee = new Employee();


            EmployeeAssetViewModel employeeAssetViewModel = new EmployeeAssetViewModel
            {

                AssetList = new SelectList(db.Assets.Where(d => d.AllocationStatus == "Unallocated" && d.AssetStatus=="Active"), "Id", "FullAssetName"),
                EmployeeList = new SelectList(db.Employees, "Id", "FullName")
        };
           
            return View(employeeAssetViewModel);
        }


        //POST:Asset /UnallocatedAssets
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UnallocatedAssets(EmployeeAssetViewModel viewModel)
        {
          
                
                    Asset asset = db.Assets.Find(viewModel.AssetId);

             
                    asset.EmployeeId = viewModel.EmployeeId;
                    asset.AllocationStatus = viewModel.AllocationStatus;

                    db.Entry(asset).State = EntityState.Modified;
                    db.SaveChanges();

                return RedirectToAction("Index", new { Grouping = EntityCategory.asset_subcategory });


            


            //return View();


        }

        // POST: Asset/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Create)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Create)]
        //public ActionResult Create([Bind(Include = "Id,AssetCode,AssetName,AssetQuantity,AssetStatus,AssetSerialNumber,AssetEngravementNumber,AssetPurchaseDate,AssetPurchasePrice,AssetComment,AssetClassId,MaintenanceFrequencyId,EmployeeId,SupplierId,AssetIndicatorId,AssetDisposalDate")] Asset asset)
        public ActionResult Create(Asset asset)
        {
            
            if (ModelState.IsValid)
            {
                var lastAsset = db.Assets.OrderByDescending(c => c.AssetCode).FirstOrDefault();
                if (lastAsset == null)
                {
                    asset.AssetCode = "AC/DC/001";

                }
                else
                {//generating A random Asst class code

                    Random random = new Random();
                    asset.AssetCode = "AC/DC/" + random.Next(2000, 2200);
                }
                
                    if (asset.EmployeeId == null)
                     {
                         asset.AllocationStatus = "Unallocated";

                        //asset.EmployeeId = 0;
                     }
                else
                {
                    asset.AllocationStatus = "Allocated";
                }
                     ViewBag.EmployeeId = asset.EmployeeId;

                

                db.Assets.Add(asset);
                db.SaveChanges();


                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
                string action = "Created an Asset";
                string description = "Created an asset whose Asset Code is " + asset.AssetCode + " and Asset Name " + asset.AssetName;
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//


                return RedirectToAction("Index", new { Grouping = EntityCategory.asset_subcategory });
            }

            ViewBag.AssetClassName = new SelectList(db.AssetClasses, "Id", "AssetClassName", asset.AssetName);
            ViewBag.AssetClassId = new SelectList(db.AssetClasses, "Id", "AssetClassCode", asset.AssetClassId);
            ViewBag.AssetIndicatorId = new SelectList(db.AssetIndicators, "Id", "AssetIndicatorName", asset.AssetIndicatorId);

            ViewBag.AssetSubclass = new SelectList(db.AssetIndicators.Where(s => s.Grouping == EntityCategory.asset_subcategory).Select(y => y.AssetIndicatorName).Distinct());


            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName", asset.EmployeeId);
            ViewBag.MaintenanceFrequencyId = new SelectList(db.MaintenanceFrequencies, "Id", "TheFrequency", asset.MaintenanceFrequencyId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "SupplierName", asset.SupplierId);
            db.SaveChanges();
            return View(asset);
        }

        
        // GET: Asset/Edit/5
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Edit)]
        public ActionResult Edit(int? id, EntityCategory Grouping)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            ViewBag.Grouping = Grouping;
            if (asset == null)
            {
                return HttpNotFound();
            }

            ViewBag.AssetSubclass = new SelectList(db.AssetIndicators.Where(s => s.Grouping == EntityCategory.asset_subcategory).Select(y => y.AssetIndicatorName).Distinct());
            ViewBag.AssetClassId = new SelectList(db.AssetClasses, "Id", "AssetClassName", asset.AssetClassId);
            ViewBag.AssetIndicatorId = new SelectList(db.AssetIndicators, "Id", "AssetIndicatorName", asset.AssetIndicatorId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName", asset.EmployeeId);
            ViewBag.MaintenanceFrequencyId = new SelectList(db.MaintenanceFrequencies, "Id", "TheFrequency", asset.MaintenanceFrequencyId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "SupplierName", asset.SupplierId);
            return View(asset);
        }

        // POST: Asset/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Edit)]
        public ActionResult Edit(Asset asset)
        {
            if (ModelState.IsValid)
            {
                Disposal disposal = db.Disposals.SingleOrDefault(m => m.AssetId == asset.Id);
                if (disposal != null)
                {
                    asset.AssetStatus = "Disposed";
                }
                if (asset.EmployeeId == null)
                {
                    asset.AllocationStatus = "Unallocated";

                    //asset.EmployeeId = 0;
                }
                else
                {
                    asset.AllocationStatus = "Allocated";
                }
                ViewBag.EmployeeId = asset.EmployeeId;

                db.Entry(asset).State = EntityState.Modified;

                db.SaveChanges();



                //**************Create a transaction here and log the user****************//
                DateTime dateAndTime = DateTime.Now;
                string name = HttpContext.User.Identity.Name;
                ViewBag.Name = name;
               string action = "Edited an Asset";
                string description = "Edited an asset whose Asset Code is " + asset.AssetCode + " and Asset Name " + asset.AssetName;
                ActivityLogService activityLogService = new ActivityLogService();
                activityLogService.LogAction(db, action, description, name, dateAndTime);

                //*******************End of transaction***********************//

                return RedirectToAction("Index", new { Grouping = EntityCategory.asset_subcategory });
            }
            //InitializeViewBagData();
            ViewBag.AssetClassId = new SelectList(db.AssetClasses, "Id", "AssetClassCode", asset.AssetClassId);
            ViewBag.AssetIndicatorId = new SelectList(db.AssetIndicators, "Id", "AssetIndicatorName", asset.AssetIndicatorId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName", asset.EmployeeId);
            ViewBag.MaintenanceFrequencyId = new SelectList(db.MaintenanceFrequencies, "Id", "TheFrequency", asset.MaintenanceFrequencyId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "SupplierName", asset.SupplierId);
            ViewBag.AssetSubclass = new SelectList(db.AssetIndicators.Where(s => s.Grouping == EntityCategory.asset_subcategory).Select(y => y.AssetIndicatorName).Distinct());

            return View(asset);
        }

        // GET: Asset/Delete/5
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Delete)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Asset asset = db.Assets.Find(id);
            if (asset == null)
            {
                return HttpNotFound();
            }
            return View(asset);
        }

        // POST: Asset/Delete/5
        [HttpPost, ActionName("Delete")]
        [DynamicRoleAuthorize(ControllerName.Asset, ActionName.Delete)]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Asset asset = db.Assets.Find(id);
            db.Assets.Remove(asset);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
        /*****************************Exporting Asset List to excel************************/
        // POST: Asset/AssetList
        [HttpPost]
        public ActionResult AssetList(string statusFilter, string classFilter, string startDateFilter, string endDateFilter, string SearchedItemFilter)
        {
            var theAsset = db.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);

            //***********************Genesis of Filtering************************//
            DateTime startDate;
            DateTime endDate;

            if ((!String.IsNullOrEmpty(statusFilter)) || (!String.IsNullOrEmpty(classFilter)) || (startDateFilter != null) || (endDateFilter != null))
            {
                 int theAssetClassId = Convert.ToInt32(classFilter);
                if ((!String.IsNullOrEmpty(startDateFilter)) && (!String.IsNullOrEmpty(endDateFilter)))
                {
                    startDate = DateTime.Parse(startDateFilter);
                    endDate = DateTime.Parse(endDateFilter);

                    theAsset = db.Assets.Where(d => d.AssetStatus == statusFilter).Where(c => startDate <= c.AssetPurchaseDate && endDate >= c.AssetPurchaseDate).Include(a => a.AssetClass).Where(d => d.AssetClassId == theAssetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);

                }
                else {
                    theAsset = db.Assets.Where(d => d.AssetStatus == statusFilter).Include(a => a.AssetClass).Where(d => d.AssetClassId == theAssetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);

                }

                ViewBag.assetClassFilter = classFilter;
                ViewBag.assetStatusFilter = statusFilter;
                ViewBag.startDate = startDateFilter;
                ViewBag.endDate = endDateFilter;
            }

            else
            {
                theAsset = db.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                ViewBag.assetStatusFilter = "Active";
            }
            
            //***********************Revelation of Filtering by Asset Class************************//

            var asset = theAsset;

            ExcelPackage Ep = new ExcelPackage();
            ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("AssetList");
            Sheet.Cells["A1"].Value = "ASSET CODE";
            Sheet.Cells["B1"].Value = "ASSET NAME";
            Sheet.Cells["C1"].Value = "PURCHASE DATE";
            Sheet.Cells["D1"].Value = "PURCHASE PRICE";
            Sheet.Cells["E1"].Value = "DEPRECIATION %";
            Sheet.Cells["F1"].Value = "DEPRECIATION AMOUNT";
            Sheet.Cells["G1"].Value = "ACC. DEPRECIATION";
            Sheet.Cells["H1"].Value = "NETBOOK VALUE";
            Sheet.Cells["I1"].Value = "ASSET STATUS";

            int row = 2;


            foreach (var item in asset)
            {

                int purchaseprice = item.AssetPurchasePrice;
                double TheDepPercentage = item.AssetClass.DepPercentage;
                var depMethod = item.AssetClass.DepreciationMethod;
                string DepreciationMethod = depMethod.ToString();

                DateTime startDepDate = item.StartDepreciationDate;
                DateTime currentDate;
                
                
                if (item.EndDepreciationDate != null)
                {
                    currentDate = item.EndDepreciationDate.GetValueOrDefault();
                }
                else
                {
                    currentDate = DateTime.UtcNow;
                }

                int usefullife = currentDate.Year - startDepDate.Year;

                CalculateDepreciations calculateDepreciations = new CalculateDepreciations();
                int[] myArray = new int[3];
               // DateTime startDepDate = item.StartDepreciationDate;
                myArray = calculateDepreciations.CalculateDepAmount(purchaseprice, DepreciationMethod, (int)TheDepPercentage, usefullife, startDepDate, currentDate);
                int singledepAmount = Convert.ToInt32(myArray[0]);
                int netBookValue = Convert.ToInt32(myArray[1]);
                int accDepreciation = Convert.ToInt32(myArray[2]);

                //values with commas
                string purchasePriceCommas = string.Format("{0:n0}", item.AssetPurchasePrice);
                string singleDepAmountCommas = string.Format("{0:n0}", singledepAmount);
                string accDepreciationCommas = string.Format("{0:n0}", accDepreciation);
                string netBookValueCommas = string.Format("{0:n0}", netBookValue);

                Sheet.Cells[string.Format("D{0}", row)].Style.Numberformat.Format = "#,##0";
                Sheet.Cells[string.Format("F{0}", row)].Style.Numberformat.Format = "#,##0";
                Sheet.Cells[string.Format("G{0}", row)].Style.Numberformat.Format = "#,##0";
                Sheet.Cells[string.Format("H{0}", row)].Style.Numberformat.Format = "#,##0";
                

                Sheet.Cells[string.Format("A{0}", row)].Value = item.AssetCode;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.AssetName;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.AssetPurchaseDate.ToString();
                Sheet.Cells[string.Format("D{0}", row)].Value = purchasePriceCommas;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.AssetClass.DepPercentage;
                Sheet.Cells[string.Format("F{0}", row)].Value = singleDepAmountCommas;
                Sheet.Cells[string.Format("G{0}", row)].Value = accDepreciationCommas;
                Sheet.Cells[string.Format("H{0}", row)].Value = netBookValueCommas;
                Sheet.Cells[string.Format("I{0}", row)].Value = item.AssetStatus;


                row++;
            }
            Sheet.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment: filename=" + "AssetList.xlsx");
            Response.BinaryWrite(Ep.GetAsByteArray());
            Response.End();

            //**************Create a transaction here and log the user****************//
            DateTime dateAndTime = DateTime.Now;
            string name = HttpContext.User.Identity.Name;
            ViewBag.Name = name;       
            string action = "Exported to Excel";
            string description = "Exported the Assets List to Excel";
            ActivityLogService activityLogService = new ActivityLogService();
            activityLogService.LogAction(db, action, description, name,dateAndTime);

            //*******************End of transaction***********************//


            using (MemoryStream stream = new MemoryStream())
            {
                Ep.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "AssetList.xlsx");

            }
        }

        // GET: Asset/AssetReport
      
        public ActionResult AssetReport(string sortOrder,string currentFilter, string searchString, int? page, int? id, string assetStatusId, string assetClassId, string textStartDate, string textEndDate, int? thepageSize, string filterStatus, EntityCategory Grouping, string Group)
        {
            //to enable search, page and sort
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSorting = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSorting = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.AssetCodeSorting = sortOrder == "AssetCode" ? "assetCode_desc" : "AssetCode";
            ViewBag.AssetClassSorting = sortOrder == "AssetClass" ? "assetClass_desc" : "AssetClass";
            ViewBag.PurchasePriceSorting = sortOrder == "PurchasePrice" ? "PurchasePrice_desc" : "PurchasePrice";
            ViewBag.DepPercentageSorting = sortOrder == "DepPercentage" ? "DepPercentage_desc" : "DepPercentage";
            ViewBag.DepAmountSorting = sortOrder == "DepAmount" ? "DepAmount_desc" : "DepAmount";
            ViewBag.NBVSorting = sortOrder == "NBValue" ? "NBValue_desc" : "NBValue";
            ViewBag.AccDepSorting = sortOrder == "AccDep" ? "AccDep_desc" : "AccDep";
            ViewBag.EngNumberSorting = sortOrder == "EngNumberSorting" ? "EngNumberSorting_desc" : "EngNumberSorting";

            ViewBag.Grouping = Grouping;
            ViewBag.CurrentFilter = searchString;
            
            //ViewBag.CurrentFilter = searchString;
            var viewModel = new AssetViewModel();

            DateTime startDate;
            DateTime endDate;

            //***********************Genesis of Filtering************************//

            if ((!String.IsNullOrEmpty(assetStatusId)) || (!String.IsNullOrEmpty(assetClassId)) || (textStartDate != null) || (textEndDate != null))
            {


                if ((!String.IsNullOrEmpty(textStartDate)) && (!String.IsNullOrEmpty(textEndDate)) && (!String.IsNullOrEmpty(assetClassId)))
                {
                    startDate = DateTime.Parse(textStartDate);
                    endDate = DateTime.Parse(textEndDate);
                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId).Where(c => startDate <= c.AssetPurchaseDate && endDate >= c.AssetPurchaseDate).Include(a => a.AssetClass).Where(d => d.AssetClassName == assetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                }
                else if ((!String.IsNullOrEmpty(textStartDate)) && (!String.IsNullOrEmpty(textEndDate)))
                {
                    startDate = DateTime.Parse(textStartDate);
                    endDate = DateTime.Parse(textEndDate);

                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId).Where(c => startDate <= c.AssetPurchaseDate && endDate >= c.AssetPurchaseDate).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                }
                else if ((!String.IsNullOrEmpty(textStartDate)) && (!String.IsNullOrEmpty(textEndDate)) && (!String.IsNullOrEmpty(Group)))
                {
                    startDate = DateTime.Parse(textStartDate);
                    endDate = DateTime.Parse(textEndDate);

                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId && d.AssetSubclass == Group).Where(c => startDate <= c.AssetPurchaseDate && endDate >= c.AssetPurchaseDate).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                }
                else if (!String.IsNullOrEmpty(assetClassId) && (!String.IsNullOrEmpty(assetStatusId)) && (!String.IsNullOrEmpty(Group)))
                {
                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId && d.AssetSubclass == Group).Include(a => a.AssetClass).Where(d => d.AssetClass.AssetClassName == assetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(assetClassId) && (!String.IsNullOrEmpty(assetStatusId)))
                {
                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId).Include(a => a.AssetClass).Where(d => d.AssetClass.AssetClassName == assetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(Group) && (!String.IsNullOrEmpty(assetStatusId)))
                {
                    viewModel.Assets = db.Assets.Where(d => d.AssetStatus == assetStatusId && d.AssetSubclass == Group).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(assetClassId))
                {

                    viewModel.Assets = db.Assets.Include(a => a.AssetClass).Where(d => d.AssetClass.AssetClassName == assetClassId).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(assetStatusId))
                {
                    viewModel.Assets = db.Assets.Where(s => s.AssetStatus == assetStatusId).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                else if (!String.IsNullOrEmpty(Group))
                {
                    viewModel.Assets = db.Assets.Where(s => s.AssetSubclass == Group).Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);

                    ViewBag.realClass = assetClassId;
                    ViewBag.realStatus = assetStatusId;
                }
                ViewBag.assetClassFilter = assetClassId;
                ViewBag.assetStatusFilter = assetStatusId;
                ViewBag.startDate = textStartDate;
                ViewBag.endDate = textEndDate;
            }

            else
            {
                viewModel.Assets = db.Assets.Where(d => d.AssetStatus == "Active").Include(a => a.AssetClass).Include(a => a.AssetIndicator).Include(a => a.Employee).Include(a => a.MaintenanceFrequency).Include(a => a.Supplier);
                
                ViewBag.assetStatusFilter = "Active";
            }
            //***********************Revelation of Filtering ********************//
            
            //searching functionality starts here
            if (!String.IsNullOrEmpty(searchString))
            {
                
                string realName = searchString.ToUpper();
                viewModel.Assets = viewModel.Assets.Where(s => s.AssetEngravementNumber.ToUpper().Contains(realName) || s.AssetName.ToUpper().Contains(realName) || s.AssetClass.AssetClassName.ToUpper().Contains(realName));

                ViewBag.SearchedItem = searchString;
            }
            if (!String.IsNullOrEmpty(filterStatus))
            {
                ViewBag.FilterStatus = filterStatus;
                if (filterStatus == "show")
                {
                    ViewBag.FilterStatus = "show";
                }
                else
                {
                    ViewBag.FilterStatus = "hide";
                }
            }
            else
            {
                ViewBag.FilterStatus = "hide";
            }

            List<Asset> assets = new List<Asset>();

            //*****************Genesis of automatic ready for disposing********************//
            
            foreach (var Theasset in viewModel.Assets)
            {
                Asset myasset = new Asset();
                int purchaseprice = Theasset.AssetPurchasePrice;
                double TheDepPercentage = Theasset.AssetClass.DepPercentage;

                var depMethod = Theasset.AssetClass.DepreciationMethod;
                int depAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * purchaseprice);

                int WorthAmount = purchaseprice - depAmount;

                string DepreciationMethod = depMethod.ToString();

                //getting the useful life
                DateTime startDepreciationDate;
                startDepreciationDate = Theasset.StartDepreciationDate;
                DateTime currentDate;

                if (Theasset.EndDepreciationDate != null)
                {
                    currentDate = Theasset.EndDepreciationDate.GetValueOrDefault();
                }
                else
                {
                    currentDate = DateTime.UtcNow;
                }

                int usefullife = currentDate.Year - startDepreciationDate.Year;

                // creating an assetList
                List<Asset> theAssetList = new List<Asset>();

                    //when reducing balance is chosen
                int? salvageValue = Theasset.SalvageValue;
                if (depMethod == AssetClass.DepMethods.ReducingBalanceMethod)
                {
                    ViewBag.DisplayMethod = "Using the Reducing Balance Method";
                }
                else
                {
                    ViewBag.DisplayMethod = "Using the Straight Line Method";
                }
                    

                //depreciation starts from here
                int singlePurchaseprice = Theasset.AssetPurchasePrice;
                int singleDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * singlePurchaseprice);

                int depPerDayAmount = singleDepAmount / 365;
                int daysPassed = startDepreciationDate.Date.Subtract(currentDate).Days;
                int firstYear = startDepreciationDate.Year;

                DateTime firstYearEndDate = new DateTime(firstYear, 12, 31, 0, 0, 0);
                int numberofDays = (firstYearEndDate - startDepreciationDate).Days;
                int yearOneDepAmount = depPerDayAmount * numberofDays;
                int singleWorthAmount = singlePurchaseprice - yearOneDepAmount;
                int Years = startDepreciationDate.Year;
                int theaccumulatedDep = yearOneDepAmount;

                for (int counter = 1; counter <= (usefullife-1); counter++)
                {
                    yearOneDepAmount = Convert.ToInt32((Convert.ToDouble(TheDepPercentage) / Convert.ToDouble(100)) * singleWorthAmount);
                    if(depMethod == AssetClass.DepMethods.ReducingBalanceMethod)
                    {
                        singleWorthAmount = singleWorthAmount - yearOneDepAmount;

                    }
                    theaccumulatedDep = theaccumulatedDep + yearOneDepAmount;
                    //checking whether worth amount == salvage value for automatic ready for disposal status
                    if (salvageValue != null)
                    {
                        if ((singleWorthAmount <= salvageValue) || (singleWorthAmount == 0))
                        {
                            break;

                        }
                    }
                   // int percent = Convert.ToInt32(TheDepPercentage);
                    theAssetList.Add(new Asset()
                    {
                        Year = Years,

                        DepPercentage = TheDepPercentage,

                        DepAmount = yearOneDepAmount,

                        GrossAmount = singleWorthAmount,

                        AccumulatedDepAmount = theaccumulatedDep,

                        DepreciationMethod = depMethod.ToString()

                    });
                    Years++;
                }
                
                myasset.DepAmount = yearOneDepAmount;
                myasset.GrossAmount = singleWorthAmount;
                myasset.accumulatedDepreciation = theaccumulatedDep;
                myasset.DepPercentage = TheDepPercentage;
                myasset.Id = Theasset.Id;
                myasset.DepreciationMethod = depMethod.ToString();
                myasset.AssetName = Theasset.AssetName;
                myasset.AssetClassName = Theasset.AssetClass.AssetClassName;
                myasset.AssetCode = Theasset.AssetCode;
                myasset.AssetEngravementNumber = Theasset.AssetEngravementNumber;
                myasset.AssetStatus = Theasset.AssetStatus;
                myasset.AssetPurchasePrice = Theasset.AssetPurchasePrice;
                myasset.AssetPurchaseDate = Theasset.AssetPurchaseDate;
                assets.Add(myasset);

                //end of depreciation calculation

                ViewBag.theAssetList = theAssetList;
                
                
            }
            
            //sort format
            switch (sortOrder)
            {
                case "name_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.AssetName);
                    break;
                case "Date":
                    viewModel.Assets = assets.OrderBy(s => s.AssetPurchaseDate);
                    break;
                case "date_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.AssetPurchaseDate);
                    break;
                case "AssetClass":
                    viewModel.Assets = assets.OrderBy(s => s.AssetClassName);
                    break;
                case "assetClass_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.AssetClassName);
                    break;
                case "AssetCode":
                    viewModel.Assets = assets.OrderBy(s => s.AssetCode);
                    break;
                case "assetCode_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.AssetCode);
                    break;
                case "PurchasePrice":
                    viewModel.Assets = assets.OrderBy(s => s.AssetPurchasePrice);
                    break;
                case "PurchasePrice_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.AssetPurchasePrice);
                    break;
                case "DepPercentage":
                    viewModel.Assets = assets.OrderBy(s => s.DepPercentage);
                    break;
                case "DepPercentage_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.DepPercentage);
                    break;
                case "DepAmount":
                    viewModel.Assets = assets.OrderBy(s => s.DepAmount);
                    break;
                case "DepAmount_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.DepAmount);
                    break;
                case "NBValue":
                    viewModel.Assets = assets.OrderBy(s => s.GrossAmount);
                    break;
                case "NBValue_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.GrossAmount);
                    break;
                case "AccDep":
                    viewModel.Assets = assets.OrderBy(s => s.accumulatedDepreciation);
                    break;
                case "AccDep_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.accumulatedDepreciation);
                    break;
                case "EngNumberSorting":
                    viewModel.Assets = assets.OrderBy(s => s.AssetEngravementNumber);
                    break;
                case "EngNumberSorting_desc":
                    viewModel.Assets = assets.OrderByDescending(s => s.AssetEngravementNumber);
                    break;
                default:
                    viewModel.Assets = assets.OrderBy(s => s.AssetName);
                    break;
            }


            ViewBag.assetClassId = new SelectList(db.AssetClasses.Select(y => y.AssetClassName).Distinct());


            ViewBag.assetStatusId = new SelectList(db.Assets.Select(y => y.AssetStatus).Distinct());
            ViewBag.Group = new SelectList(db.AssetIndicators.Where(s => s.Grouping == EntityCategory.asset_subcategory).Select(y => y.AssetIndicatorName).Distinct());

            int pageSize = (thepageSize ?? 10);
            int pageNumber = (page ?? 1);
            ViewBag.PageSize = pageSize;
            ViewBag.realClass = "";

            ViewBag.numEmployees = viewModel.Assets.Count();

            //**************Create a transaction here and log the user****************//
            DateTime dateAndTime = DateTime.Now;
            string name = HttpContext.User.Identity.Name;
            ViewBag.Name = name;           
            string action = "Calculated Assets Depreciation";
            string description = "Calculated depreciation for the Assets Report ";
            ActivityLogService activityLogService = new ActivityLogService();
            activityLogService.LogAction(db, action, description, name, dateAndTime);

            //*******************End of transaction***********************//

            return View(viewModel.Assets.ToPagedList(pageNumber, pageSize));
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

     
    }
    

}
